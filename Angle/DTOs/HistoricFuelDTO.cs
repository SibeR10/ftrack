﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class HistoricFuelDTO
    {
        public int HistoricFuelID { get; set; }

		[Display(Name = "Fecha de carga")]
		public DateTime DateChargeVehicle { get; set; }

		public int Charge { get; set; }

		[Display(Name = "Combustible")]
		public int FuelID { get; set; }

		[Display(Name = "Chofer")]
		public int DriverID { get; set; }

		[Display(Name = "Vehiculo")]
		public int VehiclesID { get; set; }

	}
}
