﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        private readonly FTContext FTContext;

        public CustomerRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }
        
        public async Task<IEnumerable<DocumentUpload>> GetDocumentUpload()
        {

            var listDocumentUpload = await FTContext.DocumentUpload.ToListAsync();

            return listDocumentUpload;
        }

        public async Task<IEnumerable<Status>> GetStatusByCustomer(int id)
        {

            var listStatusByCustomer = await FTContext.Customer
                .Include(x => x.Status)
                .Where(x => x.StatusID == id)
                .Select(x => x.Status)
                .ToListAsync();

            return listStatusByCustomer;
        }

        
        public async Task<IEnumerable<Division>> GetDivision()
        {

            var listDivision = await FTContext.Division.ToListAsync();

            return listDivision;
        }

        public async Task<IEnumerable<Division>> GetDivisionByCustomer(int id)
        {

            var listDivisionByCustomer = await FTContext.Customer
                .Include(x => x.Division)
                .Where(x => x.DivisionID == id)
                .Select(x => x.Division)
                .ToListAsync();

            return listDivisionByCustomer;
        }
        
        public async Task<IEnumerable<CustomerType>> GetCustomerType()
        {

            var listCustomerType = await FTContext.CustomerType.ToListAsync();

            return listCustomerType;
        }

        public async Task<IEnumerable<CustomerType>> GetCustomerTypeByCustomer(int id)
        {

            var listCustomerTypeByCustomer = await FTContext.Customer
                .Include(x => x.CustomerType)
                .Where(x => x.CustomerTypeID == id)
                .Select(x => x.CustomerType)
                .ToListAsync();

            return listCustomerTypeByCustomer;
        }
        
        
        public async Task<IEnumerable<DocumentType>> GetDocumentType()
        {

            var listDocumentType = await FTContext.DocumentType.ToListAsync();

            return listDocumentType;
        }

        public async Task<IEnumerable<DocumentType>> GetDocumentTypeByCustomer(int id)
        {

            var listDocumentTypeByCustomer = await FTContext.Customer
                .Include(x => x.DocumentType)
                .Where(x => x.DocumentTypeID == id)
                .Select(x => x.DocumentType)
                .ToListAsync();

            return listDocumentTypeByCustomer;
        }
        public async Task<IEnumerable<Account>> GetAccount()
        {

            var listAccount = await FTContext.Account.ToListAsync();

            return listAccount;
        }

        public async Task<IEnumerable<Account>> GetAccountByCustomer(int id)
        {

            var listAccountByCustomer = await FTContext.Customer
                .Include(x => x.Account)
                .Where(x => x.AccountID == id)
                .Select(x => x.Account)
                .ToListAsync();

            return listAccountByCustomer;
        }
        public async Task<IEnumerable<AccountType>> GetAccountType()
        {

            var listAccountType = await FTContext.AccountType.ToListAsync();

            return listAccountType;
        }

        public async Task<IEnumerable<AccountType>> GetAccountTypeByCustomer(int id)
        {

            var listAccountTypeByCustomer = await FTContext.Customer
                .Include(x => x.AccountType)
                .Where(x => x.AccountTypeID == id)
                .Select(x => x.AccountType)
                .ToListAsync();

            return listAccountTypeByCustomer;
        }
        public async Task<IEnumerable<SalesPerson>> GetSalesPerson()
        {

            var listSalesPerson = await FTContext.SalesPerson.ToListAsync();

            return listSalesPerson;
        }

        public async Task<IEnumerable<SalesPerson>> GetSalesPersonByCustomer(int id)
        {

            var listSalesPersonByCustomer = await FTContext.Customer
                .Include(x => x.SalesPerson)
                .Where(x => x.SalesPersonID == id)
                .Select(x => x.SalesPerson)
                .ToListAsync();

            return listSalesPersonByCustomer;
        }
        public async Task<IEnumerable<Contact>> GetContact()
        {

            var listContact = await FTContext.Contact.ToListAsync();

            return listContact;
        }

        public async Task<IEnumerable<Contact>> GetContactByCustomer(int id)
        {

            var listContactByCustomer = await FTContext.Contact
                .Include(x => x.Customer)
                .Where(x => x.CustomerID == id)
                .ToListAsync();

            return listContactByCustomer;
        }
        public async Task<IEnumerable<Address>> GetAddress()
        {

            var listAddress = await FTContext.Address.ToListAsync();

            return listAddress;
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {

            var listGeoLevel1 = await FTContext.GeoLevel1.ToListAsync();

            return listGeoLevel1;
        }

        public async Task<IEnumerable<Address>> GetAddressByCustomer(int id)
        {

            var listAddressByCustomer = await FTContext.Address
                .Include(x => x.Customer)
                .Where(x => x.CustomerID == id)
                .ToListAsync();

            return listAddressByCustomer;
        }
             

    }
}
