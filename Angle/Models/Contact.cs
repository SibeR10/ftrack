﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Contact
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContactID { get; set; }
        public string ContactName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactPosition { get; set; }
        public int ContactMainPhone { get; set; }
        public int ContactCellPhone { get; set; }
        public string ContactMail { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
        public Customer Customer { get; set; }
        public int CustomerID { get; set; }
    }
}
