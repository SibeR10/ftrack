﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IDriverTypeRepository : IGenericRepository<DriverType>
    {
        Task<IEnumerable<Status>> GetStatus();

        Task<IEnumerable<Status>> GetStatusByDriverType(int id);
    }
}
