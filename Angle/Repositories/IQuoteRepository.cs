﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IQuoteRepository : IGenericRepository<Quote>
    {
        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();
        Task<IEnumerable<QuoteStatus>> GetQuoteStatus();
        Task<IEnumerable<Warehouse>> GetWarehouse();
        Task<IEnumerable<Country>> GetCountry();
        Task<IEnumerable<ChargesByQuote>> GetChargesByQuote();
        Task<IEnumerable<Customer>> GetCustomer();
        Task<IEnumerable<SalesPerson>> GetSalesPerson();
        Task<IEnumerable<Division>> GetDivision();
        Task<IEnumerable<AddressType>> GetAddressType();
        Task<IEnumerable<ServiceType>> GetServiceType();
        Task<IEnumerable<ItemsQuote>> GetItemsQuote();
        Task<IEnumerable<MerchandiseType>> GetMerchandiseType();
        Task<IEnumerable<ContractSpecial>> GetContractSpecial();
        Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence();
        Task<IEnumerable<ContractSequence>> GetContractSequence();
        Task<IEnumerable<Person>> GetSender();
        Task<IEnumerable<Person>> GetRemittee();
    }
}
