﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class TransportType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TransportTypeID { get; set; }
        public string TransportTypeName { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
