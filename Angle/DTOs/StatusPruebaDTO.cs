﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class StatusPruebaDTO
    {
        [Required(ErrorMessage = "The QuoteStatus Name is required")]
        [Display(Name = "Descripción del status")]
        public string QuoteStatusName { get; set; }

        [Display(Name = "Color de la etiqueta")]
        public string StatusColor { get; set; }

    }
}
