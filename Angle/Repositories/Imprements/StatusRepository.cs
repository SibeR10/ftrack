﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class StatusRepository : GenericRepository<Status>, IStatusRepository
    {
        private readonly FTContext FTContext;

        public StatusRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
    }
}
