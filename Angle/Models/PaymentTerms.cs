﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class PaymentTerms
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PaymentTermsID { get; set; }
        public string PaymentTermsDescription { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
