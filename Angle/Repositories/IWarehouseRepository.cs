﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IWarehouseRepository : IGenericRepository<Warehouse>
    {
        Task<IEnumerable<Status>> GetStatus();

        Task<IEnumerable<Status>> GetStatusByWarehouse(int id);
        Task<IEnumerable<Division>> GetDivision();

        Task<IEnumerable<Division>> GetDivisionByWarehouse(int id);
    }
}
