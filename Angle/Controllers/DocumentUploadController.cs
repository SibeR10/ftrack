﻿using AutoMapper;
using Angle.DTOs;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Collections.Generic;

using Microsoft.Data.SqlClient;
using DocumentUpload = Angle.Models.DocumentUpload;

using System.Data;

namespace Angle.Controllers
{
    public class DocumentUploadController : Controller
    {
        private readonly IDocumentUploadService documentuploadService;
        private readonly IMapper mapper;

        public DocumentUploadController(IDocumentUploadService documentuploadService,
            IMapper mapper)
        {
            this.documentuploadService = documentuploadService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await documentuploadService.GetAll();
            var listDocumentUpload = data.Select(x => mapper.Map<DocumentUploadDTO>(x)).ToList();
            return View(listDocumentUpload);
        }

        public async Task<IActionResult> FileUpload(List<IFormFile> files, string Module, int ModuleID)
        {
            var size = files.Sum(f => f.Length);
            int IDModulo = ModuleID;
            string Modulo = Module;
            var filePaths = new List<string>();
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    var filePath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\files", formFile.FileName);
                    filePaths.Add(filePath);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                        SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                        conexion.Open();
                        string Attachment = formFile.FileName;

                        string cadena = "insert into [dbo].[FT_DocumentUpload]([DateUpload],[Module],[Attachment],[ModuleID]) values ('2021-05-02','" + Modulo + "','" + Attachment + "' ," + IDModulo + ")";
                        SqlCommand comando = new SqlCommand(cadena, conexion);
                        comando.ExecuteNonQuery();
                        conexion.Close();
                    }
                }
            }

            return RedirectToAction("Index", "Customer");
        }


        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats officedocument.spreadsheetml.sheet"},  
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }

        public async Task<IActionResult> Download(string filename)
        {
            if (filename == null)
                return Content("filename not present");

            var path = Path.Combine(Directory.GetCurrentDirectory(),"wwwroot/files", filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(path), Path.GetFileName(path));
        }

        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await documentuploadService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DocumentUploadDTO documentuploadDTO)
        {

            if (ModelState.IsValid)
            {

                var documentupload = mapper.Map<DocumentUpload>(documentuploadDTO);

                documentupload = await documentuploadService.Insert(documentupload);

                return RedirectToAction("Index");
            }

            return View(documentuploadDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var documentupload = await documentuploadService.GetById(id.Value);

            if (documentupload == null)
                return NotFound();

            ViewBag.Status = await documentuploadService.GetStatus();
            var documentuploadDTO = mapper.Map<DocumentUploadDTO>(documentupload);
            return View(documentuploadDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(DocumentUploadDTO documentuploadDTO)
        {
            if (ModelState.IsValid)
            {
                var documentupload = mapper.Map<DocumentUpload>(documentuploadDTO);

                documentupload = await documentuploadService.Update(documentupload);

                return RedirectToAction("Index");
            }

            return View(documentuploadDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var documentupload = await documentuploadService.GetById(id.Value);

            if (documentupload == null)
                return NotFound();

            var documentuploadDTO = mapper.Map<DocumentUploadDTO>(documentupload);

            return View(documentuploadDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await documentuploadService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}