﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class HistoricFuel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HistoricFuelID { get; set; }
		public DateTime DateChargeVehicle { get; set; }
		public int Charge { get; set; }
		public Fuel Fuel { get; set; }
		public int FuelID { get; set; }
		public Driver Driver { get; set; }
		public int DriverID { get; set; }
		public Vehicles Vehicles { get; set; }
		public int VehiclesID { get; set; }
	}
}