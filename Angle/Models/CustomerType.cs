﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class CustomerType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomerTypeID { get; set; }
        public string CustomerTypeName { get; set; }
        public string CustomerTypeDescription { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
