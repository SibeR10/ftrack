﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class AccountTypeDTO
    {
        public int AccountTypeID { get; set; }

		[Display(Name = "Tipo de cuenta")]
		public string AccountTypeName { get; set; }
		
		[Display(Name = "Estatus")]
		public int StatusID { get; set; }

	
	}
}