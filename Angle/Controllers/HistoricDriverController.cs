﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;
using HistoricDriver = Angle.Models.HistoricDriver;
using Angle.Services.Implements;
using System;
using Microsoft.Data.SqlClient;

namespace Angle.Controllers
{
    public class HistoricDriverController : Controller
    {
        private readonly IHistoricDriverService historicdriverService;
        private readonly IMapper mapper;

        public HistoricDriverController(IHistoricDriverService historicdriverService,
            IMapper mapper)
        {
            this.historicdriverService = historicdriverService;
            this.mapper = mapper;
        }


        public async Task<IActionResult> CreateAsync(int? id)
        {

            ViewBag.Driver = await historicdriverService.GetDriver();
            ViewBag.Status = await historicdriverService.GetStatus();
            ViewBag.Vehicles = await historicdriverService.GetVehiclesById(id.Value);

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(HistoricDriverDTO historicdriverDTO)
        {


            if (ModelState.IsValid)
            {


                var historicdriver = mapper.Map<HistoricDriver>(historicdriverDTO);
                var vehicle = historicdriver.VehiclesID;
                var driver = historicdriver.DriverID;

                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();
                string cadena = "UPDATE [dbo].[FT_Vehicles] SET [DriverID] = " + driver + " WHERE VehiclesID = " + vehicle + "";
                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();
                conexion.Close();

                historicdriver = await historicdriverService.Insert(historicdriver);

                return RedirectToAction("Index", "Vehicles");
            }

            return View(historicdriverDTO);
        }



        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var historicdriver = await historicdriverService.GetById(id.Value);

            if (historicdriver == null)
                return NotFound();

            var historicdriverDTO = mapper.Map<HistoricDriverDTO>(historicdriver);
            return View(historicdriverDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(HistoricDriverDTO historicdriverDTO)
        {
            if (ModelState.IsValid)
            {
                var historicdriver = mapper.Map<HistoricDriver>(historicdriverDTO);

                historicdriver = await historicdriverService.Update(historicdriver);

                return RedirectToAction("Index");
            }

            return View(historicdriverDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var historicdriver = await historicdriverService.GetById(id.Value);

            if (historicdriver == null)
                return NotFound();

            var historicdriverDTO = mapper.Map<HistoricDriverDTO>(historicdriver);

            return View(historicdriverDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await historicdriverService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}