﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ChargesByQuote
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ChargesByQuoteID { get; set; }
        public MerchandiseType MerchandiseType { get; set; }
        public int MerchandiseTypeID { get; set; }
        public Quote Quote { get; set; }
        public int QuoteID { get; set; }
        public ServiceType ServiceType { get; set; }
        public int ServiceTypeID { get; set; }
        public ApplyBy ApplyBy { get; set; }
        public int ApplyByID { get; set; }
        public MeasureUnit MeasureUnit { get; set; }
        public int MeasureUnitID { get; set; }
        public int ValueCharge { get; set; }
        public double Tariff { get; set; }
    }
}	
