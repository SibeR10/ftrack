﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Driver
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DriverID { get; set; }
        public string DriverName { get; set; }
        public string DriverLastName { get; set; }
        public int Document { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
        public Division Division { get; set; }
        public int DivisionID { get; set; }
        public DriverType DriverType { get; set; }
        public int DriverTypeID { get; set; }
        public DocumentType DocumentType { get; set; }
        public int DocumentTypeID { get; set; }

        
    }
}
