﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IDriverService : IGenericService<Driver>
    {
        Task<IEnumerable<Status>> GetStatus();
        Task<IEnumerable<Division>> GetDivision();
        Task<IEnumerable<DriverType>> GetDriverType();
        Task<IEnumerable<DocumentType>> GetDocumentType();
        Task<IEnumerable<Status>> GetStatusByDriver(int id);
        Task<IEnumerable<Division>> GetDivisionByDriver(int id);
        Task<IEnumerable<DriverType>> GetDriverTypeByDriver(int id);
        Task<IEnumerable<DocumentType>> GetDocumentTypeByDriver(int id);

        
    }
}
