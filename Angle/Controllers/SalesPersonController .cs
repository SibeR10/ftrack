﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using SalesPerson = Angle.Models.SalesPerson;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class SalesPersonController : Controller
    {
        private readonly ISalesPersonService salespersonService;
        private readonly IMapper mapper;

        public SalesPersonController(ISalesPersonService salespersonService,
            IMapper mapper)
        {
            this.salespersonService = salespersonService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await salespersonService.GetAll();
            ViewBag.Status = await salespersonService.GetStatus();
            var listSalesPerson = data.Select(x => mapper.Map<SalesPersonDTO>(x)).ToList();
            return View(listSalesPerson);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await salespersonService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SalesPersonDTO salespersonDTO)
        {
            
            if (ModelState.IsValid)
            {

                var salesperson = mapper.Map<SalesPerson>(salespersonDTO);

                salesperson = await salespersonService.Insert(salesperson);

                return RedirectToAction("Index");
            }

            return View(salespersonDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var salesperson = await salespersonService.GetById(id.Value);

            if (salesperson == null)
                return NotFound();

            ViewBag.Status = await salespersonService.GetStatus();
            var salespersonDTO = mapper.Map<SalesPersonDTO>(salesperson);
            return View(salespersonDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SalesPersonDTO salespersonDTO)
        {
            if (ModelState.IsValid)
            {
                var salesperson = mapper.Map<SalesPerson>(salespersonDTO);

                salesperson = await salespersonService.Update(salesperson);

                return RedirectToAction("Index");
            }

            return View(salespersonDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var salesperson = await salespersonService.GetById(id.Value);

            if (salesperson == null)
                return NotFound();

            var salespersonDTO = mapper.Map<SalesPersonDTO>(salesperson);

            return View(salespersonDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await salespersonService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}