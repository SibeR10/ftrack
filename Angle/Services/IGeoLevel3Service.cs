﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IGeoLevel3Service : IGenericService<GeoLevel3>
    {
        Task<IEnumerable<GeoLevel2>> GetGeoLevel2();

        Task<IEnumerable<GeoLevel2>> GetGeoLevel2ByGeoLevel3(int id);
    }
}
