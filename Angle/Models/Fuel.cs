﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Fuel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FuelID { get; set; }
        public string FuelType { get; set; }
        public string FuelDescription { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
