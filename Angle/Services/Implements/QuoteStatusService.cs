﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Services.Implements
{
    public class QuoteStatusService : GenericService<QuoteStatus>, IQuoteStatusService
    {
        private IQuoteStatusRepository QuoteStatusRepository;

        public QuoteStatusService(IQuoteStatusRepository QuoteStatusRepository) : base(QuoteStatusRepository)
        {
            this.QuoteStatusRepository = QuoteStatusRepository;
        }

        public Task Insert(List<QuoteStatus> list)
        {
            throw new System.NotImplementedException();
        }
    }
}
