﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class PaymentTypeService : GenericService<PaymentType>, IPaymentTypeService
    {
        private IPaymentTypeRepository paymenttypeRepository;

        public PaymentTypeService(IPaymentTypeRepository paymenttypeRepository) : base(paymenttypeRepository)
        {
            this.paymenttypeRepository = paymenttypeRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await paymenttypeRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByPaymentType(int id)
        {
            return await paymenttypeRepository.GetStatusByPaymentType(id);
        }

    }
}
