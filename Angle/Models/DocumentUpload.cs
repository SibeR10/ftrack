﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations.Schema;
namespace Angle.Models
{
    public class DocumentUpload
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DocumentUploadID { get; set; }
        public DateTime DateUpload { get; set; }
        public string Module { get; set; }
        public string Attachment { get; set; }
        public int ModuleID { get; set; }

    }
}
