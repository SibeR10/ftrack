﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Warehouse
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WarehouseID { get; set; }
        public string WarehouseName { get; set; }
        public string WarehouseDescription { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
        public Division Division { get; set; }
        public int DivisionID { get; set; }
    }
}
