﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class ItemsQuoteRepository : GenericRepository<ItemsQuote>, IItemsQuoteRepository
    {
        private readonly FTContext FTContext;

        public ItemsQuoteRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {

            var listGeoLevel1 = await FTContext.GeoLevel1.ToListAsync();

            return listGeoLevel1;
        }
        public async Task<IEnumerable<Customer>> GetCustomer()
        {

            var listCustomer = await FTContext.Customer.ToListAsync();

            return listCustomer;
        }
        public async Task<IEnumerable<SalesPerson>> GetSalesPerson()
        {

            var listSalesPerson = await FTContext.SalesPerson.ToListAsync();

            return listSalesPerson;
        }
        public async Task<IEnumerable<Division>> GetDivision()
        {

            var listDivision = await FTContext.Division.ToListAsync();

            return listDivision;
        }
        public async Task<IEnumerable<AddressType>> GetAddressType()
        {

            var listAddressType = await FTContext.AddressType.ToListAsync();

            return listAddressType;
        }
        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {

            var listWarehouse = await FTContext.Warehouse.ToListAsync();

            return listWarehouse;
        }
        public async Task<IEnumerable<Country>> GetCountry()
        {

            var listCountry = await FTContext.Country.ToListAsync();

            return listCountry;
        }
        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {

            var listServiceType = await FTContext.ServiceType.ToListAsync();

            return listServiceType;
        }

        

    }
}
