﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class MeasureUnitDTO
    {
        public int MeasureUnitID { get; set; }

        [Required(ErrorMessage = "The MeasureUnitName is required")]
        [Display(Name = "Nombre")]
        public string MeasureUnitName { get; set; }

        [Required(ErrorMessage = "The MeasureUnitName is required")]
        [Display(Name = "Descripcion")]
        public string MeasureUnitDescription { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

