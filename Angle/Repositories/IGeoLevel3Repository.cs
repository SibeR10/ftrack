﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IGeoLevel3Repository : IGenericRepository<GeoLevel3>
    {
        Task<IEnumerable<GeoLevel2>> GetGeoLevel2();

        Task<IEnumerable<GeoLevel2>> GetGeoLevel2ByGeoLevel3(int id);
    }
}
