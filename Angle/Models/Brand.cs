﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Brand
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BrandID { get; set; }
        public string BrandDescription { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
