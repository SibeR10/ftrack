﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class DriverRepository : GenericRepository<Driver>, IDriverRepository
    {
        private readonly FTContext FTContext;

        public DriverRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }

        public async Task<IEnumerable<Status>> GetStatusByDriver(int id)
        {

            var listStatusByDriver = await FTContext.Driver
                .Include(x => x.Status)
                .Where(x => x.StatusID == id)
                .Select(x => x.Status)
                .ToListAsync();

            return listStatusByDriver;
        }


        public async Task<IEnumerable<Division>> GetDivision()
        {

            var listDivision = await FTContext.Division.ToListAsync();

            return listDivision;
        }

        public async Task<IEnumerable<Division>> GetDivisionByDriver(int id)
        {

            var listDivisionByDriver = await FTContext.Driver
                .Include(x => x.Division)
                .Where(x => x.DivisionID == id)
                .Select(x => x.Division)
                .ToListAsync();

            return listDivisionByDriver;
        }

        public async Task<IEnumerable<DriverType>> GetDriverType()
        {

            var listDriverType = await FTContext.DriverType.ToListAsync();

            return listDriverType;
        }

        public async Task<IEnumerable<DriverType>> GetDriverTypeByDriver(int id)
        {

            var listDriverTypeByDriver = await FTContext.Driver
                .Include(x => x.DriverType)
                .Where(x => x.DriverTypeID == id)
                .Select(x => x.DriverType)
                .ToListAsync();

            return listDriverTypeByDriver;
        }

        public async Task<IEnumerable<DocumentType>> GetDocumentType()
        {

            var listDocumentType = await FTContext.DocumentType.ToListAsync();

            return listDocumentType;
        }

        public async Task<IEnumerable<DocumentType>> GetDocumentTypeByDriver(int id)
        {

            var listDocumentTypeByDriver = await FTContext.Driver
                .Include(x => x.DocumentType)
                .Where(x => x.DocumentTypeID == id)
                .Select(x => x.DocumentType)
                .ToListAsync();

            return listDocumentTypeByDriver;
        }

        
    }
}
