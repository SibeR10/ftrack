﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class VehiclesDTO
    {
        public int VehiclesID { get; set; }

		[Display(Name = "Placa")]
		public string LicensePlateCar { get; set; }

		[Display(Name = "Estatus")]
		public int StatusID { get; set; }

		[Display(Name = "Capacidad")]
		public int Capacity { get; set; }

		
		[Display(Name = "Matricula")]
		public string Enrollment { get; set; }

		[Display(Name = "Color")]
		public string ColourCar { get; set; }

		[Display(Name = "Equipo")]
		public string Equipment { get; set; }

		[Display(Name = "Modelo")]
		public string ModelCar { get; set; }

		[Display(Name = "Año")]
		public int YearCar { get; set; }

		[Display(Name = "Chasis")]
		public string ChassisCar { get; set; }

		[Display(Name = "Seguro/Póliza")]
		public string InsuranceCar { get; set; }
		
		[Display(Name = "Fecha de expiración de la póliza")]
		public DateTime ExpirationInsuranceCar { get; set; }

		[Display(Name = "Fecha de creación del registro")]
		public DateTime CreateDateTransaction { get; set; }

		[Display(Name = "Usuario creador del registro")]
		public string CreateUserTransaction { get; set; }

		[Display(Name = "Actualizado")]
		public DateTime UpdateDateTransaction { get; set; }

		[Display(Name = "Modificado por")]
		public string UpdateUserTransaction { get; set; }


		[Display(Name = "División")]
		public int DivisionID { get; set; }

		[Display(Name = "Marca")]
		public int BrandID { get; set; }

		[Display(Name = "Chofer")]
		public int DriverID { get; set; }
	}
}