﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IItemsQuoteService : IGenericService<ItemsQuote>
    {
        Task<IEnumerable<Warehouse>> GetWarehouse();
        Task<IEnumerable<Country>> GetCountry();
        Task<IEnumerable<Customer>> GetCustomer();
        Task<IEnumerable<SalesPerson>> GetSalesPerson();
        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();
        Task<IEnumerable<Division>> GetDivision();
        Task<IEnumerable<AddressType>> GetAddressType();
        Task<IEnumerable<ServiceType>> GetServiceType();

    }
}
