﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Currency = Angle.Models.Currency;
namespace Angle.Controllers
{
    public class CurrencyController : Controller
    {
        private readonly ICurrencyService currencyService;
        private readonly IMapper mapper;

        public CurrencyController(ICurrencyService currencyService,
            IMapper mapper)
        {
            this.currencyService = currencyService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await currencyService.GetAll();

            var listCurrency = data.Select(x => mapper.Map<CurrencyDTO>(x)).ToList();

            return View(listCurrency);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CurrencyDTO currencyDTO)
        {
            if (ModelState.IsValid)
            {

                var currency = mapper.Map<Currency>(currencyDTO);

                currency = await currencyService.Insert(currency);


                return RedirectToAction("Index");
            }

            return View(currencyDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var currency = await currencyService.GetById(id.Value);

            if (currency == null)
                return NotFound();

            var currencyDTO = mapper.Map<CurrencyDTO>(currency);

            return View(currencyDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CurrencyDTO currencyDTO)
        {
            if (ModelState.IsValid)
            {
                var currency = mapper.Map<Currency>(currencyDTO);

                currency = await currencyService.Update(currency);
                return RedirectToAction("Index");
            }

            return View(currencyDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var currency = await currencyService.GetById(id.Value);

            if (currency == null)
                return NotFound();

            var currencyDTO = mapper.Map<CurrencyDTO>(currency);

            return View(currencyDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await currencyService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}