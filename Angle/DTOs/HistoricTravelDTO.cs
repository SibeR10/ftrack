﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class HistoricTravelDTO
    {
        public int HistoricTravelID { get; set; }

        public int TravelID { get; set; }
        public int ServiceOrderID { get; set; }
    }
}