﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class GeoLevel3Service : GenericService<GeoLevel3>, IGeoLevel3Service
    {
        private IGeoLevel3Repository geolevel3Repository;

        public GeoLevel3Service(IGeoLevel3Repository geolevel3Repository) : base(geolevel3Repository)
        {
            this.geolevel3Repository = geolevel3Repository;
        }

        public async Task<IEnumerable<GeoLevel2>> GetGeoLevel2()
        {
            return await geolevel3Repository.GetGeoLevel2();
        }

        public async Task<IEnumerable<GeoLevel2>> GetGeoLevel2ByGeoLevel3(int id)
        {
            return await geolevel3Repository.GetGeoLevel2ByGeoLevel3(id);
        }

    }
}
