﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class AddressTypeService : GenericService<AddressType>, IAddressTypeService
    {
        private IAddressTypeRepository addresstypeRepository;

        public AddressTypeService(IAddressTypeRepository addresstypeRepository) : base(addresstypeRepository)
        {
            this.addresstypeRepository = addresstypeRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await addresstypeRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByAddressType(int id)
        {
            return await addresstypeRepository.GetStatusByAddressType(id);
        }

    }
}
