﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IVehiclesRepository : IGenericRepository<Vehicles>
    {
        Task<IEnumerable<Status>> GetStatus();
        Task<IEnumerable<DocumentUpload>> GetDocumentUpload();
        Task<IEnumerable<Status>> GetStatusByVehicles(int id);
        Task<IEnumerable<Division>> GetDivision();
        Task<IEnumerable<Division>> GetDivisionByVehicles(int id);
        Task<IEnumerable<Brand>> GetBrand();
        Task<IEnumerable<Brand>> GetBrandByVehicles(int id);
        Task<IEnumerable<Driver>> GetDriver();
        Task<IEnumerable<Driver>> GetDriverByVehicles(int id);
        Task<IEnumerable<HistoricDriver>> GetHistoricDriver();
        Task<IEnumerable<HistoricDriver>> GetHistoricDriverByVehicle(int id);
        Task<IEnumerable<HistoricFuel>> GetHistoricFuel();
        Task<IEnumerable<HistoricFuel>> GetHistoricFuelByVehicle(int id);
        Task<IEnumerable<Fuel>> GetFuel();

        

    }
}
