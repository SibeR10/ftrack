﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IGeoLevel2Service : IGenericService<GeoLevel2>
    {
        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();

        Task<IEnumerable<GeoLevel1>> GetGeoLevel1ByGeoLevel2(int id);
    }
}
