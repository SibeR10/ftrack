﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class GeoLevel2DTO
    {
        public int GeoLevel2ID { get; set; }

        [Required(ErrorMessage = "The GeoLevel2Name is required")]
        [Display(Name = "Geolocalizacion Nivel 2")]
        public string GeoLevel2Name { get; set; }

        [Required(ErrorMessage = "The GeoLevel2Name is required")]
        [Display(Name = "Titulo")]
        public string GeoLevel2Title { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int GeoLevel1ID { get; set; }
    }
}

