﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class MeasureUnitService : GenericService<MeasureUnit>, IMeasureUnitService
    {
        private IMeasureUnitRepository measureunitRepository;

        public MeasureUnitService(IMeasureUnitRepository measureunitRepository) : base(measureunitRepository)
        {
            this.measureunitRepository = measureunitRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await measureunitRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByMeasureUnit(int id)
        {
            return await measureunitRepository.GetStatusByMeasureUnit(id);
        }

    }
}
