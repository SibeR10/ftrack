﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Warehouse = Angle.Models.Warehouse;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class WarehouseController : Controller
    {
        private readonly IWarehouseService warehouseService;
        private readonly IMapper mapper;

        public WarehouseController(IWarehouseService warehouseService,
            IMapper mapper)
        {
            this.warehouseService = warehouseService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await warehouseService.GetAll();
            ViewBag.Status = await warehouseService.GetStatus();
            ViewBag.Division = await warehouseService.GetDivision();
            var listWarehouse = data.Select(x => mapper.Map<WarehouseDTO>(x)).ToList();
            return View(listWarehouse);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Division = await warehouseService.GetDivision();
            ViewBag.Status = await warehouseService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(WarehouseDTO warehouseDTO)
        {
            
            if (ModelState.IsValid)
            {

                var warehouse = mapper.Map<Warehouse>(warehouseDTO);

                warehouse = await warehouseService.Insert(warehouse);

                return RedirectToAction("Index");
            }

            return View(warehouseDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var warehouse = await warehouseService.GetById(id.Value);

            if (warehouse == null)
                return NotFound();

            ViewBag.Division = await warehouseService.GetDivision();
            ViewBag.Status = await warehouseService.GetStatus();
            var warehouseDTO = mapper.Map<WarehouseDTO>(warehouse);
            return View(warehouseDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(WarehouseDTO warehouseDTO)
        {
            if (ModelState.IsValid)
            {
                var warehouse = mapper.Map<Warehouse>(warehouseDTO);

                warehouse = await warehouseService.Update(warehouse);

                return RedirectToAction("Index");
            }

            return View(warehouseDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var warehouse = await warehouseService.GetById(id.Value);

            if (warehouse == null)
                return NotFound();

            var warehouseDTO = mapper.Map<WarehouseDTO>(warehouse);

            return View(warehouseDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await warehouseService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}