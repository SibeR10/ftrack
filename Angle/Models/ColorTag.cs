﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations.Schema;
namespace Angle.Models
{
    public class ColorTag
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ColorTagID { get; set; }
        public string ColorTagName { get; set; }
        public string ColorTagDescription { get; set; }
    }
}
