﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ContractSpecialSequence
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int ContractSpecialSequenceID { get; set; }
        public int WeightMin { get; set; }
        public int BasePrice { get; set; }
        public int ExtraPrice { get; set; }
        public ContractSpecial ContractSpecial { get; set; }
        public int ContractSpecialID { get; set; }
        public MerchandiseType MerchandiseType { get; set; }
        public int MerchandiseTypeID { get; set; }
        public DeliveryMode DeliveryMode { get; set; }
        public int DeliveryModeID { get; set; }
        public GeoLevel1 Origin { get; set; }
        public int OriginID { get; set; }
        public GeoLevel1 Destination { get; set; }
        public int DestinationID { get; set; }
        public Customer Customer { get; set; }
        public int CustomerID { get; set; }
        public ServiceType ServiceType { get; set; }
        public int ServiceTypeID { get; set; }
       
    }
}
