﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using TransportType = Angle.Models.TransportType;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class TransportTypeController : Controller
    {
        private readonly ITransportTypeService transporttypeService;
        private readonly IMapper mapper;

        public TransportTypeController(ITransportTypeService transporttypeService,
            IMapper mapper)
        {
            this.transporttypeService = transporttypeService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await transporttypeService.GetAll();
            ViewBag.Status = await transporttypeService.GetStatus();
            var listTransportType = data.Select(x => mapper.Map<TransportTypeDTO>(x)).ToList();
            return View(listTransportType);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await transporttypeService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(TransportTypeDTO transporttypeDTO)
        {
            
            if (ModelState.IsValid)
            {

                var transporttype = mapper.Map<TransportType>(transporttypeDTO);

                transporttype = await transporttypeService.Insert(transporttype);

                return RedirectToAction("Index");
            }

            return View(transporttypeDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var transporttype = await transporttypeService.GetById(id.Value);

            if (transporttype == null)
                return NotFound();

            ViewBag.Status = await transporttypeService.GetStatus();
            var transporttypeDTO = mapper.Map<TransportTypeDTO>(transporttype);
            return View(transporttypeDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TransportTypeDTO transporttypeDTO)
        {
            if (ModelState.IsValid)
            {
                var transporttype = mapper.Map<TransportType>(transporttypeDTO);

                transporttype = await transporttypeService.Update(transporttype);

                return RedirectToAction("Index");
            }

            return View(transporttypeDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var transporttype = await transporttypeService.GetById(id.Value);

            if (transporttype == null)
                return NotFound();

            var transporttypeDTO = mapper.Map<TransportTypeDTO>(transporttype);

            return View(transporttypeDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await transporttypeService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}