﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class GeoLevel1DTO
    {
        public int GeoLevel1ID { get; set; }
        public int GeoLevel1P { get; set; }

        [Required(ErrorMessage = "The GeoLevel1Name is required")]
        [Display(Name = "Ubicación")]
        public string GeoLevel1Name { get; set; }

        [Required(ErrorMessage = "The GeoLevel1Title is required")]
        [Display(Name = "Zona")]
        public string GeoLevel1Title { get; set; }

        [Required(ErrorMessage = "The Pais is required")]
        [Display(Name = "Pais")]
        public int CountryID { get; set; }
    }
}

