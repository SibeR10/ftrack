﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Driver = Angle.Models.Driver;
using Angle.Services.Implements;
using System.Data;
using Microsoft.Data.SqlClient;

namespace Angle.Controllers
{
    public class DriverController : Controller
    {
        private readonly IDriverService driverService;
        private readonly IMapper mapper;

        public DriverController(IDriverService driverService,
            IMapper mapper)
        {
            this.driverService = driverService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await driverService.GetAll();
            ViewBag.Status = await driverService.GetStatus();
            ViewBag.Division = await driverService.GetDivision();
            ViewBag.DriverType = await driverService.GetDriverType();
            ViewBag.DocumentType = await driverService.GetDocumentType();
            var listDriver = data.Select(x => mapper.Map<DriverDTO>(x)).ToList();
            return View(listDriver);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await driverService.GetStatus();
            ViewBag.Division = await driverService.GetDivision();
            ViewBag.DriverType = await driverService.GetDriverType();
            ViewBag.DocumentType = await driverService.GetDocumentType();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DriverDTO driverDTO)
        {
            
            if (ModelState.IsValid)
            {

                var driver = mapper.Map<Driver>(driverDTO);

                driver = await driverService.Insert(driver);

                return RedirectToAction("Index");
            }

            return View(driverDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var driver = await driverService.GetById(id.Value);

            if (driver == null)
                return NotFound();

            ViewBag.Status = await driverService.GetStatus();
            ViewBag.Division = await driverService.GetDivision();
            ViewBag.DriverType = await driverService.GetDriverType();
            ViewBag.DocumentType = await driverService.GetDocumentType();
            var driverDTO = mapper.Map<DriverDTO>(driver);
            return View(driverDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(DriverDTO driverDTO)
        {
            if (ModelState.IsValid)
            {
                var driver = mapper.Map<Driver>(driverDTO);

                driver = await driverService.Update(driver);

                return RedirectToAction("Index");
            }

            return View(driverDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var driver = await driverService.GetById(id.Value);

            if (driver == null)
                return NotFound();

            var driverDTO = mapper.Map<DriverDTO>(driver);

            return View(driverDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await driverService.Delete(id.Value);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Import(IFormFile file)
        {
            var list = new List<Driver>();
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowcount = worksheet.Dimension.Rows;
                    for (int row = 2; row <= rowcount; row++)
                    {
                        list.Add(new Driver
                        {
                            DocumentTypeID = (int)(double)worksheet.Cells[row, 1].Value,
                            Document = (int)(double)worksheet.Cells[row, 2].Value,
                            DriverName = worksheet.Cells[row, 3].Value.ToString().Trim(),
                            DriverLastName = worksheet.Cells[row, 4].Value.ToString().Trim(),
                            StatusID = (int)(double)worksheet.Cells[row, 5].Value,
                            DivisionID = (int)(double)worksheet.Cells[row, 6].Value,
                            DriverTypeID = (int)(double)worksheet.Cells[row, 7].Value,
                        });
                    }
                }
            }

            list.ForEach(p =>
            {

                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();
                int DocumentTypeID = p.DocumentTypeID;
                int Document = p.Document;
                string DriverName = p.DriverName;
                string DriverLastName = p.DriverLastName;
                int StatusID = p.StatusID;
                int DivisionID = p.DivisionID;
                int DriverTypeID = p.DriverTypeID;
                string cadena = "insert into [dbo].[FT_Driver]([DocumentTypeID],[Document],[DriverName],[DriverLastName],[StatusID],[DivisionID],[DriverTypeID]) " + "values (" + DocumentTypeID + "," + Document + ",'" + DriverName + "','" + DriverLastName + "'" + "," + StatusID + "," + DivisionID + "," + DriverTypeID + ")";
                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();
                conexion.Close();

            });


            return RedirectToAction("Index");
        }
    }
}