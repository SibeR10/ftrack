﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class AddressTypeDTO
    {
        public int AddressTypeID { get; set; }

        [Required(ErrorMessage = "The AddressTypeName is required")]
        [Display(Name = "Tipo de dirección")]
        public string AddressTypeName { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

