﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class GeoLevel3DTO
    {
        public int GeoLevel3ID { get; set; }

        [Required(ErrorMessage = "The GeoLevel3Name is required")]
        [Display(Name = "Geolocalizacion Nivel 3")]
        public string GeoLevel3Name { get; set; }

        [Required(ErrorMessage = "The GeoLevel3Name is required")]
        [Display(Name = "Titulo")]
        public string GeoLevel3Title { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int GeoLevel2ID { get; set; }
    }
}

