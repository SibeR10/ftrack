// Sweet Alert
// -----------------------------------

(function() {
    'use strict';

    $(initSweetAlert);



    function initSweetAlert() {

        $('#formEstatus').on('submit', function () {
            if ($('#formEstatus').parsley()) {
                swal("Estatus", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formMoneda').on('submit', function () {
            if ($('#formMoneda').parsley()) {
                swal("Moneda", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formDivision').on('submit', function () {
            if ($('#formDivision').parsley()) {
                swal("Division", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formDocumentType').on('submit', function () {
            if ($('#formDocumentType').parsley()) {
                swal("Tipo de documento", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formCustomerType').on('submit', function () {
            if ($('#formCustomerType').parsley()) {
                swal("Tipo de cliente", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formBrand').on('submit', function () {
            if ($('#formBrand').parsley()) {
                swal("Marca", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formFuel').on('submit', function () {
            if ($('#formFuel').parsley()) {
                swal("Combustible", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formDriverType').on('submit', function () {
            if ($('#formDriverType').parsley()) {
                swal("Tipo de chofer", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formPaymentType').on('submit', function () {
            if ($('#formPaymentType').parsley()) {
                swal("Formas de pago", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formPaymentTerms').on('submit', function () {
            if ($('#formPaymentTerms').parsley()) {
                swal("Terminos de pago", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formPais').on('submit', function () {
            if ($('#formPais').parsley()) {
                swal("Pais", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formGeoLevel1').on('submit', function () {
            if ($('#formGeoLevel1').parsley()) {
                swal("Geolocalizacion: Nivel 1", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formGeoLevel2').on('submit', function () {
            if ($('#formGeoLevel2').parsley()) {
                swal("Geolocalizacion: Nivel 2", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formGeoLevel3').on('submit', function () {
            if ($('#formGeoLevel3').parsley()) {
                swal("Geolocalizacion: Nivel 3", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formAddressType').on('submit', function () {
            if ($('#formAddressType').parsley()) {
                swal("Tipo de direccion", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formSalesPerson').on('submit', function () {
            if ($('#formSalesPerson').parsley()) {
                swal("Vendedor", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formDriver').on('submit', function () {
            if ($('#formDriver').parsley()) {
                swal("Chofer", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formVehicles').on('submit', function () {
            if ($('#formVehicles').parsley()) {
                swal("Vehiculo", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formHistoricDriver').on('submit', function () {
            if ($('#formHistoricDriver').parsley()) {
                swal("Chofer", "Se asign� chofer al vehiculo satisfactoriamente", "success");
            }
        });

        $('#formHistoricFuel').on('submit', function () {
            if ($('#formHistoricFuel').parsley()) {
                swal("Combustible", "Se asign� combustible al vehiculo satisfactoriamente", "success");
            }
        });

        $('#formCustomer').on('submit', function () {
            if ($('#formCustomer').parsley()) {
                swal("Cliente", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formContact').on('submit', function () {
            if ($('#formContact').parsley()) {
                swal("Contacto", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formAddress').on('submit', function () {
            if ($('#formAddress').parsley()) {
                swal("Direcci&oacute;n", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formEstatusCotizacion').on('submit', function () {
            if ($('#formEstatusCotizacion').parsley()) {
                swal("Estatus para cotizaciones", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formAlmacen').on('submit', function () {
            if ($('#formAlmacen').parsley()) {
                swal("Sucursal", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formAplicadoPor').on('submit', function () {
            if ($('#formAplicadoPor').parsley()) {
                swal("Aplicado por", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formUnidadDeMedida').on('submit', function () {
            if ($('#formUnidadDeMedida').parsley()) {
                swal("Unidad de medida", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formTipoServicio').on('submit', function () {
            if ($('#formTipoServicio').parsley()) {
                swal("Tipo de servicio", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formTipoTransporte').on('submit', function () {
            if ($('#formTipoTransporte').parsley()) {
                swal("Tipo de transporte", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formTipoMercancia').on('submit', function () {
            if ($('#formTipoMercancia').parsley()) {
                swal("Tipo de mercancia", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formModalidadEntrega').on('submit', function () {
            if ($('#formModalidadEntrega').parsley()) {
                swal("Modalidad de entrega", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formClaseServicio').on('submit', function () {
            if ($('#formClaseServicio').parsley()) {
                swal("Clase de servicio", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formTarifaRegular').on('submit', function () {
            if ($('#formTarifaRegular').parsley()) {
                swal("Tarifa regular", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formTarifaEspecial').on('submit', function () {
            if ($('#formTarifaEspecial').parsley()) {
                swal("Tarifa especial", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formTarifaRegularSequence').on('submit', function () {
            if ($('#formTarifaRegularSequence').parsley()) {
                swal("Tarifa regular", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formTarifaEspecialSequence').on('submit', function () {
            if ($('#formTarifaEspecialSequence').parsley()) {
                swal("Tarifa especial", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formCotizacion').on('submit', function () {
            if ($('#formCotizacion').parsley()) {
                swal("Cotizaci�n", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formTipoCuenta').on('submit', function () {
            if ($('#formTipoCuenta').parsley()) {
                swal("Tipo de cuenta", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formEstatusOrdenServicio').on('submit', function () {
            if ($('#formEstatusOrdenServicio').parsley()) {
                swal("Estatus para Orden de Servicio", "Registro creado satisfactoriamente", "success");
            }
        });

        $('#formViajes').on('submit', function () {
            if ($('#formViajes').parsley()) {
                swal("Viaje", "Registro creado satisfactoriamente", "success");
            }
        });


        $('#swal-demo1').on('click', function(e) {
            e.preventDefault();
            swal("Here's a message!")
        });

        $('#swal-demo2').on('click', function(e) {
            e.preventDefault();
            swal("Here's a message!", "It's pretty, isn't it?")
        });

        $('#swal-demo3').on('click', function(e) {
            e.preventDefault();
            swal("Good job!", "You clicked the button!", "success")
        });

        $('#swal-demo4').on('click', function(e) {
            e.preventDefault();
            swal({
                title: 'Are you sure?',
                text: 'Your will not be able to recover this imaginary file!',
                icon: 'warning',
                buttons: {
                    cancel: true,
                    confirm: {
                        text: 'Yes, delete it!',
                        value: true,
                        visible: true,
                        className: "bg-danger",
                        closeModal: true
                    }
                }
            }).then(function() {
                swal('Booyah!');
            });

        });

        $('#swal-demo5').on('click', function(e) {
            e.preventDefault();
            swal({
                title: 'Are you sure?',
                text: 'Your will not be able to recover this imaginary file!',
                icon: 'warning',
                buttons: {
                    cancel: {
                        text: 'No, cancel plx!',
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false
                    },
                    confirm: {
                        text: 'Yes, delete it!',
                        value: true,
                        visible: true,
                        className: "bg-danger",
                        closeModal: false
                    }
                }
            }).then(function(isConfirm) {
                if (isConfirm) {
                    swal('Deleted!', 'Your imaginary file has been deleted.', 'success');
                } else {
                    swal('Cancelled', 'Your imaginary file is safe :)', 'error');
                }
            });

        });

    }

})();