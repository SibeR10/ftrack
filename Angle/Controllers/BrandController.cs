﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Brand = Angle.Models.Brand;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class BrandController : Controller
    {
        private readonly IBrandService brandService;
        private readonly IMapper mapper;

        public BrandController(IBrandService brandService,
            IMapper mapper)
        {
            this.brandService = brandService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await brandService.GetAll();
            ViewBag.Status = await brandService.GetStatus();
            var listBrand = data.Select(x => mapper.Map<BrandDTO>(x)).ToList();
            return View(listBrand);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await brandService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(BrandDTO brandDTO)
        {
            
            if (ModelState.IsValid)
            {

                var brand = mapper.Map<Brand>(brandDTO);

                brand = await brandService.Insert(brand);

                return RedirectToAction("Index");
            }

            return View(brandDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var brand = await brandService.GetById(id.Value);

            if (brand == null)
                return NotFound();

            ViewBag.Status = await brandService.GetStatus();
            var brandDTO = mapper.Map<BrandDTO>(brand);
            return View(brandDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(BrandDTO brandDTO)
        {
            if (ModelState.IsValid)
            {
                var brand = mapper.Map<Brand>(brandDTO);

                brand = await brandService.Update(brand);

                return RedirectToAction("Index");
            }

            return View(brandDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var brand = await brandService.GetById(id.Value);

            if (brand == null)
                return NotFound();

            var brandDTO = mapper.Map<BrandDTO>(brand);

            return View(brandDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await brandService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}