﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class MerchandiseTypeService : GenericService<MerchandiseType>, IMerchandiseTypeService
    {
        private IMerchandiseTypeRepository merchandisetypeRepository;

        public MerchandiseTypeService(IMerchandiseTypeRepository merchandisetypeRepository) : base(merchandisetypeRepository)
        {
            this.merchandisetypeRepository = merchandisetypeRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await merchandisetypeRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByMerchandiseType(int id)
        {
            return await merchandisetypeRepository.GetStatusByMerchandiseType(id);
        }

    }
}
