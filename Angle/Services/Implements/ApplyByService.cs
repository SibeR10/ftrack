﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ApplyByService : GenericService<ApplyBy>, IApplyByService
    {
        private IApplyByRepository applybyRepository;

        public ApplyByService(IApplyByRepository applybyRepository) : base(applybyRepository)
        {
            this.applybyRepository = applybyRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await applybyRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByApplyBy(int id)
        {
            return await applybyRepository.GetStatusByApplyBy(id);
        }

    }
}
