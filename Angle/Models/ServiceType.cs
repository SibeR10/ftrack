﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ServiceType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServiceTypeID { get; set; }
        public string ServiceTypeName { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
