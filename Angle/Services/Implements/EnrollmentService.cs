﻿using Angle.Models;
using Angle.Repositories;
using System.Threading.Tasks;

namespace Angle.Services.Implements
{
    public class EnrollmentService : GenericService<Enrollment>, IEnrollmentService
    {
        private IEnrollmentRepository enrollmentRepository;

        public EnrollmentService(IEnrollmentRepository enrollmentRepository) : base(enrollmentRepository)
        {
            this.enrollmentRepository = enrollmentRepository;
        }        
    }
}
