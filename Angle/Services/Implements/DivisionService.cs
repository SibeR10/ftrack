﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class DivisionService : GenericService<Division>, IDivisionService
    {
        private IDivisionRepository divisionRepository;

        public DivisionService(IDivisionRepository divisionRepository) : base(divisionRepository)
        {
            this.divisionRepository = divisionRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await divisionRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByDivision(int id)
        {
            return await divisionRepository.GetStatusByDivision(id);
        }

    }
}
