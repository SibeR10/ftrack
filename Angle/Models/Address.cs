﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Address
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AddressID { get; set; }
        public string AddressName { get; set; }
        public int PostalCode { get; set; }
        public AddressType AddressType { get; set; }
        public int AddressTypeID { get; set; }
        public GeoLevel1 GeoLevel1 { get; set; }
        public int GeoLevel1ID { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
        public Customer Customer { get; set; }
        public int CustomerID { get; set; }
    }
}
