﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class BrandDTO
    {
        public int BrandID { get; set; }

        [Required(ErrorMessage = "The Marca is required")]
        [Display(Name = "Marca")]
        public string BrandDescription { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

