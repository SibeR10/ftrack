﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IMeasureUnitService : IGenericService<MeasureUnit>
    {
        Task<IEnumerable<Status>> GetStatus();

        Task<IEnumerable<Status>> GetStatusByMeasureUnit(int id);
    }
}
