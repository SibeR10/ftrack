﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class ContractSpecialRepository : GenericRepository<ContractSpecial>, IContractSpecialRepository
    {
        private readonly FTContext FTContext;

        public ContractSpecialRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence()
        {

            var listContractSpecialSequence = await FTContext.ContractSpecialSequence.ToListAsync();

            return listContractSpecialSequence;
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }

        public async Task<IEnumerable<Customer>> GetCustomer()
        {

            var listCustomer = await FTContext.Customer.ToListAsync();

            return listCustomer;
        }

        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {

            var listServiceType = await FTContext.ServiceType.ToListAsync();

            return listServiceType;
        }
        public async Task<IEnumerable<ContractSpecial>> GetContractSpecial()
        {

            var listContractSpecial = await FTContext.ContractSpecial.ToListAsync();

            return listContractSpecial;
        }


        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {

            var listGeoLevel1 = await FTContext.GeoLevel1.ToListAsync();

            return listGeoLevel1;
        }
        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {

            var listWarehouse = await FTContext.Warehouse.ToListAsync();

            return listWarehouse;
        }
        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {

            var listMerchandiseType = await FTContext.MerchandiseType.ToListAsync();

            return listMerchandiseType;
        }
        public async Task<IEnumerable<DeliveryMode>> GetDeliveryMode()
        {

            var listDeliveryMode = await FTContext.DeliveryMode.ToListAsync();

            return listDeliveryMode;
        }
    }
}
