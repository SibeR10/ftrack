﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class MeasureUnit
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MeasureUnitID { get; set; }
        public string MeasureUnitName { get; set; }
        public string MeasureUnitDescription { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
