﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using HistoricFuel = Angle.Models.HistoricFuel;
using Angle.Services.Implements;
using System;

namespace Angle.Controllers
{
    public class HistoricFuelController : Controller
    {
        private readonly IHistoricFuelService historicfuelService;
        private readonly IMapper mapper;

        public HistoricFuelController(IHistoricFuelService historicfuelService,
            IMapper mapper)
        {
            this.historicfuelService = historicfuelService;
            this.mapper = mapper;
        }


        public async Task<IActionResult> CreateAsync(int? id)
        {

            ViewBag.Driver = await historicfuelService.GetDriver();
            ViewBag.Fuel = await historicfuelService.GetFuel();
            ViewBag.Vehicles = await historicfuelService.GetVehiclesById(id.Value);

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(HistoricFuelDTO historicfuelDTO)
        {


            if (ModelState.IsValid)
            {

                var historicfuel = mapper.Map<HistoricFuel>(historicfuelDTO);

                historicfuel = await historicfuelService.Insert(historicfuel);

                return RedirectToAction("Index", "Vehicles");
            }

            return View(historicfuelDTO);
        }

        private IActionResult RedirectToAction(int vehiclesID)
        {
            throw new NotImplementedException();
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var historicfuel = await historicfuelService.GetById(id.Value);

            if (historicfuel == null)
                return NotFound();

            var historicfuelDTO = mapper.Map<HistoricFuelDTO>(historicfuel);
            return View(historicfuelDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(HistoricFuelDTO historicfuelDTO)
        {
            if (ModelState.IsValid)
            {
                var historicfuel = mapper.Map<HistoricFuel>(historicfuelDTO);

                historicfuel = await historicfuelService.Update(historicfuel);

                return RedirectToAction("Index");
            }

            return View(historicfuelDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var historicfuel = await historicfuelService.GetById(id.Value);

            if (historicfuel == null)
                return NotFound();

            var historicfuelDTO = mapper.Map<HistoricFuelDTO>(historicfuel);

            return View(historicfuelDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await historicfuelService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}