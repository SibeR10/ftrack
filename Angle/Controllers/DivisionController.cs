﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Division = Angle.Models.Division;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class DivisionController : Controller
    {
        private readonly IDivisionService divisionService;
        private readonly IMapper mapper;

        public DivisionController(IDivisionService divisionService,
            IMapper mapper)
        {
            this.divisionService = divisionService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await divisionService.GetAll();
            ViewBag.Status = await divisionService.GetStatus();
            var listDivision = data.Select(x => mapper.Map<DivisionDTO>(x)).ToList();
            return View(listDivision);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await divisionService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DivisionDTO divisionDTO)
        {
            
            if (ModelState.IsValid)
            {

                var division = mapper.Map<Division>(divisionDTO);

                division = await divisionService.Insert(division);

                return RedirectToAction("Index");
            }

            return View(divisionDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var division = await divisionService.GetById(id.Value);

            if (division == null)
                return NotFound();

            ViewBag.Status = await divisionService.GetStatus();
            var divisionDTO = mapper.Map<DivisionDTO>(division);
            return View(divisionDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(DivisionDTO divisionDTO)
        {
            if (ModelState.IsValid)
            {
                var division = mapper.Map<Division>(divisionDTO);

                division = await divisionService.Update(division);

                return RedirectToAction("Index");
            }

            return View(divisionDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var division = await divisionService.GetById(id.Value);

            if (division == null)
                return NotFound();

            var divisionDTO = mapper.Map<DivisionDTO>(division);

            return View(divisionDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await divisionService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}