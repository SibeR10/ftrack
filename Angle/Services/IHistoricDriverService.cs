﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IHistoricDriverService : IGenericService<HistoricDriver>
    {
        Task<IEnumerable<Driver>> GetDriver();
        Task<IEnumerable<Status>> GetStatus();
        Task<IEnumerable<Vehicles>> GetVehiclesById(int id);

    }
}
