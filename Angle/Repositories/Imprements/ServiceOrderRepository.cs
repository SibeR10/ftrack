﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class ServiceOrderRepository : GenericRepository<ServiceOrder>, IServiceOrderRepository
    {
        private readonly FTContext FTContext;

        public ServiceOrderRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<DeliveryMode>> GetDeliveryMode()
        {

            var listDeliveryMode = await FTContext.DeliveryMode.ToListAsync();

            return listDeliveryMode;
        }
        public async Task<IEnumerable<ServiceOrderStatus>> GetStatus()
        {

            var listStatus = await FTContext.ServiceOrderStatus.ToListAsync();

            return listStatus;
        }

        public async Task<IEnumerable<Quote>> GetQuote()
        {

            var listQuote = await FTContext.Quote.ToListAsync();

            return listQuote;
        }

        public async Task<IEnumerable<Customer>> GetCustomer()
        {

            var listCustomer = await FTContext.Customer.ToListAsync();

            return listCustomer;
        }

        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {

            var listServiceType = await FTContext.ServiceType.ToListAsync();

            return listServiceType;
        }

        public async Task<IEnumerable<SalesPerson>> GetSalesPerson()
        {

            var listSalesPerson = await FTContext.SalesPerson.ToListAsync();

            return listSalesPerson;
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {

            var listGeoLevel1 = await FTContext.GeoLevel1.ToListAsync();

            return listGeoLevel1;
        }
        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {

            var listWarehouse = await FTContext.Warehouse.ToListAsync();

            return listWarehouse;
        }
        public async Task<IEnumerable<ItemsQuote>> GetItemsQuote()
        {

            var listItemsQuote = await FTContext.ItemsQuote.ToListAsync();

            return listItemsQuote;
        }
        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {

            var listMerchandiseType = await FTContext.MerchandiseType.ToListAsync();

            return listMerchandiseType;
        }
        public async Task<IEnumerable<ItemsServiceOrder>> GetItemsServiceOrder()
        {

            var listItemsServiceOrder = await FTContext.ItemsServiceOrder.ToListAsync();

            return listItemsServiceOrder;
        }
        public async Task<IEnumerable<ContractSpecial>> GetContractSpecial()
        {

            var listContractSpecial = await FTContext.ContractSpecial.ToListAsync();

            return listContractSpecial;
        }
        public async Task<IEnumerable<ContractSequence>> GetContractSequence()
        {

            var listContractSequence = await FTContext.ContractSequence.ToListAsync();

            return listContractSequence;
        }
        public async Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence()
        {

            var listContractSpecialSequence = await FTContext.ContractSpecialSequence.ToListAsync();

            return listContractSpecialSequence;
        }
        public async Task<IEnumerable<Person>> GetRemittee()
        {

            var listRemittee = await FTContext.Person.ToListAsync();

            return listRemittee;
        }
        public async Task<IEnumerable<Person>> GetSender()
        {

            var listSender = await FTContext.Person.ToListAsync();

            return listSender;
        }

        public async Task<IEnumerable<Person>> GetSenderInfo(int id)
        {

            var listSenderInfo = await FTContext.Person
                .Where(x => x.PersonID == id)
                .ToListAsync();

            return listSenderInfo;
        }
    }
}
