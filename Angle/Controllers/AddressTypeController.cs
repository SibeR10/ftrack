﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using AddressType = Angle.Models.AddressType;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class AddressTypeController : Controller
    {
        private readonly IAddressTypeService addresstypeService;
        private readonly IMapper mapper;

        public AddressTypeController(IAddressTypeService addresstypeService,
            IMapper mapper)
        {
            this.addresstypeService = addresstypeService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await addresstypeService.GetAll();
            ViewBag.Status = await addresstypeService.GetStatus();
            var listAddressType = data.Select(x => mapper.Map<AddressTypeDTO>(x)).ToList();
            return View(listAddressType);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await addresstypeService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(AddressTypeDTO addresstypeDTO)
        {
            
            if (ModelState.IsValid)
            {

                var addresstype = mapper.Map<AddressType>(addresstypeDTO);

                addresstype = await addresstypeService.Insert(addresstype);

                return RedirectToAction("Index");
            }

            return View(addresstypeDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var addresstype = await addresstypeService.GetById(id.Value);

            if (addresstype == null)
                return NotFound();

            ViewBag.Status = await addresstypeService.GetStatus();
            var addresstypeDTO = mapper.Map<AddressTypeDTO>(addresstype);
            return View(addresstypeDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(AddressTypeDTO addresstypeDTO)
        {
            if (ModelState.IsValid)
            {
                var addresstype = mapper.Map<AddressType>(addresstypeDTO);

                addresstype = await addresstypeService.Update(addresstype);

                return RedirectToAction("Index");
            }

            return View(addresstypeDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var addresstype = await addresstypeService.GetById(id.Value);

            if (addresstype == null)
                return NotFound();

            var addresstypeDTO = mapper.Map<AddressTypeDTO>(addresstype);

            return View(addresstypeDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await addresstypeService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}