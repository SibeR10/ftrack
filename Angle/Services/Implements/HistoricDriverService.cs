﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class HistoricDriverService : GenericService<HistoricDriver>, IHistoricDriverService
    {
        private IHistoricDriverRepository historicdriverRepository;

        public HistoricDriverService(IHistoricDriverRepository historicdriverRepository) : base(historicdriverRepository)
        {
            this.historicdriverRepository = historicdriverRepository;
        }

        public async Task<IEnumerable<Driver>> GetDriver()
        {
            return await historicdriverRepository.GetDriver();
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await historicdriverRepository.GetStatus();
        }
        public async Task<IEnumerable<Vehicles>> GetVehiclesById(int id)
        {
            return await historicdriverRepository.GetVehiclesById(id);
        }

    }
}
