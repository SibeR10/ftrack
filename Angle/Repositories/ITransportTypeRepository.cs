﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface ITransportTypeRepository : IGenericRepository<TransportType>
    {
        Task<IEnumerable<Status>> GetStatus();

        Task<IEnumerable<Status>> GetStatusByTransportType(int id);
    }
}
