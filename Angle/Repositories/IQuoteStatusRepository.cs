﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IQuoteStatusRepository : IGenericRepository<QuoteStatus>
    {
    }
}
