﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ContractMeasuryService : GenericService<ContractMeasury>, IContractMeasuryService
    {
        private IContractMeasuryRepository contractmeasuryRepository;

        public ContractMeasuryService(IContractMeasuryRepository contractmeasuryRepository) : base(contractmeasuryRepository)
        {
            this.contractmeasuryRepository = contractmeasuryRepository;
        }
        public async Task<IEnumerable<ContractMeasury>> GetContractMeasury()
        {
            return await contractmeasuryRepository.GetContractMeasury();
        }

    }
}
