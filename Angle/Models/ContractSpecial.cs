﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ContractSpecial
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContractSpecialID { get; set; }
        public string ContractSpecialDescription { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
        public Customer Customer { get; set; }
        public int CustomerID { get; set; }
        public ServiceType ServiceType { get; set; }
        public int ServiceTypeID { get; set; }
    }
}
