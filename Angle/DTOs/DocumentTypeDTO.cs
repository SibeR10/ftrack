﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class DocumentTypeDTO
    {
        public int DocumentTypeID { get; set; }

        [Required(ErrorMessage = "The DocumentTypeName is required")]
        [Display(Name = "Documento")]
        public string DocumentTypeName { get; set; }

        [Required(ErrorMessage = "The DocumentTypeName is required")]
        [Display(Name = "Descripción del tipo de documento")]
        public string DocumentTypeDescription { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

