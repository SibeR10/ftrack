﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IContractRepository : IGenericRepository<Contract>
    {
        Task<IEnumerable<Status>> GetStatus();
        Task<IEnumerable<ServiceType>> GetServiceType();
        Task<IEnumerable<ContractSequence>> GetContractSequence();
        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();

        Task<IEnumerable<Warehouse>> GetWarehouse();
        Task<IEnumerable<MerchandiseType>> GetMerchandiseType();
        Task<IEnumerable<DeliveryMode>> GetDeliveryMode();

    }
}
