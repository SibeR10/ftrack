﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class CustomerDTO
    {
        public int CustomerID { get; set; }

		[Display(Name = "Documento")]
		public int Document { get; set; }

		[Display(Name = "Razón Social")]
		public string CompanyName { get; set; }

		[Display(Name = "Nombre")]
		public string FirstName { get; set; }

		[Display(Name = "Apellido")]
		public string LastName { get; set; }

		[Display(Name = "Teléfono principal")]
		public int MainPhone { get; set; }

		[Display(Name = "Teléfono secundario")]
		public int SecondPhone { get; set; }

		[Display(Name = "Correo electrónico")]
		public string Email { get; set; }

		[Display(Name = "Fecha de creación de la transacción")]
		public DateTime CreateDateTransaction { get; set; }

		[Display(Name = "Usuario Creador de la transacción")]
		public string CreateUserTransaction { get; set; }

		[Display(Name = "Fecha de modificación de la transacción")]
		public DateTime UpdateDateTransaction { get; set; }

		[Display(Name = "Usuario modificador de la transacción")]
		public string UpdateUserTransaction { get; set; }

		[Display(Name = "División")]
		public int DivisionID { get; set; }

		[Display(Name = "Tipo de cliente")]
		public int CustomerTypeID { get; set; }

		[Display(Name = "Estatus")]
		public int StatusID { get; set; }

		[Display(Name = "Tipo de documento")]
		public int DocumentTypeID { get; set; }

		[Display(Name = "Cuenta")]
		public int AccountID { get; set; }

		[Display(Name = "Tipo de cuenta")]
		public int AccountTypeID { get; set; }

		[Display(Name = "Vendedor")]
		public int SalesPersonID { get; set; }
	}
}
