﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class VehiclesRepository : GenericRepository<Vehicles>, IVehiclesRepository
    {
        private readonly FTContext FTContext;

        public VehiclesRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }
        public async Task<IEnumerable<DocumentUpload>> GetDocumentUpload()
        {

            var listDocumentUpload = await FTContext.DocumentUpload.ToListAsync();

            return listDocumentUpload;
        }

        public async Task<IEnumerable<Status>> GetStatusByVehicles(int id)
        {

            var listStatusByVehicles = await FTContext.Vehicles
                .Include(x => x.Status)
                .Where(x => x.StatusID == id)
                .Select(x => x.Status)
                .ToListAsync();

            return listStatusByVehicles;
        }
        public async Task<IEnumerable<Division>> GetDivision()
        {

            var listDivision = await FTContext.Division.ToListAsync();

            return listDivision;
        }

        public async Task<IEnumerable<Division>> GetDivisionByVehicles(int id)
        {

            var listDivisionByVehicles = await FTContext.Vehicles
                .Include(x => x.Division)
                .Where(x => x.DivisionID == id)
                .Select(x => x.Division)
                .ToListAsync();

            return listDivisionByVehicles;
        }
        public async Task<IEnumerable<Brand>> GetBrand()
        {

            var listBrand = await FTContext.Brand.ToListAsync();

            return listBrand;
        }

        public async Task<IEnumerable<Brand>> GetBrandByVehicles(int id)
        {

            var listBrandByVehicles = await FTContext.Vehicles
                .Include(x => x.Brand)
                .Where(x => x.BrandID == id)
                .Select(x => x.Brand)
                .ToListAsync();

            return listBrandByVehicles;
        }

        public async Task<IEnumerable<Driver>> GetDriver()
        {

            var listDriver = await FTContext.Driver.ToListAsync();

            return listDriver;
        }

        public async Task<IEnumerable<Driver>> GetDriverByVehicles(int id)
        {

            var listDriverByVehicles = await FTContext.Vehicles
                .Include(x => x.Driver)
                .Where(x => x.DriverID == id)
                .Select(x => x.Driver)
                .ToListAsync();

            return listDriverByVehicles;
        }

        public async Task<IEnumerable<Fuel>> GetFuel()
        {

            var listFuel = await FTContext.Fuel.ToListAsync();

            return listFuel;
        }

        

        public async Task<IEnumerable<HistoricDriver>> GetHistoricDriver()
        {

            var listHistoricDriver = await FTContext.HistoricDriver.ToListAsync();

            return listHistoricDriver;
        }

        public async Task<IEnumerable<HistoricDriver>> GetHistoricDriverByVehicle(int id)
        {

            var listHistoricDriverByVehicles = await FTContext.HistoricDriver
                .Include(x => x.Vehicles)
                .Where(x => x.VehiclesID == id)
                .ToListAsync();

            return listHistoricDriverByVehicles;
        }

        public async Task<IEnumerable<HistoricFuel>> GetHistoricFuel()
        {

            var listHistoricFuel = await FTContext.HistoricFuel.ToListAsync();

            return listHistoricFuel;
        }

        public async Task<IEnumerable<HistoricFuel>> GetHistoricFuelByVehicle(int id)
        {

            var listHistoricFuelByVehicles = await FTContext.HistoricFuel
                .Include(x => x.Vehicles)
                .Where(x => x.VehiclesID == id)
                .ToListAsync();

            return listHistoricFuelByVehicles;
        }
        
    }
}
