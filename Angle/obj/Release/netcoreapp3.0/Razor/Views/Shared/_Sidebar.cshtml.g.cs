#pragma checksum "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "24e2cd235fe3ded371df322233c1a4436c056180"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(Angle.Pages.Shared.Views_Shared__Sidebar), @"mvc.1.0.view", @"/Views/Shared/_Sidebar.cshtml")]
namespace Angle.Pages.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\_ViewImports.cshtml"
using Angle;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"24e2cd235fe3ded371df322233c1a4436c056180", @"/Views/Shared/_Sidebar.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2cac7a1b7297ab84fc06688fdfc495972e2e12c0", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__Sidebar : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<!-- START Sidebar (left)-->
<div class=""aside-inner"">
    <nav class=""sidebar"" data-sidebar-anyclick-close="""">
        <!-- START sidebar nav-->
        <ul class=""sidebar-nav"">
            <!-- Iterates over all sidebar items-->
            <li class=""nav-heading"">
                <span>Menu de opciones</span>
            </li>
            <li");
            BeginWriteAttribute("class", " class=\"", 356, "\"", 403, 1);
#nullable restore
#line 10 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 364, Html.isActive(controller: "Dashboard"), 364, 39, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@">
                <a href=""#dashboard"" title=""Dashboard"" data-toggle=""collapse"">
                    <div class=""badge badge-info float-right"">3</div>
                    <em class=""icon-speedometer""></em>
                    <span data-localize=""sidebar.nav.DASHBOARD"">Inicio</span>
                </a>
                <ul id=""dashboard"" class=""sidebar-nav sidebar-subnav collapse"">
                    <li class=""sidebar-subnav-header"">Inicio</li>
                    <li");
            BeginWriteAttribute("class", " class=\"", 886, "\"", 937, 1);
#nullable restore
#line 18 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 894, Html.isActive("Dashboard", "Dashboard_v1"), 894, 43, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                        <a");
            BeginWriteAttribute("href", " href=\"", 967, "\"", 1014, 1);
#nullable restore
#line 19 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 974, Url.Action("Dashboard_v1", "Dashboard"), 974, 40, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Dashboard v1\">\r\n                            <span>Dashboard v1</span>\r\n                        </a>\r\n                    </li>\r\n                    <li");
            BeginWriteAttribute("class", " class=\"", 1174, "\"", 1225, 1);
#nullable restore
#line 23 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 1182, Html.isActive("Dashboard", "Dashboard_v2"), 1182, 43, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                        <a");
            BeginWriteAttribute("href", " href=\"", 1255, "\"", 1302, 1);
#nullable restore
#line 24 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 1262, Url.Action("Dashboard_v2", "Dashboard"), 1262, 40, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Dashboard v2\">\r\n                            <span>Dashboard v2</span>\r\n                        </a>\r\n                    </li>\r\n                    <li");
            BeginWriteAttribute("class", " class=\"", 1462, "\"", 1513, 1);
#nullable restore
#line 28 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 1470, Html.isActive("Dashboard", "Dashboard_v3"), 1470, 43, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                        <a");
            BeginWriteAttribute("href", " href=\"", 1543, "\"", 1590, 1);
#nullable restore
#line 29 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 1550, Url.Action("Dashboard_v3", "Dashboard"), 1550, 40, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Dashboard v3\">\r\n                            <span>Dashboard v3</span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li");
            BeginWriteAttribute("class", " class=\"", 1784, "\"", 1829, 1);
#nullable restore
#line 35 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 1792, Html.isActive(controller: "Pilares"), 1792, 37, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@">
                <a href=""#pilares"" title=""Pilares"" data-toggle=""collapse"">
                    <em class=""far fa-folder-open""></em>
                    <span>Pilares</span>
                </a>
                <ul id=""pilares"" class=""sidebar-nav sidebar-subnav collapse"">
                    <li class=""sidebar-subnav-header"">Clientes</li>
                    <li");
            BeginWriteAttribute("class", " class=\"", 2202, "\"", 2250, 1);
#nullable restore
#line 42 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 2210, Html.isActive("Clientes", "Clientes_h"), 2210, 40, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                        <a href=\"#clientes\" title=\"Clientes\" data-toggle=\"collapse\">\r\n                            <span>Clientes</span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li");
            BeginWriteAttribute("class", " class=\"", 2505, "\"", 2552, 1);
#nullable restore
#line 49 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 2513, Html.isActive(controller: "Operacion"), 2513, 39, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                <a");
            BeginWriteAttribute("href", " href=\"", 2574, "\"", 2612, 1);
#nullable restore
#line 50 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 2581, Url.Action("Index", "Widgets"), 2581, 31, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" title=""Operaci�n"">
                    <div class=""badge badge-success float-right"">30</div>
                    <em class=""icon-grid""></em>
                    <span data-localize=""sidebar.nav.OPERATION"">Operaci&oacute;n</span>
                </a>
            </li>
            <li");
            BeginWriteAttribute("class", " class=\"", 2903, "\"", 2952, 1);
#nullable restore
#line 56 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 2911, Html.isActive(controller: "Visibilidad"), 2911, 41, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                <a");
            BeginWriteAttribute("href", " href=\"", 2974, "\"", 3012, 1);
#nullable restore
#line 57 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 2981, Url.Action("Index", "Widgets"), 2981, 31, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" title=""Visibilidad"">
                    <div class=""badge badge-success float-right"">30</div>
                    <em class=""icon-grid""></em>
                    <span data-localize=""sidebar.nav.VISIBILITY"">Visibilidad</span>
                </a>
            </li>
            <li");
            BeginWriteAttribute("class", " class=\"", 3301, "\"", 3352, 1);
#nullable restore
#line 63 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 3309, Html.isActive(controller: "Configuracion"), 3309, 43, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@">
                <a href=""#configuracion"" title=""Configuraci�n"" data-toggle=""collapse"">
                    <em class=""far fa-folder-open""></em>
                    <div class=""badge badge-success float-right"">30</div>
                    <span>Configuraci&oacute;n</span>
                </a>
                <ul id=""configuracion"" class=""sidebar-nav sidebar-subnav collapse"">
                    <li class=""sidebar-subnav-header"">Configuraci&oacute;n</li>
                    <li");
            BeginWriteAttribute("class", " class=\"", 3843, "\"", 3896, 1);
#nullable restore
#line 71 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 3851, Html.isActive("Multilevels", "Multilevel_1"), 3851, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@">
                        <a href=""#vehiculo_conf"" title=""Level 1"" data-toggle=""collapse"">
                            <div class=""badge badge-success float-right"">30</div>
                            <em class=""icon-grid""></em>
                            <span>Veh&iacute;culo</span>
                        </a>
                        <ul id=""vehiculo_conf"" class=""sidebar-nav sidebar-subnav collapse"">
                            <li class=""sidebar-subnav-header"">Veh�culo</li>
                            <li");
            BeginWriteAttribute("class", " class=\"", 4419, "\"", 4472, 1);
#nullable restore
#line 79 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 4427, Html.isActive("Multilevels", "Multilevel_1"), 4427, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 4510, "\"", 4559, 1);
#nullable restore
#line 80 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 4517, Url.Action("Multilevel_1", "Multilevels"), 4517, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Combustible\">\r\n                                    <span>Combustible</span>\r\n                                </a>\r\n                            </li>\r\n                            <li");
            BeginWriteAttribute("class", " class=\"", 4749, "\"", 4802, 1);
#nullable restore
#line 84 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 4757, Html.isActive("Multilevels", "Multilevel_1"), 4757, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 4840, "\"", 4889, 1);
#nullable restore
#line 85 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 4847, Url.Action("Multilevel_1", "Multilevels"), 4847, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Marca\">\r\n                                    <span>Marca</span>\r\n                                </a>\r\n                            </li>\r\n                        </ul>\r\n                    </li>\r\n                    <li");
            BeginWriteAttribute("class", " class=\"", 5117, "\"", 5170, 1);
#nullable restore
#line 91 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 5125, Html.isActive("Multilevels", "Multilevel_1"), 5125, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@">
                        <a href=""#clientes_conf"" title=""Level 1"" data-toggle=""collapse"">
                            <div class=""badge badge-success float-right"">30</div>
                            <em class=""icon-grid""></em>
                            <span>Clientes</span>
                        </a>
                        <ul id=""clientes_conf"" class=""sidebar-nav sidebar-subnav collapse"">
                            <li class=""sidebar-subnav-header"">Clientes</li>
                            <li");
            BeginWriteAttribute("class", " class=\"", 5686, "\"", 5739, 1);
#nullable restore
#line 99 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 5694, Html.isActive("Multilevels", "Multilevel_1"), 5694, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 5777, "\"", 5826, 1);
#nullable restore
#line 100 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 5784, Url.Action("Multilevel_1", "Multilevels"), 5784, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Taxonom�a de pa�s\">\r\n                                    <span>Taxonom&iacute;a de pa&iacute;s</span>\r\n                                </a>\r\n                            </li>\r\n                            <li");
            BeginWriteAttribute("class", " class=\"", 6042, "\"", 6095, 1);
#nullable restore
#line 104 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 6050, Html.isActive("Multilevels", "Multilevel_1"), 6050, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 6133, "\"", 6182, 1);
#nullable restore
#line 105 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 6140, Url.Action("Multilevel_1", "Multilevels"), 6140, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Tipos de clientes\">\r\n                                    <span>Tipos de clientes</span>\r\n                                </a>\r\n                            </li>\r\n                            <li");
            BeginWriteAttribute("class", " class=\"", 6384, "\"", 6437, 1);
#nullable restore
#line 109 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 6392, Html.isActive("Multilevels", "Multilevel_1"), 6392, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 6475, "\"", 6524, 1);
#nullable restore
#line 110 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 6482, Url.Action("Multilevel_1", "Multilevels"), 6482, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Tipos de documentos\">\r\n                                    <span>Tipos de documentos</span>\r\n                                </a>\r\n                            </li>\r\n                            <li");
            BeginWriteAttribute("class", " class=\"", 6730, "\"", 6783, 1);
#nullable restore
#line 114 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 6738, Html.isActive("Multilevels", "Multilevel_1"), 6738, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 6821, "\"", 6870, 1);
#nullable restore
#line 115 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 6828, Url.Action("Multilevel_1", "Multilevels"), 6828, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"T�rminos de pago\">\r\n                                    <span>T&eacute;rminos de pago</span>\r\n                                </a>\r\n                            </li>\r\n                            <li");
            BeginWriteAttribute("class", " class=\"", 7077, "\"", 7130, 1);
#nullable restore
#line 119 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 7085, Html.isActive("Multilevels", "Multilevel_1"), 7085, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 7168, "\"", 7217, 1);
#nullable restore
#line 120 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 7175, Url.Action("Multilevel_1", "Multilevels"), 7175, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Formas de pago\">\r\n                                    <span>Formas de pago</span>\r\n                                </a>\r\n                            </li>\r\n                        </ul>\r\n                    </li>\r\n                    <li");
            BeginWriteAttribute("class", " class=\"", 7463, "\"", 7516, 1);
#nullable restore
#line 126 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 7471, Html.isActive("Multilevels", "Multilevel_1"), 7471, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@">
                        <a href=""#general_conf"" title=""Level 1"" data-toggle=""collapse"">
                            <div class=""badge badge-success float-right"">30</div>
                            <em class=""icon-grid""></em>
                            <span>General</span>
                        </a>
                        <ul id=""general_conf"" class=""sidebar-nav sidebar-subnav collapse"">
                            <li class=""sidebar-subnav-header"">General</li>
                            <li");
            BeginWriteAttribute("class", " class=\"", 8028, "\"", 8081, 1);
#nullable restore
#line 134 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 8036, Html.isActive("Multilevels", "Multilevel_1"), 8036, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 8119, "\"", 8168, 1);
#nullable restore
#line 135 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 8126, Url.Action("Multilevel_1", "Multilevels"), 8126, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Division\">\r\n                                    <span>Division</span>\r\n                                </a>\r\n                            </li>\r\n                            <li");
            BeginWriteAttribute("class", " class=\"", 8352, "\"", 8405, 1);
#nullable restore
#line 139 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 8360, Html.isActive("Multilevels", "Multilevel_1"), 8360, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 8443, "\"", 8492, 1);
#nullable restore
#line 140 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 8450, Url.Action("Multilevel_1", "Multilevels"), 8450, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Tipo de cuenta\">\r\n                                    <span>Tipo de cuenta</span>\r\n                                </a>\r\n                            </li>\r\n                            <li");
            BeginWriteAttribute("class", " class=\"", 8688, "\"", 8741, 1);
#nullable restore
#line 144 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 8696, Html.isActive("Multilevels", "Multilevel_1"), 8696, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 8779, "\"", 8828, 1);
#nullable restore
#line 145 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 8786, Url.Action("Multilevel_1", "Multilevels"), 8786, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Tipo de cliente\">\r\n                                    <span>Tipo de cliente</span>\r\n                                </a>\r\n                            </li>\r\n                            <li");
            BeginWriteAttribute("class", " class=\"", 9026, "\"", 9072, 1);
#nullable restore
#line 149 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 9034, Html.isActive("Monedas", "Monedas_h"), 9034, 38, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 9110, "\"", 9152, 1);
#nullable restore
#line 150 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 9117, Url.Action("Monedas_h", "Monedas"), 9117, 35, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" title=\"Moneda\">\r\n                                    <span>Moneda</span>\r\n                                </a>\r\n                            </li>\r\n                            <li");
            BeginWriteAttribute("class", " class=\"", 9332, "\"", 9378, 1);
#nullable restore
#line 154 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 9340, Html.isActive("Estatus", "Estatus_h"), 9340, 38, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 9416, "\"", 9458, 1);
#nullable restore
#line 155 "C:\Users\siber\Desktop\netcore-static\Angle\Angle\Views\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 9423, Url.Action("Estatus_h", "Estatus"), 9423, 35, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" title=""Estatus"">
                                    <span>Estatus</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- END sidebar nav-->
    </nav>
</div>
<!-- END Sidebar (left)-->
<style>
    .wrapper .aside-container .aside-inner {
        padding-top: 70px;
    }

    .sidebar, .sidebar-subnav > li > .nav-item, .sidebar-subnav > li > a, .sidebar-nav > li.active > a {
        background-color: #eae7e7 !important;
    }

</style>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
