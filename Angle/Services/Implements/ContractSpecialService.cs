﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ContractSpecialService : GenericService<ContractSpecial>, IContractSpecialService
    {
        private IContractSpecialRepository contractspecialRepository;

        public ContractSpecialService(IContractSpecialRepository contractspecialRepository) : base(contractspecialRepository)
        {
            this.contractspecialRepository = contractspecialRepository;
        }

        public async Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence()
        {
            return await contractspecialRepository.GetContractSpecialSequence();
        }
        
        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await contractspecialRepository.GetStatus();
        }

        public async Task<IEnumerable<Customer>> GetCustomer()
        {
            return await contractspecialRepository.GetCustomer();
        }
        

        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {
            return await contractspecialRepository.GetServiceType();
        }

        public async Task<IEnumerable<ContractSpecial>> GetContractSpecial()
        {
            return await contractspecialRepository.GetContractSpecial();
        }


        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {
            return await contractspecialRepository.GetWarehouse();
        }


        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {
            return await contractspecialRepository.GetMerchandiseType();
        }


        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await contractspecialRepository.GetGeoLevel1();
        }


        public async Task<IEnumerable<DeliveryMode>> GetDeliveryMode()
        {
            return await contractspecialRepository.GetDeliveryMode();
        }
    }
}
