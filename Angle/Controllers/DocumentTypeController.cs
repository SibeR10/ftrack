﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using DocumentType = Angle.Models.DocumentType;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class DocumentTypeController : Controller
    {
        private readonly IDocumentTypeService documenttypeService;
        private readonly IMapper mapper;

        public DocumentTypeController(IDocumentTypeService documenttypeService,
            IMapper mapper)
        {
            this.documenttypeService = documenttypeService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await documenttypeService.GetAll();
            ViewBag.Status = await documenttypeService.GetStatus();
            var listDocumentType = data.Select(x => mapper.Map<DocumentTypeDTO>(x)).ToList();
            return View(listDocumentType);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await documenttypeService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DocumentTypeDTO documenttypeDTO)
        {
            
            if (ModelState.IsValid)
            {

                var documenttype = mapper.Map<DocumentType>(documenttypeDTO);

                documenttype = await documenttypeService.Insert(documenttype);

                return RedirectToAction("Index");
            }

            return View(documenttypeDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var documenttype = await documenttypeService.GetById(id.Value);

            if (documenttype == null)
                return NotFound();

            ViewBag.Status = await documenttypeService.GetStatus();
            var documenttypeDTO = mapper.Map<DocumentTypeDTO>(documenttype);
            return View(documenttypeDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(DocumentTypeDTO documenttypeDTO)
        {
            if (ModelState.IsValid)
            {
                var documenttype = mapper.Map<DocumentType>(documenttypeDTO);

                documenttype = await documenttypeService.Update(documenttype);

                return RedirectToAction("Index");
            }

            return View(documenttypeDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var documenttype = await documenttypeService.GetById(id.Value);

            if (documenttype == null)
                return NotFound();

            var documenttypeDTO = mapper.Map<DocumentTypeDTO>(documenttype);

            return View(documenttypeDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await documenttypeService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}