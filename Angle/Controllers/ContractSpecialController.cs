﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using ContractSpecial = Angle.Models.ContractSpecial;
using Angle.Services.Implements;


using System.Data;
using Microsoft.Data.SqlClient;
namespace Angle.Controllers
{
    public class ContractSpecialController : Controller
    {
        private readonly IContractSpecialService contractspecialService;
        private readonly IMapper mapper;

        public ContractSpecialController(IContractSpecialService contractspecialService,
            IMapper mapper)
        {
            this.contractspecialService = contractspecialService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await contractspecialService.GetAll();
            ViewBag.Status = await contractspecialService.GetStatus();
            ViewBag.Customer = await contractspecialService.GetCustomer();
            ViewBag.ServiceType = await contractspecialService.GetServiceType();

            var listContractSpecial = data.Select(x => mapper.Map<ContractSpecialDTO>(x)).ToList();
            return View(listContractSpecial);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.ServiceType = await contractspecialService.GetServiceType();
            ViewBag.Status = await contractspecialService.GetStatus();
            ViewBag.Customer = await contractspecialService.GetCustomer();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ContractSpecialDTO contractspecialDTO)
        {
            
            if (ModelState.IsValid)
            {

                var contractspecial = mapper.Map<ContractSpecial>(contractspecialDTO);

                contractspecial = await contractspecialService.Insert(contractspecial);

                return RedirectToAction("Index");
            }

            return View(contractspecialDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var contractspecial = await contractspecialService.GetById(id.Value);

            if (contractspecial == null)
                return NotFound();

            ViewBag.ContractSpecialSequence = await contractspecialService.GetContractSpecialSequence();
            ViewBag.Customer = await contractspecialService.GetCustomer();
            ViewBag.Status = await contractspecialService.GetStatus();
            ViewBag.ServiceType = await contractspecialService.GetServiceType();
            ViewBag.GeoLevel1 = await contractspecialService.GetGeoLevel1();
            ViewBag.Warehouse = await contractspecialService.GetWarehouse();
            ViewBag.MerchandiseType = await contractspecialService.GetMerchandiseType();
            ViewBag.DeliveryMode = await contractspecialService.GetDeliveryMode();
            var contractspecialDTO = mapper.Map<ContractSpecialDTO>(contractspecial);
            return View(contractspecialDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ContractSpecialDTO contractspecialDTO)
        {
            if (ModelState.IsValid)
            {
                var contractspecial = mapper.Map<ContractSpecial>(contractspecialDTO);

                contractspecial = await contractspecialService.Update(contractspecial);

                var url = "/Edit/" + contractspecial.ContractSpecialID;
                return RedirectToAction(url);
            }

            return View(contractspecialDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var contractspecial = await contractspecialService.GetById(id.Value);

            if (contractspecial == null)
                return NotFound();

            var contractspecialDTO = mapper.Map<ContractSpecialDTO>(contractspecial);

            return View(contractspecialDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await contractspecialService.Delete(id.Value);

            return RedirectToAction("Index");
        }


        public async Task<IActionResult> Import(IFormFile file, int ContractSpecialID, int CustomerID)
        {
            var list = new List<ContractSequence>();
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowcount = worksheet.Dimension.Rows;
                    for (int row = 2; row <= rowcount; row++)
                    {
                        list.Add(new ContractSequence
                        {

                            WeightMin = (int)(double)worksheet.Cells[row, 1].Value,
                            BasePrice = (int)(double)worksheet.Cells[row, 2].Value,
                            ExtraPrice = (int)(double)worksheet.Cells[row, 3].Value,
                            MerchandiseTypeID = (int)(double)worksheet.Cells[row, 4].Value,
                            DeliveryModeID = (int)(double)worksheet.Cells[row, 5].Value,
                            OriginID = (int)(double)worksheet.Cells[row, 6].Value,
                            DestinationID = (int)(double)worksheet.Cells[row, 7].Value,
                            ServiceTypeID = (int)(double)worksheet.Cells[row, 8].Value,
                        });
                    }
                }
            }

            list.ForEach(p =>
            {

                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();
                int WeightMin = p.WeightMin;
                int BasePrice = p.BasePrice;
                int ExtraPrice = p.ExtraPrice;
                int MerchandiseTypeID = p.MerchandiseTypeID;
                int DeliveryModeID = p.DeliveryModeID;
                int OriginID = p.OriginID;
                int DestinationID = p.DestinationID;
                int ServiceTypeID = p.ServiceTypeID;
                string cadena = "INSERT INTO [dbo].[FT_ContractSpecialSequence]([ContractSpecialID],[CustomerID],[WeightMin],[BasePrice],[ExtraPrice],[MerchandiseTypeID],[DeliveryModeID],[OriginID],[DestinationID],[ServiceTypeID])VALUES(" + ContractSpecialID + "," +CustomerID+"," + WeightMin + "," + BasePrice + "," + ExtraPrice + "," + MerchandiseTypeID + "," + DeliveryModeID + "," + OriginID + "," + DestinationID + "," + ServiceTypeID + ")";
                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();
                conexion.Close();

            });


            return RedirectToAction("Index");
        }


    }
}