﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;
using ChargesByQuote = Angle.Models.ChargesByQuote;

namespace Angle.Controllers
{
    public class ChargesByQuoteController : Controller
    {
        private readonly IChargesByQuoteService chargesbyquoteService;
        private readonly IMapper mapper;

        public ChargesByQuoteController(IChargesByQuoteService chargesbyquoteService,
            IMapper mapper)
        {
            this.chargesbyquoteService = chargesbyquoteService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await chargesbyquoteService.GetAll();

            ViewBag.MerchandiseType = await chargesbyquoteService.GetMerchandiseType();
            var listChargesByQuote = data.Select(x => mapper.Map<ChargesByQuoteDTO>(x)).ToList();

            return View(listChargesByQuote);
        }

        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.MerchandiseType = await chargesbyquoteService.GetMerchandiseType();
            ViewBag.ServiceType = await chargesbyquoteService.GetServiceType();
            ViewBag.ApplyBy = await chargesbyquoteService.GetApplyBy();
            ViewBag.MeasureUnit = await chargesbyquoteService.GetMeasureUnit();
            ViewBag.Quote = await chargesbyquoteService.GetQuote();
            ViewBag.Customer = await chargesbyquoteService.GetCustomer();
            ViewBag.ContractSpecial = await chargesbyquoteService.GetContractSpecial();
            ViewBag.ContractSpecialSequence = await chargesbyquoteService.GetContractSpecialSequence();
            ViewBag.ContractSpecialMeasury = await chargesbyquoteService.GetContractSpecialMeasury();
            ViewBag.ContractSequence = await chargesbyquoteService.GetContractSequence();
            ViewBag.ContractMeasury = await chargesbyquoteService.GetContractMeasury();
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Create(ChargesByQuoteDTO chargesbyquoteDTO)
        {
            if (ModelState.IsValid)
            {

                var chargesbyquote = mapper.Map<ChargesByQuote>(chargesbyquoteDTO);

                chargesbyquote = await chargesbyquoteService.Insert(chargesbyquote);


                return RedirectToAction("Index", "Quote");
            }

            return View(chargesbyquoteDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var chargesbyquote = await chargesbyquoteService.GetById(id.Value);

            if (chargesbyquote == null)
                return NotFound();

            var chargesbyquoteDTO = mapper.Map<ChargesByQuoteDTO>(chargesbyquote);

            return View(chargesbyquoteDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ChargesByQuoteDTO chargesbyquoteDTO)
        {
            if (ModelState.IsValid)
            {
                var chargesbyquote = mapper.Map<ChargesByQuote>(chargesbyquoteDTO);

                chargesbyquote = await chargesbyquoteService.Update(chargesbyquote);
                return RedirectToAction("Index");
            }

            return View(chargesbyquoteDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var chargesbyquote = await chargesbyquoteService.GetById(id.Value);

            if (chargesbyquote == null)
                return NotFound();

            var chargesbyquoteDTO = mapper.Map<ChargesByQuoteDTO>(chargesbyquote);

            return View(chargesbyquoteDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await chargesbyquoteService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}