﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class DeliveryModeService : GenericService<DeliveryMode>, IDeliveryModeService
    {
        private IDeliveryModeRepository deliverymodeRepository;

        public DeliveryModeService(IDeliveryModeRepository deliverymodeRepository) : base(deliverymodeRepository)
        {
            this.deliverymodeRepository = deliverymodeRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await deliverymodeRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByDeliveryMode(int id)
        {
            return await deliverymodeRepository.GetStatusByDeliveryMode(id);
        }

    }
}
