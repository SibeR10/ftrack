﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using CustomerType = Angle.Models.CustomerType;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class CustomerTypeController : Controller
    {
        private readonly ICustomerTypeService customertypeService;
        private readonly IMapper mapper;

        public CustomerTypeController(ICustomerTypeService customertypeService,
            IMapper mapper)
        {
            this.customertypeService = customertypeService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await customertypeService.GetAll();
            ViewBag.Status = await customertypeService.GetStatus();
            var listCustomerType = data.Select(x => mapper.Map<CustomerTypeDTO>(x)).ToList();
            return View(listCustomerType);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await customertypeService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CustomerTypeDTO customertypeDTO)
        {
            
            if (ModelState.IsValid)
            {

                var customertype = mapper.Map<CustomerType>(customertypeDTO);

                customertype = await customertypeService.Insert(customertype);

                return RedirectToAction("Index");
            }

            return View(customertypeDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var customertype = await customertypeService.GetById(id.Value);

            if (customertype == null)
                return NotFound();

            ViewBag.Status = await customertypeService.GetStatus();
            var customertypeDTO = mapper.Map<CustomerTypeDTO>(customertype);
            return View(customertypeDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CustomerTypeDTO customertypeDTO)
        {
            if (ModelState.IsValid)
            {
                var customertype = mapper.Map<CustomerType>(customertypeDTO);

                customertype = await customertypeService.Update(customertype);

                return RedirectToAction("Index");
            }

            return View(customertypeDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var customertype = await customertypeService.GetById(id.Value);

            if (customertype == null)
                return NotFound();

            var customertypeDTO = mapper.Map<CustomerTypeDTO>(customertype);

            return View(customertypeDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await customertypeService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}