﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IDocumentTypeService : IGenericService<DocumentType>
    {
        Task<IEnumerable<Status>> GetStatus();

        Task<IEnumerable<Status>> GetStatusByDocumentType(int id);
    }
}
