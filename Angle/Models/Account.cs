﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations.Schema;
namespace Angle.Models
{
    public class Account
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AccountID { get; set; }
        public int CreditLimit { get; set; }
        public DateTime StartDate { get; set; }
        public PaymentTerms PaymentTerms { get; set; }
        public int PaymentTermsID{ get; set; }
        public PaymentType PaymentType { get; set; }
        public int PaymentTypeID { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }

    }
}
