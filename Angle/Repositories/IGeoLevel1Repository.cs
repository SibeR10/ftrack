﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IGeoLevel1Repository : IGenericRepository<GeoLevel1>
    {
        Task<IEnumerable<Country>> GetCountry();

        Task<IEnumerable<Country>> GetCountryByGeoLevel1(int id);
    }
}
