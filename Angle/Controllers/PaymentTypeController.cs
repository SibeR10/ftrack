﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using PaymentType = Angle.Models.PaymentType;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class PaymentTypeController : Controller
    {
        private readonly IPaymentTypeService paymenttypeService;
        private readonly IMapper mapper;

        public PaymentTypeController(IPaymentTypeService paymenttypeService,
            IMapper mapper)
        {
            this.paymenttypeService = paymenttypeService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await paymenttypeService.GetAll();
            ViewBag.Status = await paymenttypeService.GetStatus();
            var listPaymentType = data.Select(x => mapper.Map<PaymentTypeDTO>(x)).ToList();
            return View(listPaymentType);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await paymenttypeService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(PaymentTypeDTO paymenttypeDTO)
        {
            
            if (ModelState.IsValid)
            {

                var paymenttype = mapper.Map<PaymentType>(paymenttypeDTO);

                paymenttype = await paymenttypeService.Insert(paymenttype);

                return RedirectToAction("Index");
            }

            return View(paymenttypeDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var paymenttype = await paymenttypeService.GetById(id.Value);

            if (paymenttype == null)
                return NotFound();

            ViewBag.Status = await paymenttypeService.GetStatus();
            var paymenttypeDTO = mapper.Map<PaymentTypeDTO>(paymenttype);
            return View(paymenttypeDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(PaymentTypeDTO paymenttypeDTO)
        {
            if (ModelState.IsValid)
            {
                var paymenttype = mapper.Map<PaymentType>(paymenttypeDTO);

                paymenttype = await paymenttypeService.Update(paymenttype);

                return RedirectToAction("Index");
            }

            return View(paymenttypeDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var paymenttype = await paymenttypeService.GetById(id.Value);

            if (paymenttype == null)
                return NotFound();

            var paymenttypeDTO = mapper.Map<PaymentTypeDTO>(paymenttype);

            return View(paymenttypeDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await paymenttypeService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}