﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IDriverRepository : IGenericRepository<Driver>
    {
        Task<IEnumerable<Status>> GetStatus();
        Task<IEnumerable<Status>> GetStatusByDriver(int id);

        Task<IEnumerable<Division>> GetDivision();
        Task<IEnumerable<Division>> GetDivisionByDriver(int id);

        Task<IEnumerable<DriverType>> GetDriverType();
        Task<IEnumerable<DriverType>> GetDriverTypeByDriver(int id);

        Task<IEnumerable<DocumentType>> GetDocumentType();
        Task<IEnumerable<DocumentType>> GetDocumentTypeByDriver(int id);

        
    }
}
