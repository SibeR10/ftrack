﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class DocumentTypeService : GenericService<DocumentType>, IDocumentTypeService
    {
        private IDocumentTypeRepository documenttypeRepository;

        public DocumentTypeService(IDocumentTypeRepository documenttypeRepository) : base(documenttypeRepository)
        {
            this.documenttypeRepository = documenttypeRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await documenttypeRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByDocumentType(int id)
        {
            return await documenttypeRepository.GetStatusByDocumentType(id);
        }

    }
}
