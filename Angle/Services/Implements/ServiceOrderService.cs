﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Services.Implements
{
    public class ServiceOrderService : GenericService<ServiceOrder>, IServiceOrderService
    {
        private IServiceOrderRepository ServiceOrderRepository;

        public ServiceOrderService(IServiceOrderRepository ServiceOrderRepository) : base(ServiceOrderRepository)
        {
            this.ServiceOrderRepository = ServiceOrderRepository;
        }

        public async Task<IEnumerable<DeliveryMode>> GetDeliveryMode()
        {
            return await ServiceOrderRepository.GetDeliveryMode();
        }
        
        public async Task<IEnumerable<ServiceOrderStatus>> GetStatus()
        {
            return await ServiceOrderRepository.GetStatus();
        }


        public async Task<IEnumerable<Quote>> GetQuote()
        {
            return await ServiceOrderRepository.GetQuote();
        }


        public async Task<IEnumerable<Customer>> GetCustomer()
        {
            return await ServiceOrderRepository.GetCustomer();
        }
        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {
            return await ServiceOrderRepository.GetServiceType();
        }
        public async Task<IEnumerable<SalesPerson>> GetSalesPerson()
        {
            return await ServiceOrderRepository.GetSalesPerson();
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await ServiceOrderRepository.GetGeoLevel1();
        }
        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {
            return await ServiceOrderRepository.GetWarehouse();
        }
        public async Task<IEnumerable<ItemsQuote>> GetItemsQuote()
        {
            return await ServiceOrderRepository.GetItemsQuote();
        }
        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {
            return await ServiceOrderRepository.GetMerchandiseType();
        }
        public async Task<IEnumerable<ItemsServiceOrder>> GetItemsServiceOrder()
        {
            return await ServiceOrderRepository.GetItemsServiceOrder();
        }
        public async Task<IEnumerable<ContractSpecial>> GetContractSpecial()
        {
            return await ServiceOrderRepository.GetContractSpecial();
        }
        public async Task<IEnumerable<ContractSequence>> GetContractSequence()
        {
            return await ServiceOrderRepository.GetContractSequence();
        }
        public async Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence()
        {
            return await ServiceOrderRepository.GetContractSpecialSequence();
        }
        public async Task<IEnumerable<Person>> GetRemittee()
        {
            return await ServiceOrderRepository.GetRemittee();
        }
        public async Task<IEnumerable<Person>> GetSender()
        {
            return await ServiceOrderRepository.GetSender();
        }
        public async Task<IEnumerable<Person>> GetSenderInfo(int id)
        {
            return await ServiceOrderRepository.GetSenderInfo(id);
        }



    }
}
