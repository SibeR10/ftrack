﻿using Angle.Data;
using Angle.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;
using Angle.Services;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Objects;
using System.Data;

namespace Angle.Controllers
{
    public class ReportesController : Controller
    {

        private readonly FTContext _context;

        private readonly IReportesService reportesService;
        private readonly IMapper mapper;
        public ReportesController(FTContext context, IReportesService reportesService,
            IMapper mapper)
        {
            _context = context;
            this.reportesService = reportesService;
            this.mapper = mapper;
        }
        private static SqlConnection NewDBConnection()
        {
            SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");

            conexion.Open();
            return conexion;
        }
        public async Task<IActionResult> Reporte1Async(int? FilterDivision, int? FilterTipoCliente, int? FilterEstatus)
        {
            ViewBag.Division = await _context.Division.ToListAsync();
            ViewBag.CustomerType = await _context.CustomerType.ToListAsync();
            ViewBag.DocumentType = await _context.DocumentType.ToListAsync();
            ViewBag.Status = await _context.Status.ToListAsync();
            ViewBag.SalesPerson = await _context.SalesPerson.ToListAsync();

           
            using (SqlConnection con = NewDBConnection())
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[FT_Reporte1]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@DivisionID", FilterDivision);
                    cmd.Parameters.AddWithValue("@CustomerTypeID", FilterTipoCliente);
                    cmd.Parameters.AddWithValue("@StatusID", FilterEstatus);
                    using (SqlDataReader r = cmd.ExecuteReader())
                    {
                        List<Customer> customer = new List<Customer>();
                        while (r.Read())
                        {

                            customer.Add(new Customer()
                            {
                                //Asignar los valores de las propiedades:
                                //DivisionName = (string)r["DivisionName"],
                                CustomerID = (int)r["CustomerID"],
                                //CustomerTypeName = (string)r["CustomerTypeName"],
                                //DocumentTypeName = (string)r["DocumentTypeName"],
                                Document = (int)r["Document"],
                                CompanyName = (string)r["CompanyName"],
                                FirstName = (string)r["FirstName"],
                                LastName = (string)r["LastName"],
                                //SalesPersonName = (string)r["SalesPersonName"],
                                MainPhone = (int)r["MainPhone"],
                                SecondPhone = (int)r["SecondPhone"],
                                Email = (string)r["Email"],
                                //StatusName = (string)r["StatusName"],
                                //StatusColor = (string)r["StatusColor"]
                            });

                            ViewBag.Customer = customer.ToList();

                        }
                    }
                }
            }



           
            return View();
        }


        public async Task<IActionResult> Reporte2Async(int? FilterCustomer)
        {


            ViewBag.Customer = await reportesService.GetCustomer();
            ViewBag.Division = await _context.Division.ToListAsync();
            ViewBag.CustomerType = await _context.CustomerType.ToListAsync();
            ViewBag.CustomerAverage = await _context.ServiceOrder.ToListAsync();
            ViewBag.Status = await _context.Status.ToListAsync();
            using (SqlConnection con = NewDBConnection())
            {
                using (SqlCommand cmd = new SqlCommand("[dbo].[FT_Reporte2]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CustomerID", FilterCustomer);
                    using (SqlDataReader r = cmd.ExecuteReader())
                    {
                        List<Customer> customer = new List<Customer>();
                        while (r.Read())
                        {
                            customer.Add(new Customer()
                            {
                                //Asignar los valores de las propiedades:
                                //DivisionName = (string)r["DivisionName"],
                                CompanyName = (string)r["CompanyName"],
                                FirstName = (string)r["FirstName"],
                                LastName = (string)r["LastName"],
                                //CustomerTypeName = (string)r["CustomerTypeName"],
                               // Weight = (int)r["Weight"],
                               // Amount = (int)r["Amount"]

                            });

                            ViewBag.CustomerFinal = customer.ToList();

                        }
                    }
                }
            }



            return View();
        }

        public async Task<IActionResult> Reporte3Async()
        {
            int id = 1;

            ViewBag.Division = await _context.Division.ToListAsync();
            ViewBag.Country = await _context.Country.ToListAsync();
            ViewBag.Warehouse = await _context.Warehouse.ToListAsync();
            ViewBag.Customer = await _context.Customer.ToListAsync();
            ViewBag.Driver = await _context.Driver.ToListAsync();
            ViewBag.Vehicles = await _context.Vehicles.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.DeliveryCustomer = await _context.ServiceOrder.ToListAsync();

            return View();
        }

        public async Task<IActionResult> Reporte4Async()
        {
            int id = 1;

            ViewBag.Country = await _context.Country.ToListAsync();
            ViewBag.Warehouse = await _context.Warehouse.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.AccountStatus = await _context.ServiceOrder.ToListAsync();

            return View();
        }
        public async Task<IActionResult> Reporte5Async()
        {
            int id = 1;

            ViewBag.Division = await _context.Division.ToListAsync();
            ViewBag.Country = await _context.Country.ToListAsync();
            ViewBag.Warehouse = await _context.Warehouse.ToListAsync();
            ViewBag.Customer = await _context.Customer.ToListAsync();
            ViewBag.Driver = await _context.Driver.ToListAsync();
            ViewBag.Vehicles = await _context.Vehicles.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.DeliveriesProgress = await _context.ServiceOrder.ToListAsync();

            return View();
        }
        public async Task<IActionResult> Reporte6Async()
        {
            int id = 1;

            ViewBag.Division = await _context.Division.ToListAsync();
            ViewBag.Country = await _context.Country.ToListAsync();
            ViewBag.Warehouse = await _context.Warehouse.ToListAsync();
            ViewBag.Customer = await _context.Customer.ToListAsync();
            ViewBag.Driver = await _context.Driver.ToListAsync();
            ViewBag.Vehicles = await _context.Vehicles.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.DeliveriesOnTime = await _context.ServiceOrder.ToListAsync();

            return View();
        }
        public async Task<IActionResult> Reporte7Async()
        {
            int id = 1;

            ViewBag.Division = await _context.Division.ToListAsync();
            ViewBag.Country = await _context.Country.ToListAsync();
            ViewBag.Warehouse = await _context.Warehouse.ToListAsync();
            ViewBag.Customer = await _context.Customer.ToListAsync();
            ViewBag.Driver = await _context.Driver.ToListAsync();
            ViewBag.Vehicles = await _context.Vehicles.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.DeliveriesOutTime = await _context.ServiceOrder.ToListAsync();

            return View();
        }
        public async Task<IActionResult> Reporte8Async()
        {
            int id = 1;

            ViewBag.Division = await _context.Division.ToListAsync();
            ViewBag.Country = await _context.Country.ToListAsync();
            ViewBag.Warehouse = await _context.Warehouse.ToListAsync();
            ViewBag.Customer = await _context.Customer.ToListAsync();
            ViewBag.Driver = await _context.Driver.ToListAsync();
            ViewBag.Vehicles = await _context.Vehicles.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.PendingExpiredDeliveries = await _context.ServiceOrder.ToListAsync();

            return View();
        }
        public async Task<IActionResult> Reporte9Async()
        {
            int id = 1;

            ViewBag.Division = await _context.Division.ToListAsync();
            ViewBag.Country = await _context.Country.ToListAsync();
            ViewBag.Warehouse = await _context.Warehouse.ToListAsync();
            ViewBag.Customer = await _context.Customer.ToListAsync();
            ViewBag.Driver = await _context.Driver.ToListAsync();
            ViewBag.Vehicles = await _context.Vehicles.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.DriverDeliveries = await _context.ServiceOrder.ToListAsync();

            return View();
        }
        public async Task<IActionResult> Reporte10Async()
        {
            int id = 1;

            ViewBag.Division = await _context.Division.ToListAsync();
            ViewBag.Country = await _context.Country.ToListAsync();
            ViewBag.Warehouse = await _context.Warehouse.ToListAsync();
            ViewBag.Customer = await _context.Customer.ToListAsync();
            ViewBag.Driver = await _context.Driver.ToListAsync();
            ViewBag.Vehicles = await _context.Vehicles.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.VehiclesDeliveries = await _context.ServiceOrder.ToListAsync();

            return View();
        }
        public async Task<IActionResult> Reporte11Async()
        {
            int id = 1;

            ViewBag.Division = await _context.Division.ToListAsync();
            ViewBag.Country = await _context.Country.ToListAsync();
            ViewBag.Warehouse = await _context.Warehouse.ToListAsync();
            ViewBag.Customer = await _context.Customer.ToListAsync();
            ViewBag.Driver = await _context.Driver.ToListAsync();
            ViewBag.Vehicles = await _context.Vehicles.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.Income = await _context.ServiceOrder.ToListAsync();

            return View();
        }
        public async Task<IActionResult> Reporte12Async()
        {
            int id = 1;

            ViewBag.Division = await _context.Division.ToListAsync();
            ViewBag.Customer = await _context.Customer.ToListAsync();
            ViewBag.Driver = await _context.Driver.ToListAsync();
            ViewBag.Vehicles = await _context.Vehicles.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.HistoricFuel = await _context.HistoricFuel.ToListAsync();
            ViewBag.Expense = await _context.ServiceOrder.ToListAsync();

            return View();
        }
    }
}