﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface ITravelService : IGenericService<Travel>
    {
        Task<IEnumerable<Status>> GetStatus();
        Task<IEnumerable<GeoLevel1>> GetOrigin();
        Task<IEnumerable<GeoLevel1>> GetDestination();
        Task<IEnumerable<Vehicles>> GetVehicles();
        Task<IEnumerable<Driver>> GetDriver();
        Task<IEnumerable<HistoricTravel>> GetHistoricTravel();
        Task<IEnumerable<ItemsServiceOrder>> GetItemsServiceOrder();
        Task<IEnumerable<ServiceOrder>> GetServiceOrder();
        Task<IEnumerable<ItemsQuote>> GetItemsQuote();
        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();
        Task<IEnumerable<Customer>> GetCustomer();
        Task<IEnumerable<StopTravel>> GetStopTravel();

    }
}
