﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using ApplyBy = Angle.Models.ApplyBy;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class ApplyByController : Controller
    {
        private readonly IApplyByService applybyService;
        private readonly IMapper mapper;

        public ApplyByController(IApplyByService applybyService,
            IMapper mapper)
        {
            this.applybyService = applybyService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await applybyService.GetAll();
            ViewBag.Status = await applybyService.GetStatus();
            var listApplyBy = data.Select(x => mapper.Map<ApplyByDTO>(x)).ToList();
            return View(listApplyBy);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await applybyService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ApplyByDTO applybyDTO)
        {
            
            if (ModelState.IsValid)
            {

                var applyby = mapper.Map<ApplyBy>(applybyDTO);

                applyby = await applybyService.Insert(applyby);

                return RedirectToAction("Index");
            }

            return View(applybyDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var applyby = await applybyService.GetById(id.Value);

            if (applyby == null)
                return NotFound();

            ViewBag.Status = await applybyService.GetStatus();
            var applybyDTO = mapper.Map<ApplyByDTO>(applyby);
            return View(applybyDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ApplyByDTO applybyDTO)
        {
            if (ModelState.IsValid)
            {
                var applyby = mapper.Map<ApplyBy>(applybyDTO);

                applyby = await applybyService.Update(applyby);

                return RedirectToAction("Index");
            }

            return View(applybyDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var applyby = await applybyService.GetById(id.Value);

            if (applyby == null)
                return NotFound();

            var applybyDTO = mapper.Map<ApplyByDTO>(applyby);

            return View(applybyDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await applybyService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}