﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Travel = Angle.Models.Travel;
using Angle.Services.Implements;
using System.Data.SqlClient;

namespace Angle.Controllers
{
    public class TravelController : Controller
    {
        private readonly ITravelService travelService;
        private readonly IMapper mapper;

        public TravelController(ITravelService travelService,
            IMapper mapper)
        {
            this.travelService = travelService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await travelService.GetAll();
            ViewBag.Status = await travelService.GetStatus();
            ViewBag.Origin = await travelService.GetOrigin();
            ViewBag.Destination = await travelService.GetDestination();
            var listTravel = data.Select(x => mapper.Map<TravelDTO>(x)).ToList();
            return View(listTravel);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await travelService.GetStatus();
            ViewBag.Origin = await travelService.GetOrigin();
            ViewBag.Destination = await travelService.GetDestination();
            ViewBag.Vehicles = await travelService.GetVehicles();
            ViewBag.Driver = await travelService.GetDriver();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(TravelDTO travelDTO)
        {
            
            if (ModelState.IsValid)
            {

                var travel = mapper.Map<Travel>(travelDTO);

                travel = await travelService.Insert(travel);

                return RedirectToAction("Index");
            }

            return View(travelDTO);
        }

        [HttpPost]
        public async Task<IActionResult> CreateItem(int TravelID ,int ServiceOrderID)
        {
            SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
            conexion.Open();
            string cadena = "INSERT INTO [dbo].[FT_HistoricTravel]([TravelID] ,[ServiceOrderID])VALUES("+TravelID+","+ServiceOrderID+")";
            SqlCommand comando = new SqlCommand(cadena, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            var url = "/Edit/" + TravelID;
            return RedirectToAction(url);
        }

        [HttpPost]
        public async Task<IActionResult> CreateStopTravel(int TravelID ,int StopID)
        {
            SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
            conexion.Open();
            string cadena = "INSERT INTO [dbo].[FT_StopTravel]([StopID],[TravelID])VALUES(" + StopID + "," + TravelID + ")";
            SqlCommand comando = new SqlCommand(cadena, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            var url = "/Edit/" + TravelID;
            return RedirectToAction(url);
        }
        
        
        public async Task<IActionResult> DeleteParada(int TravelID ,int StopID)
        {
            SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
            conexion.Open();
            string cadena = "DELETE FROM [dbo].[FT_StopTravel] WHERE TravelID = "+TravelID+" AND StopID = "+StopID+"";
            SqlCommand comando = new SqlCommand(cadena, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            var url = "/Edit/" + TravelID;
            return RedirectToAction(url);
        }
        
        public async Task<IActionResult> DeleteItem(int id)
        {


            SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
            conexion.Open();
            string cadena = "DELETE FROM [dbo].[FT_HistoricTravel] WHERE ServiceOrderID = " + id + "";
            SqlCommand comando = new SqlCommand(cadena, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            var url = "/Index";
            return RedirectToAction(url);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var travel = await travelService.GetById(id.Value);

            if (travel == null)
                return NotFound();

            ViewBag.Status = await travelService.GetStatus();
            ViewBag.Origin = await travelService.GetOrigin();
            ViewBag.Destination = await travelService.GetDestination();
            ViewBag.Vehicles = await travelService.GetVehicles();
            ViewBag.Driver = await travelService.GetDriver();
            ViewBag.HistoricTravel = await travelService.GetHistoricTravel();
            ViewBag.ItemsQuote = await travelService.GetItemsQuote();
            ViewBag.ItemsServiceOrder = await travelService.GetItemsServiceOrder();
            ViewBag.ServiceOrder = await travelService.GetServiceOrder();
            ViewBag.GeoLevel1 = await travelService.GetGeoLevel1();
            ViewBag.Customer = await travelService.GetCustomer();
            ViewBag.StopTravel = await travelService.GetStopTravel();
            var travelDTO = mapper.Map<TravelDTO>(travel);
            return View(travelDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TravelDTO travelDTO)
        {
            if (ModelState.IsValid)
            {
                var travel = mapper.Map<Travel>(travelDTO);

                travel = await travelService.Update(travel);

                return RedirectToAction("Index");
            }

            return View(travelDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var travel = await travelService.GetById(id.Value);

            if (travel == null)
                return NotFound();

            var travelDTO = mapper.Map<TravelDTO>(travel);

            return View(travelDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await travelService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}