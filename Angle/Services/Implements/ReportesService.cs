﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ReportesService : GenericService<Customer>, IReportesService
    {
        private IReportesRepository reportesRepository;

        public ReportesService(IReportesRepository reportesRepository) : base(reportesRepository)
        {
            this.reportesRepository = reportesRepository;
        }

        public async Task<IEnumerable<Customer>> GetFilterCustomer(int division, int CustomerTypeID, int StatusID)
        {
            return await reportesRepository.GetFilterCustomer(division, CustomerTypeID, StatusID);
        }
        public async Task<IEnumerable<Customer>> GetCustomer()
        {
            return await reportesRepository.GetCustomer();
        }

        
    }
}
