﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class TravelService : GenericService<Travel>, ITravelService
    {
        private ITravelRepository travelRepository;

        public TravelService(ITravelRepository travelRepository) : base(travelRepository)
        {
            this.travelRepository = travelRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await travelRepository.GetStatus();
        }
        public async Task<IEnumerable<GeoLevel1>> GetOrigin()
        {
            return await travelRepository.GetOrigin();
        }
        public async Task<IEnumerable<GeoLevel1>> GetDestination()
        {
            return await travelRepository.GetDestination();
        }
        public async Task<IEnumerable<Driver>> GetDriver()
        {
            return await travelRepository.GetDriver();
        }
        public async Task<IEnumerable<Vehicles>> GetVehicles()
        {
            return await travelRepository.GetVehicles();
        }
        public async Task<IEnumerable<HistoricTravel>> GetHistoricTravel()
        {
            return await travelRepository.GetHistoricTravel();
        }
        public async Task<IEnumerable<ItemsServiceOrder>> GetItemsServiceOrder()
        {
            return await travelRepository.GetItemsServiceOrder();
        }
        public async Task<IEnumerable<ServiceOrder>> GetServiceOrder()
        {
            return await travelRepository.GetServiceOrder();
        }
        public async Task<IEnumerable<ItemsQuote>> GetItemsQuote()
        {
            return await travelRepository.GetItemsQuote();
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await travelRepository.GetGeoLevel1();
        }
        public async Task<IEnumerable<Customer>> GetCustomer()
        {
            return await travelRepository.GetCustomer();
        }
        public async Task<IEnumerable<StopTravel>> GetStopTravel()
        {
            return await travelRepository.GetStopTravel();
        }
    }
}
