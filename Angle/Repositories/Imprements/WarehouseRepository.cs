﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class WarehouseRepository : GenericRepository<Warehouse>, IWarehouseRepository
    {
        private readonly FTContext FTContext;

        public WarehouseRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }

        public async Task<IEnumerable<Status>> GetStatusByWarehouse(int id)
        {

            var listStatusByWarehouse = await FTContext.Warehouse
                .Include(x => x.Status)
                .Where(x => x.StatusID == id)
                .Select(x => x.Status)
                .ToListAsync();

            return listStatusByWarehouse;
        }
        public async Task<IEnumerable<Division>> GetDivision()
        {

            var listDivision = await FTContext.Division.ToListAsync();

            return listDivision;
        }

        public async Task<IEnumerable<Division>> GetDivisionByWarehouse(int id)
        {

            var listDivisionByWarehouse = await FTContext.Warehouse
                .Include(x => x.Division)
                .Where(x => x.DivisionID == id)
                .Select(x => x.Division)
                .ToListAsync();

            return listDivisionByWarehouse;
        }

        
    }
}
