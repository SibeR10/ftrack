﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ItemsReservation
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemsReservationID { get; set; }

              

        public Reservation Reservation { get; set; }
        public int ReservationID { get; set; }
        public MerchandiseType MerchandiseType { get; set; }
        public int MerchandiseTypeID { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public int Weight { get; set; }
        public int Amount { get; set; }
    }
}

