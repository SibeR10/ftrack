﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using AccountType = Angle.Models.AccountType;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class AccountTypeController : Controller
    {
        private readonly IAccountTypeService accounttypeService;
        private readonly IMapper mapper;

        public AccountTypeController(IAccountTypeService accounttypeService,
            IMapper mapper)
        {
            this.accounttypeService = accounttypeService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await accounttypeService.GetAll();
            ViewBag.Status = await accounttypeService.GetStatus();
            var listAccountType = data.Select(x => mapper.Map<AccountTypeDTO>(x)).ToList();
            return View(listAccountType);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await accounttypeService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(AccountTypeDTO accounttypeDTO)
        {
            
            if (ModelState.IsValid)
            {

                var accounttype = mapper.Map<AccountType>(accounttypeDTO);

                accounttype = await accounttypeService.Insert(accounttype);

                return RedirectToAction("Index");
            }

            return View(accounttypeDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var accounttype = await accounttypeService.GetById(id.Value);

            if (accounttype == null)
                return NotFound();

            ViewBag.Status = await accounttypeService.GetStatus();
            var accounttypeDTO = mapper.Map<AccountTypeDTO>(accounttype);
            return View(accounttypeDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(AccountTypeDTO accounttypeDTO)
        {
            if (ModelState.IsValid)
            {
                var accounttype = mapper.Map<AccountType>(accounttypeDTO);

                accounttype = await accounttypeService.Update(accounttype);

                return RedirectToAction("Index");
            }

            return View(accounttypeDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var accounttype = await accounttypeService.GetById(id.Value);

            if (accounttype == null)
                return NotFound();

            var accounttypeDTO = mapper.Map<AccountTypeDTO>(accounttype);

            return View(accounttypeDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await accounttypeService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}