﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IGeoLevel1Service : IGenericService<GeoLevel1>
    {
        Task<IEnumerable<Country>> GetCountry();

        Task<IEnumerable<Country>> GetCountryByGeoLevel1(int id);
    }
}
