﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ContractService : GenericService<Contract>, IContractService
    {
        private IContractRepository contractRepository;

        public ContractService(IContractRepository contractRepository) : base(contractRepository)
        {
            this.contractRepository = contractRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await contractRepository.GetStatus();
        }

        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {
            return await contractRepository.GetServiceType();
        }

        public async Task<IEnumerable<ContractSequence>> GetContractSequence()
        {
            return await contractRepository.GetContractSequence();
        }

        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await contractRepository.GetGeoLevel1();
        }
        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {
            return await contractRepository.GetWarehouse();
        }
        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {
            return await contractRepository.GetMerchandiseType();
        }
        public async Task<IEnumerable<DeliveryMode>> GetDeliveryMode()
        {
            return await contractRepository.GetDeliveryMode();
        }

    }
}
