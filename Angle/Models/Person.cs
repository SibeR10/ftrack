﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Person
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PersonID { get; set; }
        public string PersonName { get; set; }
        public int PersonDocument { get; set; }
        public int PersonTelepone { get; set; }
        public string PersonEmail { get; set; }
    }
}		