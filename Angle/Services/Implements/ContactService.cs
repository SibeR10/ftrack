﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ContactService : GenericService<Contact>, IContactService
    {
        private IContactRepository contactRepository;

        public ContactService(IContactRepository contactRepository) : base(contactRepository)
        {
            this.contactRepository = contactRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await contactRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByContact(int id)
        {
            return await contactRepository.GetStatusByContact(id);
        }


        public async Task<IEnumerable<Customer>> GetCustomerById(int id)
        {
            return await contactRepository.GetCustomerById(id);
        }

        public async Task<IEnumerable<Customer>> GetCustomer()
        {
            return await contactRepository.GetCustomer();
        }


    }
}
