﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class GeoLevel1Service : GenericService<GeoLevel1>, IGeoLevel1Service
    {
        private IGeoLevel1Repository geolevel1Repository;

        public GeoLevel1Service(IGeoLevel1Repository geolevel1Repository) : base(geolevel1Repository)
        {
            this.geolevel1Repository = geolevel1Repository;
        }

        public async Task<IEnumerable<Country>> GetCountry()
        {
            return await geolevel1Repository.GetCountry();
        }

        public async Task<IEnumerable<Country>> GetCountryByGeoLevel1(int id)
        {
            return await geolevel1Repository.GetCountryByGeoLevel1(id);
        }

    }
}
