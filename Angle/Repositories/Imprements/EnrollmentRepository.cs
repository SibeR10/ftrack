﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class EnrollmentRepository : GenericRepository<Enrollment>, IEnrollmentRepository
    {
        private readonly FTContext FTContext;

        public EnrollmentRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }

        public new async Task<List<Enrollment>> GetAll()
        {
            var listEnrollments = await FTContext.Enrollments.Include(x => x.Course)
                .Include(x => x.Student)
                .ToListAsync();

            return listEnrollments;
        }
    }
}
