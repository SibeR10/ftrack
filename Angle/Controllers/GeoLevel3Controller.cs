﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using GeoLevel3 = Angle.Models.GeoLevel3;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class GeoLevel3Controller : Controller
    {
        private readonly IGeoLevel3Service geolevel3Service;
        private readonly IMapper mapper;

        public GeoLevel3Controller(IGeoLevel3Service geolevel3Service,
            IMapper mapper)
        {
            this.geolevel3Service = geolevel3Service;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await geolevel3Service.GetAll();
            ViewBag.GeoLevel2 = await geolevel3Service.GetGeoLevel2();
            var listGeoLevel3 = data.Select(x => mapper.Map<GeoLevel3DTO>(x)).ToList();
            return View(listGeoLevel3);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.GeoLevel2 = await geolevel3Service.GetGeoLevel2();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(GeoLevel3DTO geolevel3DTO)
        {
            
            if (ModelState.IsValid)
            {

                var geolevel3 = mapper.Map<GeoLevel3>(geolevel3DTO);

                geolevel3 = await geolevel3Service.Insert(geolevel3);

                return RedirectToAction("Index");
            }

            return View(geolevel3DTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var geolevel3 = await geolevel3Service.GetById(id.Value);

            if (geolevel3 == null)
                return NotFound();

            ViewBag.GeoLevel2 = await geolevel3Service.GetGeoLevel2();
            var geolevel3DTO = mapper.Map<GeoLevel3DTO>(geolevel3);
            return View(geolevel3DTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(GeoLevel3DTO geolevel3DTO)
        {
            if (ModelState.IsValid)
            {
                var geolevel3 = mapper.Map<GeoLevel3>(geolevel3DTO);

                geolevel3 = await geolevel3Service.Update(geolevel3);

                return RedirectToAction("Index");
            }

            return View(geolevel3DTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var geolevel3 = await geolevel3Service.GetById(id.Value);

            if (geolevel3 == null)
                return NotFound();

            var geolevel3DTO = mapper.Map<GeoLevel3DTO>(geolevel3);

            return View(geolevel3DTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await geolevel3Service.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}