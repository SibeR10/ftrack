﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IWarehouseService : IGenericService<Warehouse>
    {
        Task<IEnumerable<Status>> GetStatus();

        Task<IEnumerable<Status>> GetStatusByWarehouse(int id);
        Task<IEnumerable<Division>> GetDivision();

        Task<IEnumerable<Division>> GetDivisionByWarehouse(int id);
    }
}
