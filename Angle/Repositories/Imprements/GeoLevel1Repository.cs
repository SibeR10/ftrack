﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class GeoLevel1Repository : GenericRepository<GeoLevel1>, IGeoLevel1Repository
    {
        private readonly FTContext FTContext;

        public GeoLevel1Repository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<Country>> GetCountry()
        {

            var listCountry = await FTContext.Country.ToListAsync();

            return listCountry;
        }

        public async Task<IEnumerable<Country>> GetCountryByGeoLevel1(int id)
        {

            var listCountryByGeoLevel1 = await FTContext.GeoLevel1
                .Include(x => x.Country)
                .Where(x => x.CountryID == id)
                .Select(x => x.Country)
                .ToListAsync();

            return listCountryByGeoLevel1;
        }

        
    }
}
