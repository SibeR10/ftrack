﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ItemsQuote
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemsQuoteID { get; set; }

              

        public Quote Quote { get; set; }
        public int QuoteID { get; set; }
        public MerchandiseType MerchandiseType { get; set; }
        public int MerchandiseTypeID { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public int Weight { get; set; }
        public int Amount { get; set; }
    }
}

