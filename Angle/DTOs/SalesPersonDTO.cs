﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class SalesPersonDTO
    {
        public int SalesPersonID { get; set; }

        [Required(ErrorMessage = "The SalesPersonName is required")]
        [Display(Name = "Vendedor")]
        public string SalesPersonName { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

