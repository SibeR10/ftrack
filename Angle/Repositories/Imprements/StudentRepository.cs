﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class StudentRepository : GenericRepository<Student>, IStudentRepository
    {
        private readonly FTContext FTContext;

        public StudentRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }

        public async Task<IEnumerable<Course>> GetCoursesByStudentId(int id) {

            var listCourses = await FTContext.Enrollments
                .Include(x => x.Course)
                .Where(x => x.StudentID == id)
                .Select(x => x.Course)
                .ToListAsync();

            return listCourses;
        }

        public new async Task Delete(int id)
        {
            var entity = await GetById(id);

            if (entity == null)
                throw new Exception("The entity is null");            

            var flag = FTContext.Enrollments.Any(x => x.StudentID == id);

            if (flag)
                throw new Exception("The student have enrollments");

            FTContext.Students.Remove(entity);
            await FTContext.SaveChangesAsync();
        }
    }
}
