﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Contract = Angle.Models.Contract;
using Angle.Services.Implements;

using System.Data;
using Microsoft.Data.SqlClient;
namespace Angle.Controllers
{
    public class ContractController : Controller
    {
        private readonly IContractService contractService;
        private readonly IMapper mapper;

        public ContractController(IContractService contractService,
            IMapper mapper)
        {
            this.contractService = contractService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await contractService.GetAll();
            ViewBag.Status = await contractService.GetStatus();
            ViewBag.ServiceType = await contractService.GetServiceType();
            var listContract = data.Select(x => mapper.Map<ContractDTO>(x)).ToList();
            return View(listContract);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await contractService.GetStatus();
            ViewBag.ServiceType = await contractService.GetServiceType();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ContractDTO contractDTO)
        {
            
            if (ModelState.IsValid)
            {

                var contract = mapper.Map<Contract>(contractDTO);

                contract = await contractService.Insert(contract);

                return RedirectToAction("Index");
            }

            return View(contractDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var contract = await contractService.GetById(id.Value);

            if (contract == null)
                return NotFound();

            ViewBag.Status = await contractService.GetStatus();
            ViewBag.ServiceType = await contractService.GetServiceType();
            ViewBag.ContractSequence = await contractService.GetContractSequence();
            ViewBag.GeoLevel1 = await contractService.GetGeoLevel1();
            ViewBag.Warehouse = await contractService.GetWarehouse();
            ViewBag.MerchandiseType = await contractService.GetMerchandiseType();
            ViewBag.DeliveryMode = await contractService.GetDeliveryMode();
            var contractDTO = mapper.Map<ContractDTO>(contract);
            return View(contractDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ContractDTO contractDTO)
        {
            if (ModelState.IsValid)
            {
                var contract = mapper.Map<Contract>(contractDTO);

                contract = await contractService.Update(contract);
                var url = "/Edit/" + contract.ContractID;
                return RedirectToAction(url);
            }

            return View(contractDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var contract = await contractService.GetById(id.Value);

            if (contract == null)
                return NotFound();

            var contractDTO = mapper.Map<ContractDTO>(contract);

            return View(contractDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await contractService.Delete(id.Value);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Import(IFormFile file, int ContractID)
        {
            var list = new List<ContractSequence>();
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowcount = worksheet.Dimension.Rows;
                    for (int row = 2; row <= rowcount; row++)
                    {
                        list.Add(new ContractSequence
                        {
                           
                            WeightMin = (int)(double)worksheet.Cells[row, 1].Value,
                            BasePrice = (int)(double)worksheet.Cells[row, 2].Value,
                            ExtraPrice = (int)(double)worksheet.Cells[row, 3].Value,
                            MerchandiseTypeID = (int)(double)worksheet.Cells[row, 4].Value,
                            DeliveryModeID = (int)(double)worksheet.Cells[row, 5].Value,
                            OriginID = (int)(double)worksheet.Cells[row, 6].Value,
                            DestinationID = (int)(double)worksheet.Cells[row, 7].Value,
                            ServiceTypeID = (int)(double)worksheet.Cells[row, 8].Value,
                        });
                    }
                }
            }

            list.ForEach(p =>
            {
            
                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();
                int WeightMin = p.WeightMin;
                int BasePrice = p.BasePrice;
                int ExtraPrice = p.ExtraPrice;
                int MerchandiseTypeID = p.MerchandiseTypeID;
                int DeliveryModeID = p.DeliveryModeID;
                int OriginID = p.OriginID;
                int DestinationID = p.DestinationID;
                int ServiceTypeID = p.ServiceTypeID;
                string cadena = "INSERT INTO [dbo].[FT_ContractSequence]([ContractID],[WeightMin],[BasePrice],[ExtraPrice],[MerchandiseTypeID],[DeliveryModeID],[OriginID],[DestinationID],[ServiceTypeID])VALUES("+ContractID+","+WeightMin+","+BasePrice+","+ExtraPrice+","+MerchandiseTypeID+","+DeliveryModeID+","+OriginID+","+DestinationID+","+ServiceTypeID+")";
                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();
                conexion.Close();

            });


            return RedirectToAction("Index");
        }




    }
}