﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ServiceTypeService : GenericService<ServiceType>, IServiceTypeService
    {
        private IServiceTypeRepository servicetypeRepository;

        public ServiceTypeService(IServiceTypeRepository servicetypeRepository) : base(servicetypeRepository)
        {
            this.servicetypeRepository = servicetypeRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await servicetypeRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByServiceType(int id)
        {
            return await servicetypeRepository.GetStatusByServiceType(id);
        }

    }
}
