﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class AccountTypeService : GenericService<AccountType>, IAccountTypeService
    {
        private IAccountTypeRepository accounttypeRepository;

        public AccountTypeService(IAccountTypeRepository accounttypeRepository) : base(accounttypeRepository)
        {
            this.accounttypeRepository = accounttypeRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await accounttypeRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByAccountType(int id)
        {
            return await accounttypeRepository.GetStatusByAccountType(id);
        }

    }
}
