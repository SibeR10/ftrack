﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class CurrencyDTO
    {
        public int CurrencyID { get; set; }

        [Required(ErrorMessage = "The Currency Name is required")]
        [Display(Name = "Moneda")]
        public string CurrencyName { get; set; }

        [Required(ErrorMessage = "The CurrencyDescription is required")]
        [Display(Name = "Descripción")]
        public string CurrencyDescription { get; set; }

        [Required(ErrorMessage = "The CurrencyChangeUSD is required")]
        [Display(Name = "Tasa de cambio a USD")]
        public int CurrencyChangeUSD { get; set; }

    }
}
