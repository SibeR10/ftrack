﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IContractMeasuryService : IGenericService<ContractMeasury>
    {
        Task<IEnumerable<ContractMeasury>> GetContractMeasury();
    }
}
