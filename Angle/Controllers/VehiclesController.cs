﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using System.Data;
using Microsoft.Data.SqlClient;
using Vehicles = Angle.Models.Vehicles;
using Angle.Services.Implements;
using System;

namespace Angle.Controllers
{
    public class VehiclesController : Controller
    {
        private readonly IVehiclesService vehiclesService;
        private readonly IMapper mapper;

        public VehiclesController(IVehiclesService vehiclesService,
            IMapper mapper)
        {
            this.vehiclesService = vehiclesService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await vehiclesService.GetAll();
            ViewBag.Status = await vehiclesService.GetStatus();
            ViewBag.Brand = await vehiclesService.GetBrand();
            ViewBag.Division = await vehiclesService.GetDivision();
            ViewBag.Driver = await vehiclesService.GetDriver();
            var listVehicles = data.Select(x => mapper.Map<VehiclesDTO>(x)).ToList();
            return View(listVehicles);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await vehiclesService.GetStatus();
            ViewBag.Brand = await vehiclesService.GetBrand();
            ViewBag.Division = await vehiclesService.GetDivision();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(VehiclesDTO vehiclesDTO)
        {
            
            if (ModelState.IsValid)
            {

                var vehicles = mapper.Map<Vehicles>(vehiclesDTO);

                vehicles = await vehiclesService.Insert(vehicles);

                return RedirectToAction("Index");
            }

            return View(vehiclesDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var vehicles = await vehiclesService.GetById(id.Value);

            if (vehicles == null)
                return NotFound();

            ViewBag.Status = await vehiclesService.GetStatus();
            ViewBag.Brand = await vehiclesService.GetBrand();
            ViewBag.Division = await vehiclesService.GetDivision();
            ViewBag.Driver = await vehiclesService.GetDriver();
            ViewBag.Fuel = await vehiclesService.GetFuel();
            ViewBag.HistoricDriver = await vehiclesService.GetHistoricDriver();
            ViewBag.HistoricFuel = await vehiclesService.GetHistoricFuel();
            ViewBag.DocumentUpload = await vehiclesService.GetDocumentUpload();
            var vehiclesDTO = mapper.Map<VehiclesDTO>(vehicles);
            return View(vehiclesDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VehiclesDTO vehiclesDTO)
        {
            if (ModelState.IsValid)
            {
                var vehicles = mapper.Map<Vehicles>(vehiclesDTO);

                vehicles = await vehiclesService.Update(vehicles);

                return RedirectToAction("Index");
            }

            return View(vehiclesDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var vehicles = await vehiclesService.GetById(id.Value);

            if (vehicles == null)
                return NotFound();

            var vehiclesDTO = mapper.Map<VehiclesDTO>(vehicles);

            return View(vehiclesDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await vehiclesService.Delete(id.Value);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Import(IFormFile file)
        {
            var list = new List<Vehicles>();
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowcount = worksheet.Dimension.Rows;
                    for (int row = 2; row <= rowcount; row++)
                    {
                        list.Add(new Vehicles
                        {
                            LicensePlateCar = worksheet.Cells[row, 1].Value.ToString().Trim(),
                            Enrollment = worksheet.Cells[row, 2].Value.ToString().Trim(),
                            ColourCar = worksheet.Cells[row, 3].Value.ToString().Trim(),
                            Equipment = worksheet.Cells[row, 4].Value.ToString().Trim(),
                            ModelCar = worksheet.Cells[row, 5].Value.ToString().Trim(),
                            YearCar = (int)(double)worksheet.Cells[row, 6].Value,
                            ChassisCar = worksheet.Cells[row, 7].Value.ToString().Trim(),
                            InsuranceCar = worksheet.Cells[row, 8].Value.ToString().Trim(),
                            ExpirationInsuranceCar = (DateTime)worksheet.Cells[row, 9].Value,
                            DivisionID = (int)(double)worksheet.Cells[row, 10].Value,
                            BrandID = (int)(double)worksheet.Cells[row, 11].Value,
                            StatusID = (int)(double)worksheet.Cells[row, 12].Value,
                            DriverID = (int)(double)worksheet.Cells[row, 13].Value,
                        });
                    }
                }
            }
           
            list.ForEach(p =>
            {

                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();
                string LicensePlateCar = p.LicensePlateCar;
                string Enrollment = p.Enrollment;
                string ColourCar = p.ColourCar;
                string Equipment = p.Equipment;
                string ModelCar = p.ModelCar;
                int YearCar = p.YearCar;
                string ChassisCar = p.ChassisCar;
                string InsuranceCar = p.InsuranceCar;
                string CreateUserTransaction = "Obtener desde el login";
                string UpdateUserTransaction = "Obtener desde el login";
                int DivisionID = p.DivisionID;
                int BrandID = p.BrandID;
                int StatusID = p.StatusID;
                int DriverID = p.DriverID;
                string cadena = "insert into [dbo].[FT_Vehicles]([LicensePlateCar],[Enrollment],[ColourCar],[Equipment],[ModelCar],[YearCar],[ChassisCar],[InsuranceCar],[ExpirationInsuranceCar],[CreateDateTransaction],[CreateUserTransaction],[UpdateDateTransaction],[UpdateUserTransaction],[DivisionID],[BrandID],[StatusID],[DriverID])" + " values ('" + LicensePlateCar + "','" + Enrollment + "','" + ColourCar + "','" + Equipment + "','" + ModelCar + "'," + YearCar + ",'" + ChassisCar + "','" + InsuranceCar + "','2021-05-03','2021-05-03','" + CreateUserTransaction + "','2021-05-03','" + UpdateUserTransaction + "'," + DivisionID + "," + BrandID + "," + StatusID + "," + DriverID + ")";
                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();
                conexion.Close();

            });


            return RedirectToAction("Index");
        }
    }
}