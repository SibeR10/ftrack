﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Services.Implements
{
    public class ServiceOrderStatusService : GenericService<ServiceOrderStatus>, IServiceOrderStatusService
    {
        private IServiceOrderStatusRepository ServiceOrderStatusRepository;

        public ServiceOrderStatusService(IServiceOrderStatusRepository ServiceOrderStatusRepository) : base(ServiceOrderStatusRepository)
        {
            this.ServiceOrderStatusRepository = ServiceOrderStatusRepository;
        }

    }
}
