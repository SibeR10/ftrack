﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;
using ServiceOrderStatus = Angle.Models.ServiceOrderStatus;

namespace Angle.Controllers
{
    public class ServiceOrderStatusController : Controller
    {
        private readonly IServiceOrderStatusService serviceorderstatusService;
        private readonly IMapper mapper;

        public ServiceOrderStatusController(IServiceOrderStatusService serviceorderstatusService,
            IMapper mapper)
        {
            this.serviceorderstatusService = serviceorderstatusService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await serviceorderstatusService.GetAll();

            var listServiceOrderStatus = data.Select(x => mapper.Map<ServiceOrderStatusDTO>(x)).ToList();

            return View(listServiceOrderStatus);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ServiceOrderStatusDTO serviceorderstatusDTO)
        {
            if (ModelState.IsValid)
            {

                var serviceorderstatus = mapper.Map<ServiceOrderStatus>(serviceorderstatusDTO);

                serviceorderstatus = await serviceorderstatusService.Insert(serviceorderstatus);


                return RedirectToAction("Index");
            }

            return View(serviceorderstatusDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var serviceorderstatus = await serviceorderstatusService.GetById(id.Value);

            if (serviceorderstatus == null)
                return NotFound();

            var serviceorderstatusDTO = mapper.Map<ServiceOrderStatusDTO>(serviceorderstatus);

            return View(serviceorderstatusDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ServiceOrderStatusDTO serviceorderstatusDTO)
        {
            if (ModelState.IsValid)
            {
                var serviceorderstatus = mapper.Map<ServiceOrderStatus>(serviceorderstatusDTO);

                serviceorderstatus = await serviceorderstatusService.Update(serviceorderstatus);
                return RedirectToAction("Index");
            }

            return View(serviceorderstatusDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var serviceorderstatus = await serviceorderstatusService.GetById(id.Value);

            if (serviceorderstatus == null)
                return NotFound();

            var serviceorderstatusDTO = mapper.Map<ServiceOrderStatusDTO>(serviceorderstatus);

            return View(serviceorderstatusDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await serviceorderstatusService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}