﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IContractSpecialService : IGenericService<ContractSpecial>
    {
        Task<IEnumerable<Status>> GetStatus();

        Task<IEnumerable<Customer>> GetCustomer();
        Task<IEnumerable<ServiceType>> GetServiceType();
        Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence();
        Task<IEnumerable<ContractSpecial>> GetContractSpecial();
        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();
        Task<IEnumerable<Warehouse>> GetWarehouse();
        Task<IEnumerable<MerchandiseType>> GetMerchandiseType();
        Task<IEnumerable<DeliveryMode>> GetDeliveryMode();

    }
}
