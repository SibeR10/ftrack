﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Reservation
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReservationID { get; set; }
        public string ReservationDate { get; set; }
        public ServiceType ServiceType { get; set; }
        public int ServiceTypeID { get; set; }
        public int OriginID { get; set; }
        public int DestinationID { get; set; }
        public string AddressQuote { get; set; }
        public string ReferenceQuote { get; set; }
        public SalesPerson SalesPerson { get; set; }
        public int SalesPersonID { get; set; }
        public Customer Customer { get; set; }
        public int CustomerID { get; set; }
        public int StatusReservationID { get; set; }
    }
}

