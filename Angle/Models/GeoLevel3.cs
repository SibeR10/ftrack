﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class GeoLevel3
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GeoLevel3ID { get; set; }
        public string GeoLevel3Name { get; set; }
        public string GeoLevel3Title { get; set; }
        public GeoLevel2 GeoLevel2 { get; set; }
        public int GeoLevel2ID { get; set; }
    }
}
