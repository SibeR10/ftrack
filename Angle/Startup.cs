using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Angle.Data;
using Microsoft.EntityFrameworkCore;
using Angle.Repositories;
using Angle.Repositories.Implements;

using Angle.Services;
using Angle.Services.Implements;
namespace Angle
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();

            services.AddDbContext<FTContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            //Repositories
            services.AddScoped<IStudentRepository, StudentRepository>();
            services.AddScoped<IStatusRepository, StatusRepository>();
            services.AddScoped<ICurrencyRepository, CurrencyRepository>();
            services.AddScoped<IDivisionRepository, DivisionRepository>();
            services.AddScoped<IDocumentTypeRepository, DocumentTypeRepository>();
            services.AddScoped<ICustomerTypeRepository, CustomerTypeRepository>();
            services.AddScoped<IBrandRepository, BrandRepository>();
            services.AddScoped<IFuelRepository, FuelRepository>();
            services.AddScoped<IDriverTypeRepository, DriverTypeRepository>();
            services.AddScoped<IPaymentTypeRepository, PaymentTypeRepository>();
            services.AddScoped<IPaymentTermsRepository, PaymentTermsRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IGeoLevel1Repository, GeoLevel1Repository>();
            services.AddScoped<IGeoLevel2Repository, GeoLevel2Repository>();
            services.AddScoped<IGeoLevel3Repository, GeoLevel3Repository>();
            services.AddScoped<IAddressTypeRepository, AddressTypeRepository>();
            services.AddScoped<ISalesPersonRepository, SalesPersonRepository>(); 
            services.AddScoped<IDriverRepository, DriverRepository>();
            services.AddScoped<IVehiclesRepository, VehiclesRepository>();
            services.AddScoped<IHistoricDriverRepository, HistoricDriverRepository>();
            services.AddScoped<IHistoricFuelRepository, HistoricFuelRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IContactRepository, ContactRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IQuoteStatusRepository, QuoteStatusRepository>();
            services.AddScoped<IWarehouseRepository, WarehouseRepository>();
            services.AddScoped<IApplyByRepository, ApplyByRepository>();
            services.AddScoped<IMeasureUnitRepository, MeasureUnitRepository>();
            services.AddScoped<IServiceTypeRepository, ServiceTypeRepository>();
            services.AddScoped<ITransportTypeRepository, TransportTypeRepository>();
            services.AddScoped<IMerchandiseTypeRepository, MerchandiseTypeRepository>();
            services.AddScoped<IDeliveryModeRepository, DeliveryModeRepository>();
            services.AddScoped<IServiceClassRepository, ServiceClassRepository>();
            services.AddScoped<IContractRepository, ContractRepository>();
            services.AddScoped<IContractSpecialRepository, ContractSpecialRepository>();
            services.AddScoped<IContractSequenceRepository, ContractSequenceRepository>();
            services.AddScoped<IContractSpecialSequenceRepository, ContractSpecialSequenceRepository>();
            services.AddScoped<IContractMeasuryRepository, ContractMeasuryRepository>();
            services.AddScoped<IQuoteRepository, QuoteRepository>();
            services.AddScoped<IAccountTypeRepository, AccountTypeRepository>();
            services.AddScoped<IDocumentUploadRepository, DocumentUploadRepository>();
            services.AddScoped<IServiceOrderStatusRepository, ServiceOrderStatusRepository>();
            services.AddScoped<IServiceOrderRepository, ServiceOrderRepository>();
            services.AddScoped< IChargesByQuoteRepository, ChargesByQuoteRepository>();
            services.AddScoped< IReportesRepository, ReportesRepository>();
            services.AddScoped< IReservationRepository, ReservationRepository>();
            services.AddScoped< ITravelRepository, TravelRepository>();



            //Services
            services.AddScoped<IStudentService, StudentService>();
            services.AddScoped<IStatusService, StatusService>();
            services.AddScoped<ICurrencyService, CurrencyService>();
            services.AddScoped<IDivisionService, DivisionService>();
            services.AddScoped<IDocumentTypeService, DocumentTypeService>();
            services.AddScoped<ICustomerTypeService, CustomerTypeService>();
            services.AddScoped<IBrandService, BrandService>();
            services.AddScoped<IFuelService, FuelService>();
            services.AddScoped<IDriverTypeService, DriverTypeService>();
            services.AddScoped<IPaymentTypeService, PaymentTypeService>();
            services.AddScoped<IPaymentTermsService, PaymentTermsService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IGeoLevel1Service, GeoLevel1Service>();
            services.AddScoped<IGeoLevel2Service, GeoLevel2Service>();
            services.AddScoped<IGeoLevel3Service, GeoLevel3Service>();
            services.AddScoped<IAddressTypeService, AddressTypeService>();
            services.AddScoped<ISalesPersonService, SalesPersonService>();
            services.AddScoped<IDriverService, DriverService>();
            services.AddScoped<IVehiclesService, VehiclesService>();
            services.AddScoped<IHistoricDriverService, HistoricDriverService>();
            services.AddScoped<IHistoricFuelService, HistoricFuelService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IContactService, ContactService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IQuoteStatusService, QuoteStatusService>();
            services.AddScoped<IWarehouseService, WarehouseService>();
            services.AddScoped<IApplyByService, ApplyByService>();
            services.AddScoped<IMeasureUnitService, MeasureUnitService>();
            services.AddScoped<IServiceTypeService, ServiceTypeService>();
            services.AddScoped<ITransportTypeService, TransportTypeService>();
            services.AddScoped<IMerchandiseTypeService, MerchandiseTypeService>();
            services.AddScoped<IDeliveryModeService, DeliveryModeService>();
            services.AddScoped<IServiceClassService, ServiceClassService>();
            services.AddScoped<IContractService, ContractService>();
            services.AddScoped<IContractSpecialService, ContractSpecialService>();
            services.AddScoped<IContractSequenceService, ContractSequenceService>();
            services.AddScoped<IContractSpecialSequenceService, ContractSpecialSequenceService>();
            services.AddScoped<IContractMeasuryService, ContractMeasuryService>();
            services.AddScoped<IQuoteService, QuoteService>();
            services.AddScoped<IAccountTypeService, AccountTypeService>();
            services.AddScoped<IDocumentUploadService, DocumentUploadService>();
            services.AddScoped<IServiceOrderStatusService, ServiceOrderStatusService>();
            services.AddScoped<IServiceOrderService, ServiceOrderService>();
            services.AddScoped<IChargesByQuoteService, ChargesByQuoteService>();
            services.AddScoped<IReportesService, ReportesService>();
            services.AddScoped<IReservationService, ReservationService>();
            services.AddScoped<ITravelService, TravelService>();

            
            //AutoMapper
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddMvc();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapRazorPages();
              endpoints.MapControllerRoute("default", "{controller=Dashboard}/{action=Dashboard2}/{id?}");
              // endpoints.MapControllerRoute("default", "{controller=Reservation}/{action=Create}/{id?}");
            });

        }
    }
}
