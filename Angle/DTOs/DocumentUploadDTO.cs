﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class DocumentUploadDTO
    {
        public int DocumentUploadID { get; set; }

		[Display(Name = "Fecha inicio")]
		public DateTime DateUpload { get; set; }

		[Display(Name = "Module")]
		public string Module{ get; set; }

		[Display(Name = "Attachment")]
		public string Attachment{ get; set; }

		[Display(Name = "ModuleID")]
		public int ModuleID { get; set; }

	}
}