﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class HistoricTravel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HistoricTravelID { get; set; }
        public int TravelID { get; set; }
        public int ServiceOrderID { get; set; }
    }
}
