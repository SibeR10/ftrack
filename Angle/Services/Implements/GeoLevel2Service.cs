﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class GeoLevel2Service : GenericService<GeoLevel2>, IGeoLevel2Service
    {
        private IGeoLevel2Repository geolevel2Repository;

        public GeoLevel2Service(IGeoLevel2Repository geolevel2Repository) : base(geolevel2Repository)
        {
            this.geolevel2Repository = geolevel2Repository;
        }

        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await geolevel2Repository.GetGeoLevel1();
        }

        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1ByGeoLevel2(int id)
        {
            return await geolevel2Repository.GetGeoLevel1ByGeoLevel2(id);
        }

    }
}
