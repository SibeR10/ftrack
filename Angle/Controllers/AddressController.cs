﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Address = Angle.Models.Address;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class AddressController : Controller
    {
        private readonly IAddressService addressService;
        private readonly IMapper mapper;

        public AddressController(IAddressService addressService,
            IMapper mapper)
        {
            this.addressService = addressService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await addressService.GetAll();
            ViewBag.Status = await addressService.GetStatus();
            var listAddress = data.Select(x => mapper.Map<AddressDTO>(x)).ToList();
            return View(listAddress);
        }



        public async Task<IActionResult> CreateAsync(int? id)
        {

            ViewBag.Status = await addressService.GetStatus();
            ViewBag.Customer = await addressService.GetCustomerById(id.Value);
            ViewBag.AddressType = await addressService.GetAddressType();
            ViewBag.GeoLevel1 = await addressService.GetGeoLevel1();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(AddressDTO addressDTO)
        {
            
            if (ModelState.IsValid)
            {

                var address = mapper.Map<Address>(addressDTO);

                address = await addressService.Insert(address);

                return RedirectToAction("Index", "Customer");
            }

            return View(addressDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var address = await addressService.GetById(id.Value);

            if (address == null)
                return NotFound();

            ViewBag.Status = await addressService.GetStatus();
            ViewBag.Customer = await addressService.GetCustomerById(id.Value);
            ViewBag.AddressType = await addressService.GetAddressType();
            ViewBag.GeoLevel1 = await addressService.GetGeoLevel1();
            var addressDTO = mapper.Map<AddressDTO>(address);
            return View(addressDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(AddressDTO addressDTO)
        {
            if (ModelState.IsValid)
            {
                var address = mapper.Map<Address>(addressDTO);

                address = await addressService.Update(address);

                return RedirectToAction("Index", "Customer");
            }

            return View(addressDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var address = await addressService.GetById(id.Value);

            if (address == null)
                return NotFound();

            var addressDTO = mapper.Map<AddressDTO>(address);

            return View(addressDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await addressService.Delete(id.Value);

            return RedirectToAction("Index", "Customer");
        }
    }
}