﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class FuelService : GenericService<Fuel>, IFuelService
    {
        private IFuelRepository fuelRepository;

        public FuelService(IFuelRepository fuelRepository) : base(fuelRepository)
        {
            this.fuelRepository = fuelRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await fuelRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByFuel(int id)
        {
            return await fuelRepository.GetStatusByFuel(id);
        }

    }
}
