﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Division
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DivisionID { get; set; }
        public string DivisionName { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }

        public static implicit operator string(Division v)
        {
            throw new NotImplementedException();
        }
    }
}
