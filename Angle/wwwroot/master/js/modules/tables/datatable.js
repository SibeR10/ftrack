// DATATABLES
// -----------------------------------

(function() {
    'use strict';

    $(initDatatables);

    function initDatatables() {

        if (!$.fn.DataTable) return;

        // Zero configuration

        $('#datatable4Track').DataTable({
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            responsive: true,
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Buscar:',
                sLengthMenu: '_MENU_ registros por p�gina',
                info: 'mostrando p�gina _PAGE_ de _PAGES_',
                zeroRecords: 'El registro no se encuentra',
                infoEmpty: 'No hay registros disponibles',
                infoFiltered: '(filtered from _MAX_ total records)',
                oPaginate: {
                    sNext: '<em class="fa fa-caret-right"></em>',
                    sPrevious: '<em class="fa fa-caret-left"></em>'
                }
            },
            // Datatable Buttons setup
            dom: 'Bfrtip',
            buttons: [
               // { extend: 'copy', className: 'btn-info' },
               // { extend: 'csv', className: 'btn-info' },
                { extend: 'excel', className: 'btn-info', title: 'XLS-File' },
                //{ extend: 'pdf', className: 'btn-info', title: $('title').text() },
               // { extend: 'print', className: 'btn-info' }
            ]
        }).buttons().container().appendTo('#botones');;

    }

})();