﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ReservationService : GenericService<Reservation>, IReservationService
    {
        private IReservationRepository reservationRepository;

        public ReservationService(IReservationRepository reservationRepository) : base(reservationRepository)
        {
            this.reservationRepository = reservationRepository;
        }

        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await reservationRepository.GetGeoLevel1();
        }
        

        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {
            return await reservationRepository.GetWarehouse();
        }
        
        public async Task<IEnumerable<Country>> GetCountry()
        {
            return await reservationRepository.GetCountry();
        }

        public async Task<IEnumerable<Customer>> GetCustomer()
        {
            return await reservationRepository.GetCustomer();
        }

        public async Task<IEnumerable<SalesPerson>> GetSalesPerson()
        {
            return await reservationRepository.GetSalesPerson();
        }

        public async Task<IEnumerable<Division>> GetDivision()
        {
            return await reservationRepository.GetDivision();
        }

        public async Task<IEnumerable<AddressType>> GetAddressType()
        {
            return await reservationRepository.GetAddressType();
        }

        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {
            return await reservationRepository.GetServiceType();
        }

        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {
            return await reservationRepository.GetMerchandiseType();
        }

        public async Task<IEnumerable<ContractSpecial>> GetContractSpecial()
        {
            return await reservationRepository.GetContractSpecial();
        }

        public async Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence()
        {
            return await reservationRepository.GetContractSpecialSequence();
        }

        public async Task<IEnumerable<ContractSequence>> GetContractSequence()
        {
            return await reservationRepository.GetContractSequence();
        }

        public async Task<IEnumerable<ItemsReservation>> GetItemsReservation()
        {
            return await reservationRepository.GetItemsReservation();
        }

        public async Task<IEnumerable<QuoteStatus>> GetStatus()
        {
            return await reservationRepository.GetStatus();
        }


    }
}
