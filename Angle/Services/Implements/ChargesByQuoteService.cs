﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Services.Implements
{
    public class ChargesByQuoteService : GenericService<ChargesByQuote>, IChargesByQuoteService
    {
        private IChargesByQuoteRepository ChargesByQuoteRepository;

        public ChargesByQuoteService(IChargesByQuoteRepository ChargesByQuoteRepository) : base(ChargesByQuoteRepository)
        {
            this.ChargesByQuoteRepository = ChargesByQuoteRepository;
        }
        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {
            return await ChargesByQuoteRepository.GetMerchandiseType();
        }
        public async Task<IEnumerable<Customer>> GetCustomer()
        {
            return await ChargesByQuoteRepository.GetCustomer();
        }
        public async Task<IEnumerable<Quote>> GetQuote()
        {
            return await ChargesByQuoteRepository.GetQuote();
        }
        public async Task<IEnumerable<ContractSpecial>> GetContractSpecial()
        {
            return await ChargesByQuoteRepository.GetContractSpecial();
        }
        public async Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence()
        {
            return await ChargesByQuoteRepository.GetContractSpecialSequence();
        }
        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {
            return await ChargesByQuoteRepository.GetServiceType();
        }
        public async Task<IEnumerable<ApplyBy>> GetApplyBy()
        {
            return await ChargesByQuoteRepository.GetApplyBy();
        }
        public async Task<IEnumerable<MeasureUnit>> GetMeasureUnit()
        {
            return await ChargesByQuoteRepository.GetMeasureUnit();
        }
        public async Task<IEnumerable<ContractSpecialMeasury>> GetContractSpecialMeasury()
        {
            return await ChargesByQuoteRepository.GetContractSpecialMeasury();
        }
        public async Task<IEnumerable<ContractSequence>> GetContractSequence()
        {
            return await ChargesByQuoteRepository.GetContractSequence();
        }
        public async Task<IEnumerable<ContractMeasury>> GetContractMeasury()
        {
            return await ChargesByQuoteRepository.GetContractMeasury();
        }
    }
}
