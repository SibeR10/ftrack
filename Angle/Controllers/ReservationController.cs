﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Reservation = Angle.Models.Reservation;
using Angle.Services.Implements;
using System.Data;
using Microsoft.Data.SqlClient;
using System;

using Angle.Data;
using Microsoft.EntityFrameworkCore;

namespace Angle.Controllers
{
    public class ReservationController : Controller
    {
        private readonly IReservationService reservationService;
        private readonly IServiceOrderService ServiceOrderService;
        private readonly IMapper mapper;
        private readonly FTContext FTContext;

        public ReservationController(IReservationService reservationService,
            IMapper mapper)
        {
            this.reservationService = reservationService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.Customer = await reservationService.GetCustomer();
            ViewBag.SalesPerson = await reservationService.GetSalesPerson();

            var data = await reservationService.GetAll();

            var listReservation = data.Select(x => mapper.Map<ReservationDTO>(x)).ToList();


            return View(listReservation);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await reservationService.GetStatus();
            ViewBag.ServiceType = await reservationService.GetServiceType();
            ViewBag.Customer = await reservationService.GetCustomer();
            ViewBag.SalesPerson = await reservationService.GetSalesPerson();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ReservationDTO reservationDTO)
        {
            
            if (ModelState.IsValid)
            {

                var reservation = mapper.Map<Reservation>(reservationDTO);

                reservation = await reservationService.Insert(reservation);

                var url = "/Edit/" + reservation.ReservationID;
                return RedirectToAction(url);
            }

            return View(reservationDTO);
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var reservation = await reservationService.GetById(id.Value);

            if (reservation == null)
                return NotFound();

            ViewBag.ServiceType = await reservationService.GetServiceType();
            ViewBag.Customer = await reservationService.GetCustomer();
            ViewBag.SalesPerson = await reservationService.GetSalesPerson();
            ViewBag.GeoLevel1 = await reservationService.GetGeoLevel1();
            ViewBag.Warehouse = await reservationService.GetWarehouse();
            ViewBag.MerchandiseType = await reservationService.GetMerchandiseType();
            ViewBag.ContractSpecial = await reservationService.GetContractSpecial();
            ViewBag.ContractSpecialSequence = await reservationService.GetContractSpecialSequence();
            ViewBag.ContractSequence = await reservationService.GetContractSequence();
            ViewBag.ItemsReservation = await reservationService.GetItemsReservation();
            ViewBag.Status = await reservationService.GetStatus();
            var reservationDTO = mapper.Map<ReservationDTO>(reservation);

            
            return View(reservationDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ReservationDTO reservationDTO)
        {
            var reservation = mapper.Map<Reservation>(reservationDTO);
            if (ModelState.IsValid)
            {
                //var serviceorder = mapper.Map<ServiceOrder>(ServiceOrderDTO);
                if (reservation.StatusReservationID == 1)
                { // cotizacion aprobada
                  //agregar orden de servicio
                    SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                    conexion.Open();
                    int Quoteid = reservation.ReservationID;
                    DateTime now = DateTime.Now;
                    string strDate = now.ToString("yyyy-MM-dd hh:mm:ss");
                    int Remittee = 0;
                    int Sender = 0;
                    int MainPhone = 0;
                    string Email = "-";
                    int ServiceType = reservation.ServiceTypeID;
                    int Origin = reservation.OriginID;
                    int Destination = reservation.DestinationID;
                    string Address = reservation.AddressQuote;
                    string Reference = reservation.ReferenceQuote;
                    int SalesPerson = reservation.SalesPersonID;
                    int Customer = reservation.CustomerID;
                    string DateDeliveryMax = now.ToString("yyyy-MM-dd hh:mm:ss"); 
                    int MaxDays = 0;
                    string cadena = "insert into [dbo].[FT_ServiceOrder]([QuoteID],[ServiceOrderDate],[CollectDate],[InTransitDate],[DeliveryDate],[ServiceOrderStatusID] ,[ServiceTypeID],[OriginID],[DestinationID],[AddressQuote] ,[ReferenceQuote],[SalesPersonID],[CustomerID],[Remittee] ,[Sender],[MainPhone] ,[Email], [IdCreate], [DescriptionCreate],[DateDeliveryMax],[MaxDays])" + "values (" + Quoteid + ",'" + strDate + "','0001-01-01','0001-01-01','0001-01-01',1," + ServiceType + "," + Origin + "," + Destination + ",'" + Address + "','" + Reference + "','" + SalesPerson + "','" + Customer + "'," + Remittee + "," + Sender + "," + MainPhone + ",'" + Email + "'," + Quoteid + ",'ID Reservación', '"+ DateDeliveryMax + "' , "+ MaxDays + ")";
                    SqlCommand comando = new SqlCommand(cadena, conexion);
                    comando.ExecuteNonQuery();
                    conexion.Close();
                }

                reservation = await reservationService.Update(reservation);
            }


            var url = "/Edit/" + reservation.ReservationID;
            return RedirectToAction(url);
        }       
        
        public async Task<IActionResult> CreateItem(int ReservationID, int TipoMercancia, string Description, int Quantity, int Weight, int Amount)
        {
            SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
            conexion.Open();

            string cadena = "insert into [dbo].[FT_ItemsReservation]([ReservationID],[MerchandiseTypeID],[Description],[Quantity],[Weight],[Amount]) values" + " (" + ReservationID + "," + TipoMercancia + " ,'" + Description + "'," + Quantity + "," + Weight + "," + Amount + ")";
            SqlCommand comando = new SqlCommand(cadena, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            var url = "/Edit/" + ReservationID;
            return RedirectToAction(url);
        }

        public async Task<IActionResult> DeleteItem(int ReservationID, int ItemReservationID)
        {
            SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
            conexion.Open();

            string cadena = "delete from[dbo].[FT_ItemsReservation] where ItemsReservationID = "+ ItemReservationID;
            SqlCommand comando = new SqlCommand(cadena, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            var url = "/Edit/" + ReservationID;
            return RedirectToAction(url);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var reservation = await reservationService.GetById(id.Value);

            if (reservation == null)
                return NotFound();

            var reservationDTO = mapper.Map<ReservationDTO>(reservation);

            return View(reservationDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await reservationService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}