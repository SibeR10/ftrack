﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ContractSpecialSequenceDTO
    {
        public int ContractSpecialSequenceID { get; set; }

        [Required(ErrorMessage = "The Tarifa is required")]
        [Display(Name = "Tarifa")]

        public int ContractSpecialID { get; set; }

        [Display(Name = "Peso Mínimo(Lb)")]
        public int WeightMin { get; set; }

        [Display(Name = "Costo por peso Mínimo(Lb)")]
        public int BasePrice { get; set; }

        [Display(Name = "Costo por Lb Adicional")]
        public int ExtraPrice { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }

        [Required(ErrorMessage = "The MerchandiseType is required")]
        [Display(Name = "Tipo de Mercancía")]
        public int MerchandiseTypeID { get; set; }

        [Required(ErrorMessage = "The DeliveryMode is required")]
        [Display(Name = "Modalidad de entrega")]
        public int DeliveryModeID { get; set; }

        [Required(ErrorMessage = "The Origin is required")]
        [Display(Name = "Origen")]
        public int OriginID { get; set; }

        [Required(ErrorMessage = "The Destination is required")]
        [Display(Name = "Destino")]
        public int DestinationID { get; set; }

        [Required(ErrorMessage = "The Customer is required")]
        [Display(Name = "Cliente")]
        public int CustomerID { get; set; }

        [Required(ErrorMessage = "The ServiceType is required")]
        [Display(Name = "Tipo de servicio")]
        public int ServiceTypeID { get; set; }

    }
}

