﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IReservationRepository : IGenericRepository<Reservation>
    {
        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();
        Task<IEnumerable<Warehouse>> GetWarehouse();
        Task<IEnumerable<Country>> GetCountry();
        Task<IEnumerable<Customer>> GetCustomer();
        Task<IEnumerable<SalesPerson>> GetSalesPerson();
        Task<IEnumerable<Division>> GetDivision();
        Task<IEnumerable<AddressType>> GetAddressType();
        Task<IEnumerable<ServiceType>> GetServiceType();
        Task<IEnumerable<MerchandiseType>> GetMerchandiseType();
        Task<IEnumerable<ContractSpecial>> GetContractSpecial();
        Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence();
        Task<IEnumerable<ContractSequence>> GetContractSequence();
        Task<IEnumerable<ItemsReservation>> GetItemsReservation();
        Task<IEnumerable<QuoteStatus>> GetStatus();
    }
}
