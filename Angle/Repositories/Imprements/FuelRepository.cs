﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class FuelRepository : GenericRepository<Fuel>, IFuelRepository
    {
        private readonly FTContext FTContext;

        public FuelRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }

        public async Task<IEnumerable<Status>> GetStatusByFuel(int id)
        {

            var listStatusByFuel = await FTContext.Fuel
                .Include(x => x.Status)
                .Where(x => x.StatusID == id)
                .Select(x => x.Status)
                .ToListAsync();

            return listStatusByFuel;
        }

        
    }
}
