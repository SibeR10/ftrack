﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations.Schema;
namespace Angle.Models
{
    public class AccountType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AccountTypeID { get; set; }
        public string AccountTypeName { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }

    }
}
