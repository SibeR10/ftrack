﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Services.Implements
{
    public class StatusService : GenericService<Status>, IStatusService
    {
        private IStatusRepository StatusRepository;

        public StatusService(IStatusRepository StatusRepository) : base(StatusRepository)
        {
            this.StatusRepository = StatusRepository;
        }

    }
}
