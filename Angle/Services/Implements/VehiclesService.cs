﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class VehiclesService : GenericService<Vehicles>, IVehiclesService
    {
        private IVehiclesRepository vehiclesRepository;

        public VehiclesService(IVehiclesRepository vehiclesRepository) : base(vehiclesRepository)
        {
            this.vehiclesRepository = vehiclesRepository;
        }

        public async Task<IEnumerable<DocumentUpload>> GetDocumentUpload()
        {
            return await vehiclesRepository.GetDocumentUpload();
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await vehiclesRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByVehicles(int id)
        {
            return await vehiclesRepository.GetStatusByVehicles(id);
        }

        public async Task<IEnumerable<Division>> GetDivision()
        {
            return await vehiclesRepository.GetDivision();
        }

        public async Task<IEnumerable<Division>> GetDivisionByVehicles(int id)
        {
            return await vehiclesRepository.GetDivisionByVehicles(id);
        }

        public async Task<IEnumerable<Brand>> GetBrand()
        {
            return await vehiclesRepository.GetBrand();
        }

        public async Task<IEnumerable<Brand>> GetBrandByVehicles(int id)
        {
            return await vehiclesRepository.GetBrandByVehicles(id);
        }

        public async Task<IEnumerable<Driver>> GetDriver()
        {
            return await vehiclesRepository.GetDriver();
        }

        public async Task<IEnumerable<Driver>> GetDriverByVehicles(int id)
        {
            return await vehiclesRepository.GetDriverByVehicles(id);
        }



        public async Task<IEnumerable<Fuel>> GetFuel()
        {
            return await vehiclesRepository.GetFuel();
        }

        
        public async Task<IEnumerable<HistoricDriver>> GetHistoricDriver()
        {
            return await vehiclesRepository.GetHistoricDriver();
        }

        public async Task<IEnumerable<HistoricDriver>> GetHistoricDriverByVehicle(int id)
        {
            return await vehiclesRepository.GetHistoricDriverByVehicle(id);
        }

        public async Task<IEnumerable<HistoricFuel>> GetHistoricFuel()
        {
            return await vehiclesRepository.GetHistoricFuel();
        }

        public async Task<IEnumerable<HistoricFuel>> GetHistoricFuelByVehicle(int id)
        {
            return await vehiclesRepository.GetHistoricFuelByVehicle(id);
        }
        
    }
}
