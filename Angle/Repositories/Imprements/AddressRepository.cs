﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class AddressRepository : GenericRepository<Address>, IAddressRepository
    {
        private readonly FTContext FTContext;

        public AddressRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }

        public async Task<IEnumerable<Status>> GetStatusByAddress(int id)
        {

            var listStatusByAddress = await FTContext.Address
                .Include(x => x.Status)
                .Where(x => x.StatusID == id)
                .Select(x => x.Status)
                .ToListAsync();

            return listStatusByAddress;
        }
        public async Task<IEnumerable<Customer>> GetCustomer()
        {

            var listCustomer = await FTContext.Customer.ToListAsync();

            return listCustomer;
        }


        public async Task<IEnumerable<Customer>> GetCustomerById(int id)
        {

            var listCustomerById = await FTContext.Customer
                .Where(x => x.CustomerID == id)
                .ToListAsync();

            return listCustomerById;
        }

        public async Task<IEnumerable<Country>> GetCountry()
        {

            var listCountry = await FTContext.Country.ToListAsync();

            return listCountry;
        }

        public async Task<IEnumerable<Country>> GetCountryById(int id)
        {

            var listCountryById = await FTContext.Country
                .Where(x => x.CountryID == id)
                .ToListAsync();

            return listCountryById;
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {

            var listGeoLevel1 = await FTContext.GeoLevel1.ToListAsync();

            return listGeoLevel1;
        }

        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1ById(int id)
        {

            var listGeoLevel1ById = await FTContext.GeoLevel1
                .Where(x => x.GeoLevel1ID == id)
                .ToListAsync();

            return listGeoLevel1ById;
        }
        public async Task<IEnumerable<GeoLevel2>> GetGeoLevel2()
        {

            var listGeoLevel2 = await FTContext.GeoLevel2.ToListAsync();

            return listGeoLevel2;
        }

        public async Task<IEnumerable<GeoLevel2>> GetGeoLevel2ById(int id)
        {

            var listGeoLevel2ById = await FTContext.GeoLevel2
                .Where(x => x.GeoLevel2ID == id)
                .ToListAsync();

            return listGeoLevel2ById;
        }
        public async Task<IEnumerable<GeoLevel3>> GetGeoLevel3()
        {

            var listGeoLevel3 = await FTContext.GeoLevel3.ToListAsync();

            return listGeoLevel3;
        }

        public async Task<IEnumerable<GeoLevel3>> GetGeoLevel3ById(int id)
        {

            var listGeoLevel3ById = await FTContext.GeoLevel3
                .Where(x => x.GeoLevel3ID == id)
                .ToListAsync();

            return listGeoLevel3ById;
        }
        public async Task<IEnumerable<AddressType>> GetAddressType()
        {

            var listAddressType = await FTContext.AddressType.ToListAsync();

            return listAddressType;
        }

        public async Task<IEnumerable<AddressType>> GetAddressTypeById(int id)
        {

            var listAddressTypeById = await FTContext.AddressType
                .Where(x => x.AddressTypeID == id)
                .ToListAsync();

            return listAddressTypeById;
        }


    }
}
