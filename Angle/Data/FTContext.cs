﻿using Microsoft.EntityFrameworkCore;
using Angle.Models;
using System;

namespace Angle.Data
{
    public class FTContext : DbContext
    {
        public FTContext(DbContextOptions<FTContext> options) : base(options)
        {
        }

        public FTContext()
        {
        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<Currency> Currency { get; set; }
        public DbSet<Division> Division { get; set; }
        public DbSet<DocumentType> DocumentType { get; set; }
        public DbSet<CustomerType> CustomerType { get; set; }
        public DbSet<Brand> Brand { get; set; }
        public DbSet<Fuel> Fuel { get; set; }
        public DbSet<DriverType> DriverType { get; set; }
        public DbSet<PaymentType> PaymentType { get; set; }
        public DbSet<PaymentTerms> PaymentTerms { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<GeoLevel1> GeoLevel1 { get; set; }
        public DbSet<GeoLevel2> GeoLevel2 { get; set; }

        internal void ExecuteFunction(string v, object p)
        {
            throw new NotImplementedException();
        }

        public DbSet<GeoLevel3> GeoLevel3 { get; set; }
        public DbSet<AddressType> AddressType { get; set; }

        internal object ExecuteStoreQuery<T>(string v, int? divisionID)
        {
            throw new NotImplementedException();
        }

        public DbSet<SalesPerson> SalesPerson { get; set; }
        public DbSet<Driver> Driver { get; set; }
        public DbSet<Vehicles> Vehicles { get; set; }
        public DbSet<HistoricDriver> HistoricDriver { get; set; }
        public DbSet<HistoricFuel> HistoricFuel { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Account> Account { get; set; }
        public DbSet<AccountType> AccountType { get; set; }
        public DbSet<Contact> Contact { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<QuoteStatus> QuoteStatus { get; set; }
        public DbSet<ColorTag> ColorTag { get; set; }
        public DbSet<Warehouse> Warehouse { get; set; }
        public DbSet<ApplyBy> ApplyBy { get; set; }
        public DbSet<MeasureUnit> MeasureUnit { get; set; }
        public DbSet<ServiceType> ServiceType { get; set; }
        public DbSet<TransportType> TransportType { get; set; }
        public DbSet<MerchandiseType> MerchandiseType { get; set; }
        public DbSet<DeliveryMode> DeliveryMode { get; set; }
        public DbSet<ServiceClass> ServiceClass { get; set; }
        public DbSet<Contract> Contract { get; set; }
        public DbSet<ContractSpecial> ContractSpecial { get; set; }
        public DbSet<ContractSequence> ContractSequence { get; set; }
        public DbSet<ContractSpecialSequence> ContractSpecialSequence { get; set; }
        public DbSet<ContractMeasury> ContractMeasury { get; set; }
        public DbSet<ContractSpecialMeasury> ContractSpecialMeasury { get; set; }
        public DbSet<Quote> Quote { get; set; }
        public DbSet<ChargesByQuote> ChargesByQuote { get; set; }
        public virtual DbSet<DocumentUpload> DocumentUpload { get; set; }
        public virtual DbSet<ServiceOrderStatus> ServiceOrderStatus { get; set; }
        public virtual DbSet<ServiceOrder> ServiceOrder { get; set; }
        public virtual DbSet<ItemsQuote> ItemsQuote { get; set; }
        public virtual DbSet<ItemsServiceOrder> ItemsServiceOrder { get; set; }
        public virtual DbSet<Reservation> Reservation { get; set; }
        public virtual DbSet<Travel> Travel { get; set; }
        public virtual DbSet<HistoricTravel> HistoricTravel { get; set; }
        public virtual DbSet<ItemsReservation> ItemsReservation { get; set; }
        public virtual DbSet<StopTravel> StopTravel { get; set; }
        public virtual DbSet<Person> Person { get; set; }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>().ToTable("Course");
            modelBuilder.Entity<Student>().ToTable("Student");
            modelBuilder.Entity<Status>().ToTable("FT_Status");
            modelBuilder.Entity<Currency>().ToTable("FT_Currency");
            modelBuilder.Entity<Division>().ToTable("FT_Division");
            modelBuilder.Entity<DocumentType>().ToTable("FT_DocumentType");
            modelBuilder.Entity<CustomerType>().ToTable("FT_CustomerType");
            modelBuilder.Entity<Brand>().ToTable("FT_Brand");
            modelBuilder.Entity<Fuel>().ToTable("FT_Fuel");
            modelBuilder.Entity<DriverType>().ToTable("FT_DriverType");
            modelBuilder.Entity<PaymentType>().ToTable("FT_PaymentType");
            modelBuilder.Entity<PaymentTerms>().ToTable("FT_PaymentTerms");
            modelBuilder.Entity<Country>().ToTable("FT_Country");
            modelBuilder.Entity<GeoLevel1>().ToTable("FT_GeoLevel1");
            modelBuilder.Entity<GeoLevel2>().ToTable("FT_GeoLevel2");
            modelBuilder.Entity<GeoLevel3>().ToTable("FT_GeoLevel3");
            modelBuilder.Entity<AddressType>().ToTable("FT_AddressType");
            modelBuilder.Entity<SalesPerson>().ToTable("FT_SalesPerson");
            modelBuilder.Entity<Driver>().ToTable("FT_Driver");
            modelBuilder.Entity<Vehicles>().ToTable("FT_Vehicles");
            modelBuilder.Entity<HistoricDriver>().ToTable("FT_HistoricDriver");
            modelBuilder.Entity<HistoricFuel>().ToTable("FT_HistoricFuel");
            modelBuilder.Entity<Customer>().ToTable("FT_Customer");
            modelBuilder.Entity<AccountType>().ToTable("FT_AccountType");
            modelBuilder.Entity<Account>().ToTable("FT_Account");
            modelBuilder.Entity<Contact>().ToTable("FT_Contact");
            modelBuilder.Entity<Address>().ToTable("FT_Address");
            modelBuilder.Entity<QuoteStatus>().ToTable("FT_QuoteStatus");
            modelBuilder.Entity<ColorTag>().ToTable("FT_ColorTag");
            modelBuilder.Entity<Warehouse>().ToTable("FT_Warehouse");
            modelBuilder.Entity<ApplyBy>().ToTable("FT_ApplyBy");
            modelBuilder.Entity<MeasureUnit>().ToTable("FT_MeasureUnit");
            modelBuilder.Entity<ServiceType>().ToTable("FT_ServiceType");
            modelBuilder.Entity<TransportType>().ToTable("FT_TransportType");
            modelBuilder.Entity<MerchandiseType>().ToTable("FT_MerchandiseType");
            modelBuilder.Entity<DeliveryMode>().ToTable("FT_DeliveryMode");
            modelBuilder.Entity<ServiceClass>().ToTable("FT_ServiceClass");
            modelBuilder.Entity<Contract>().ToTable("FT_Contract");
            modelBuilder.Entity<ContractSpecial>().ToTable("FT_ContractSpecial");
            modelBuilder.Entity<ContractSequence>().ToTable("FT_ContractSequence");
            modelBuilder.Entity<ContractSpecialSequence>().ToTable("FT_ContractSpecialSequence");
            modelBuilder.Entity<ContractMeasury>().ToTable("FT_ContractMeasury");
            modelBuilder.Entity<ContractSpecialMeasury>().ToTable("FT_ContractSpecialMeasury");
            modelBuilder.Entity<Quote>().ToTable("FT_Quote");
            modelBuilder.Entity<ChargesByQuote>().ToTable("FT_ChargesByQuote");
            modelBuilder.Entity<DocumentUpload>().ToTable("FT_DocumentUpload");
            modelBuilder.Entity<ServiceOrderStatus>().ToTable("FT_ServiceOrderStatus");
            modelBuilder.Entity<ServiceOrder>().ToTable("FT_ServiceOrder").Property(x => x.CollectDate).IsRequired(false);
            modelBuilder.Entity<ServiceOrder>().ToTable("FT_ServiceOrder").Property(x => x.DeliveryDate).IsRequired(false);
            modelBuilder.Entity<ServiceOrder>().ToTable("FT_ServiceOrder").Property(x => x.InTransitDate).IsRequired(false);
            modelBuilder.Entity<ServiceOrder>().ToTable("FT_ServiceOrder").Property(x => x.ServiceOrderDate).IsRequired(false);
            modelBuilder.Entity<ChargesByQuote>().ToTable("FT_ChargesByQuote");
            modelBuilder.Entity<ItemsQuote>().ToTable("FT_ItemsQuote");
            modelBuilder.Entity<ItemsServiceOrder>().ToTable("FT_ItemsServiceOrder");
            modelBuilder.Entity<Reservation>().ToTable("FT_Reservation");
            modelBuilder.Entity<Travel>().ToTable("FT_Travel");
            modelBuilder.Entity<HistoricTravel>().ToTable("FT_HistoricTravel");
            modelBuilder.Entity<ItemsReservation>().ToTable("FT_ItemsReservation");
            modelBuilder.Entity<StopTravel>().ToTable("FT_StopTravel");
            modelBuilder.Entity<Person>().ToTable("FT_Person");

            
        }
    }
}
