﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class QuoteDTO
    {
        public int QuoteID { get; set; }

        [Required(ErrorMessage = "The QuoteDate is required")]
        [Display(Name = "Fecha de cotización")]
        public string QuoteDate { get; set; }

        [Required(ErrorMessage = "The ExpirationDate is required")]
        [Display(Name = "Fecha de vencimiento")]
        public string ExpirationDate { get; set; }

        [Required(ErrorMessage = "The QuoteStatusID is required")]
        [Display(Name = "Estatus")]
        public int QuoteStatusID { get; set; }

        [Display(Name = "Motivo de Aprobación / Rechazo")]
        public string ReasonStatus { get; set; }

        [Display(Name = "Tipo de servicio")]
        public int ServiceTypeID { get; set; }

        [Display(Name = "Origen")]
        public int OriginID { get; set; }

        [Display(Name = "Destino")]
        public int DestinationID { get; set; }


        [Display(Name = "Dirección")]
        public string AddressQuote { get; set; }


        [Display(Name = "Referencia")]
        public string ReferenceQuote { get; set; }


        [Required(ErrorMessage = "The SalesPersonID is required")]
        [Display(Name = "Vendedor")]
        public int SalesPersonID { get; set; }


        [Required(ErrorMessage = "The CustomerID is required")]
        [Display(Name = "Cliente")]
        public int CustomerID { get; set; }


        [Display(Name = "Remitente")]
        public int Sender { get; set; }


        [Display(Name = "Consignatario")]
        public int Remittee { get; set; }

        [Display(Name = "Telefono")]
        public int MainPhone { get; set; }

        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }


    }
}

	
	
	
	
	
	
	


