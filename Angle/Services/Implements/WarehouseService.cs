﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class WarehouseService : GenericService<Warehouse>, IWarehouseService
    {
        private IWarehouseRepository warehouseRepository;

        public WarehouseService(IWarehouseRepository warehouseRepository) : base(warehouseRepository)
        {
            this.warehouseRepository = warehouseRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await warehouseRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByWarehouse(int id)
        {
            return await warehouseRepository.GetStatusByWarehouse(id);
        }

        public async Task<IEnumerable<Division>> GetDivision()
        {
            return await warehouseRepository.GetDivision();
        }

        public async Task<IEnumerable<Division>> GetDivisionByWarehouse(int id)
        {
            return await warehouseRepository.GetDivisionByWarehouse(id);
        }

    }
}
