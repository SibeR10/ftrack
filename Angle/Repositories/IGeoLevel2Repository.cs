﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IGeoLevel2Repository : IGenericRepository<GeoLevel2>
    {
        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();

        Task<IEnumerable<GeoLevel1>> GetGeoLevel1ByGeoLevel2(int id);
    }
}
