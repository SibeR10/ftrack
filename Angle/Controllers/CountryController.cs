﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Country = Angle.Models.Country;
namespace Angle.Controllers
{
    public class CountryController : Controller
    {
        private readonly ICountryService countryService;
        private readonly IMapper mapper;

        public CountryController(ICountryService countryService,
            IMapper mapper)
        {
            this.countryService = countryService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await countryService.GetAll();

            var listCountry = data.Select(x => mapper.Map<CountryDTO>(x)).ToList();

            return View(listCountry);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CountryDTO countryDTO)
        {
            if (ModelState.IsValid)
            {

                var country = mapper.Map<Country>(countryDTO);

                country = await countryService.Insert(country);


                return RedirectToAction("Index");
            }

            return View(countryDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var country = await countryService.GetById(id.Value);

            if (country == null)
                return NotFound();

            var countryDTO = mapper.Map<CountryDTO>(country);

            return View(countryDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CountryDTO countryDTO)
        {
            if (ModelState.IsValid)
            {
                var country = mapper.Map<Country>(countryDTO);

                country = await countryService.Update(country);
                return RedirectToAction("Index");
            }

            return View(countryDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var country = await countryService.GetById(id.Value);

            if (country == null)
                return NotFound();

            var countryDTO = mapper.Map<CountryDTO>(country);

            return View(countryDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await countryService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}