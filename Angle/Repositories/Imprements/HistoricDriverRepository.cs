﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class HistoricDriverRepository : GenericRepository<HistoricDriver>, IHistoricDriverRepository
    {
        private readonly FTContext FTContext;

        public HistoricDriverRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }

        public async Task<IEnumerable<Driver>> GetDriver()
        {

            var listDriver = await FTContext.Driver.ToListAsync();

            return listDriver;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }

        public async Task<IEnumerable<Vehicles>> GetVehiclesById(int id)
        {

            var listVehiclesById = await FTContext.Vehicles
                .Where(x => x.VehiclesID == id)
                .ToListAsync();

            return listVehiclesById;
        }

        public Task<IEnumerable<Vehicles>> GetVehiclesById()
        {
            throw new NotImplementedException();
        }
    }
}
