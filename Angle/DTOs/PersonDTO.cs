﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class PersonDTO
    {
        public int PersonID { get; set; }

        [Required(ErrorMessage = "The PersonName is required")]
        [Display(Name = "Nombre del almacen")]
        public string PersonName { get; set; }

        [Required(ErrorMessage = "The PersonDocument is required")]
        [Display(Name = "PersonDocument")]
        public int PersonDocument { get; set; }

        [Required(ErrorMessage = "The PersonTelepone is required")]
        [Display(Name = "PersonTelepone")]
        public int PersonTelepone { get; set; }

        [Required(ErrorMessage = "The PersonEmail is required")]
        [Display(Name = "PersonEmail")]
        public string PersonEmail { get; set; }
    }
}