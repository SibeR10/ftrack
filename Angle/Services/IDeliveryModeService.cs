﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IDeliveryModeService : IGenericService<DeliveryMode>
    {
        Task<IEnumerable<Status>> GetStatus();

        Task<IEnumerable<Status>> GetStatusByDeliveryMode(int id);
    }
}
