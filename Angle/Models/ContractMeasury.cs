﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ContractMeasury
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContractMeasuryID { get; set; }
        public ContractSequence ContractSequence { get; set; }
        public int ContractSequenceID { get; set; }
        public MeasureUnit MeasureUnit { get; set; }
        public int MeasureUnitID { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public double Tariff { get; set; }
    }
}
	
	
	
