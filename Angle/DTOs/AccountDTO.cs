﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class AccountDTO
    {
        public int AccountID { get; set; }

		[Display(Name = "Limite de credito")]
		public int CreditLimit { get; set; }
		
		[Display(Name = "Fecha inicio")]
		public DateTime StartDate { get; set; }


		[Display(Name = "Estatus")]
		public int StatusID { get; set; }

		[Display(Name = "Terminos de pago")]
		public int PaymentTermsID { get; set; }

		[Display(Name = "Tiipos de pago")]
		public int PaymentTypeID { get; set; }
	}
}