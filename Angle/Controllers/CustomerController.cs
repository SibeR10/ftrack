﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;

using Customer = Angle.Models.Customer;
using System.Data;
using Microsoft.Data.SqlClient;
namespace Angle.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService customerService;
        private readonly IMapper mapper;

        public CustomerController(ICustomerService customerService,
            IMapper mapper)
        {
            this.customerService = customerService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await customerService.GetAll();
            ViewBag.Status = await customerService.GetStatus();
            ViewBag.DocumentType = await customerService.GetDocumentType();
            ViewBag.Division = await customerService.GetDivision();
            var listCustomer = data.Select(x => mapper.Map<CustomerDTO>(x)).ToList();
            return View(listCustomer);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await customerService.GetStatus();
            ViewBag.DocumentType = await customerService.GetDocumentType();
            ViewBag.Division = await customerService.GetDivision();
            ViewBag.CustomerType = await customerService.GetCustomerType();
            ViewBag.Account = await customerService.GetAccount();
            ViewBag.AccountType = await customerService.GetAccountType();
            ViewBag.SalesPerson = await customerService.GetSalesPerson();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CustomerDTO customerDTO)
        {
            
            if (ModelState.IsValid)
            {

                var customer = mapper.Map<Customer>(customerDTO);

                customer = await customerService.Insert(customer);

                return RedirectToAction("Index");
            }

            return View(customerDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var customer = await customerService.GetById(id.Value);

            if (customer == null)
                return NotFound();

            ViewBag.Status = await customerService.GetStatus();
            ViewBag.DocumentType = await customerService.GetDocumentType();
            ViewBag.Division = await customerService.GetDivision();
            ViewBag.CustomerType = await customerService.GetCustomerType();
            ViewBag.Account = await customerService.GetAccount();
            ViewBag.AccountType = await customerService.GetAccountType();
            ViewBag.SalesPerson = await customerService.GetSalesPerson();
            ViewBag.HistoricContact = await customerService.GetContact();
            ViewBag.HistoricAddress = await customerService.GetAddress();
            ViewBag.DocumentUpload = await customerService.GetDocumentUpload();
            ViewBag.GeoLevel1 = await customerService.GetGeoLevel1();
            var customerDTO = mapper.Map<CustomerDTO>(customer);
            return View(customerDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CustomerDTO customerDTO)
        {
            if (ModelState.IsValid)
            {
                var customer = mapper.Map<Customer>(customerDTO);

                customer = await customerService.Update(customer);

                return RedirectToAction("Index");
            }

            return View(customerDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var customer = await customerService.GetById(id.Value);

            if (customer == null)
                return NotFound();

            var customerDTO = mapper.Map<CustomerDTO>(customer);

            return View(customerDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await customerService.Delete(id.Value);

            return RedirectToAction("Index");
        }


        public async Task<IActionResult> Import(IFormFile file)
        {
            var list = new List<Customer>();
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowcount = worksheet.Dimension.Rows;
                    for (int row = 2; row <= rowcount; row++)
                    {
                        list.Add(new Customer
                        {
                            //  CustomerID	Document	CompanyName	FirstName	LastName	MainPhone	SecondPhone	Email	CreateDateTransaction	CreateUserTransaction	UpdateDateTransaction	UpdateUserTransaction	DivisionID	CustomerTypeID	StatusID	DocumentTypeID	AccountID	AccountTypeID	SalesPersonID

                            Document = (int)(double)worksheet.Cells[row, 1].Value,
                            CompanyName = worksheet.Cells[row, 2].Value.ToString().Trim(),
                            FirstName = worksheet.Cells[row, 3].Value.ToString().Trim(),
                            LastName = worksheet.Cells[row, 4].Value.ToString().Trim(),
                            MainPhone = (int)(double)worksheet.Cells[row, 5].Value,
                            SecondPhone = (int)(double)worksheet.Cells[row, 6].Value,
                            Email = worksheet.Cells[row, 7].Value.ToString().Trim(),
                            DivisionID = (int)(double)worksheet.Cells[row, 8].Value,
                            CustomerTypeID = (int)(double)worksheet.Cells[row, 9].Value,
                            StatusID = (int)(double)worksheet.Cells[row, 10].Value,
                            DocumentTypeID = (int)(double)worksheet.Cells[row, 11].Value,
                            AccountID = (int)(double)worksheet.Cells[row, 12].Value,
                            AccountTypeID = (int)(double)worksheet.Cells[row, 13].Value,
                            SalesPersonID = (int)(double)worksheet.Cells[row, 14].Value,
                        });
                    }
                }
            }

            list.ForEach(p =>
            {

                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();
                int Document = p.Document;
                string CompanyName = p.CompanyName;
                string FirstName = p.FirstName;
                string LastName = p.LastName;
                int MainPhone = p.MainPhone;
                int SecondPhone = p.SecondPhone;
                string Email = p.Email;
                int DivisionID = p.DivisionID;
                int CustomerTypeID = p.CustomerTypeID;
                int StatusID = p.StatusID;
                int DocumentTypeID = p.DocumentTypeID;
                int AccountID = p.AccountID;
                int AccountTypeID = p.AccountTypeID;
                int SalesPersonID = p.SalesPersonID;
                string cadena = "insert into [dbo].[FT_Customer]([Document],[CompanyName],[FirstName],[LastName],[MainPhone],[SecondPhone],[Email],[CreateDateTransaction],[CreateUserTransaction],[UpdateDateTransaction],[UpdateUserTransaction],[DivisionID],[CustomerTypeID],[StatusID],[DocumentTypeID],[AccountID],[AccountTypeID],[SalesPersonID]) " + "values (" + Document + ", '" + CompanyName + "', '" + FirstName + "', '" + LastName + "', " + MainPhone + ", " + SecondPhone + ", '" + Email + "','2021-05-03','Traer del login','2021-05-03','Traer del login'," + DivisionID + "," + CustomerTypeID + "," + StatusID + "," + DocumentTypeID + "," + AccountID + "," + AccountTypeID + "," + SalesPersonID + ")";
                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();
                conexion.Close();

            });


            return RedirectToAction("Index");
        }


        //ImportContactos
        public async Task<IActionResult> ImportContactos(IFormFile file, int CustomerID)
        {
            var list = new List<Contact>();
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowcount = worksheet.Dimension.Rows;
                    for (int row = 2; row <= rowcount; row++)
                    {
                        list.Add(new Contact
                        {
                            ContactName = worksheet.Cells[row, 1].Value.ToString().Trim(),
                            ContactLastName = worksheet.Cells[row, 2].Value.ToString().Trim(),
                            ContactPosition = worksheet.Cells[row, 3].Value.ToString().Trim(),
                            ContactMainPhone = (int)(double)worksheet.Cells[row, 4].Value,
                            ContactCellPhone = (int)(double)worksheet.Cells[row, 5].Value,
                            ContactMail = worksheet.Cells[row, 6].Value.ToString().Trim(),
                            StatusID = (int)(double)worksheet.Cells[row, 7].Value,
                        });
                    }
                }
            }

            list.ForEach(p =>
            {

                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();
                string ContactName = p.ContactName;
                string ContactLastName = p.ContactLastName;
                string ContactPosition = p.ContactPosition;
                int ContactMainPhone = p.ContactMainPhone;
                int ContactCellPhone = p.ContactCellPhone;
                string ContactMail = p.ContactMail;
                int StatusID = p.StatusID;
                string cadena = "INSERT INTO [dbo].[FT_Contact] ([ContactName],[ContactLastName],[ContactPosition],[ContactMainPhone],[ContactCellPhone],[ContactMail],[CustomerID],[StatusID]) VALUES ('" + ContactName + "','"+ ContactLastName +"', '" + ContactPosition + "', " + ContactMainPhone + ", " + ContactCellPhone + ", '" +ContactMail+"',"+CustomerID+","+StatusID+")";
               
                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();
                conexion.Close();

            });


            return RedirectToAction("Index");
        }


        //	AddressName	PostalCode	AddressTypeID	GeoLevel1ID	StatusID	CustomerID
        public async Task<IActionResult> ImportDirecciones(IFormFile file, int CustomerID)
        {
            var list = new List<Address>();
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowcount = worksheet.Dimension.Rows;
                    for (int row = 2; row <= rowcount; row++)
                    {
                        list.Add(new Address
                        {
                            AddressName = worksheet.Cells[row, 1].Value.ToString().Trim(),
                            PostalCode = (int)(double)worksheet.Cells[row, 2].Value,
                            AddressTypeID = (int)(double)worksheet.Cells[row, 3].Value,
                            GeoLevel1ID = (int)(double)worksheet.Cells[row, 4].Value,
                            StatusID = (int)(double)worksheet.Cells[row, 5].Value,
                        });
                    }
                }
            }

            list.ForEach(p =>
            {

                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();
                string AddressName = p.AddressName;
                int PostalCode = p.PostalCode;
                int AddressTypeID = p.AddressTypeID;
                int GeoLevel1ID = p.GeoLevel1ID;
                int StatusID = p.StatusID;
                string cadena = "INSERT INTO [dbo].[FT_Address]([AddressName],[PostalCode],[AddressTypeID],[GeoLevel1ID],[StatusID],[CustomerID])VALUES('"+AddressName+"',"+PostalCode+","+AddressTypeID+","+GeoLevel1ID+","+StatusID+","+CustomerID+")";

                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();
                conexion.Close();

            });


            return RedirectToAction("Index");
        }
    }
}