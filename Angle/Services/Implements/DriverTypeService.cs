﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class DriverTypeService : GenericService<DriverType>, IDriverTypeService
    {
        private IDriverTypeRepository drivertypeRepository;

        public DriverTypeService(IDriverTypeRepository drivertypeRepository) : base(drivertypeRepository)
        {
            this.drivertypeRepository = drivertypeRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await drivertypeRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByDriverType(int id)
        {
            return await drivertypeRepository.GetStatusByDriverType(id);
        }

    }
}
