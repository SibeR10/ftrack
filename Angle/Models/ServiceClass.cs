﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ServiceClass
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServiceClassID { get; set; }
        public string ServiceClassName { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
