﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class SalesPersonService : GenericService<SalesPerson>, ISalesPersonService
    {
        private ISalesPersonRepository salespersonRepository;

        public SalesPersonService(ISalesPersonRepository salespersonRepository) : base(salespersonRepository)
        {
            this.salespersonRepository = salespersonRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await salespersonRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusBySalesPerson(int id)
        {
            return await salespersonRepository.GetStatusBySalesPerson(id);
        }

    }
}
