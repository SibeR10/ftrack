﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class ContractMeasuryRepository : GenericRepository<ContractMeasury>, IContractMeasuryRepository
    {
        private readonly FTContext FTContext;

        public ContractMeasuryRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<ContractMeasury>> GetContractMeasury()
        {

            var listContractMeasury = await FTContext.ContractMeasury.ToListAsync();

            return listContractMeasury;
        }




    }
}
