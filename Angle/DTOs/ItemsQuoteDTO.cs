﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ItemsQuoteDTO
    {
        public int ItemsQuoteID { get; set; }


        [Required(ErrorMessage = "The Cotización is required")]
        [Display(Name = "Cotización")]
        public int QuoteID { get; set; }

        [Display(Name = "Tipo de servicio")]
        public int MerchandiseTypeID { get; set; }


        [Display(Name = "Descripcion")]
        public string Description { get; set; }


        [Required(ErrorMessage = "The Cantidad is required")]
        [Display(Name = "Cantidad")]
        public int Quantity { get; set; }


        [Required(ErrorMessage = "The Peso is required")]
        [Display(Name = "Peso")]
        public int Weight { get; set; }
        

        [Required(ErrorMessage = "The Monto is required")]
        [Display(Name = "Monto")]
        public int Amount { get; set; }



    }
}

	
	
	
	
	
	
	


