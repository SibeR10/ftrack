﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ItemsServiceOrder
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemsServiceOrderID { get; set; }

              

        public ServiceOrder ServiceOrder { get; set; }
        public int ServiceOrderID { get; set; }
        public MerchandiseType MerchandiseType { get; set; }
        public int MerchandiseTypeID { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public int Weight { get; set; }
        public int Amount { get; set; }
    }
}

