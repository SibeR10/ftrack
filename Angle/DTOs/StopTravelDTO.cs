﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class StopTravelDTO
    {
        public int StopTravelID { get; set; }

        [Required(ErrorMessage = "The Parada Name is required")]
        [Display(Name = "Parada")]
        public int StopID { get; set; }


    }
}
