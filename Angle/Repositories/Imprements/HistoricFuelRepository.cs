﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class HistoricFuelRepository : GenericRepository<HistoricFuel>, IHistoricFuelRepository
    {
        private readonly FTContext FTContext;

        public HistoricFuelRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }

        public async Task<IEnumerable<Driver>> GetDriver()
        {

            var listDriver = await FTContext.Driver.ToListAsync();

            return listDriver;
        }

        public async Task<IEnumerable<Fuel>> GetFuel()
        {

            var listFuel = await FTContext.Fuel.ToListAsync();

            return listFuel;
        }

        public async Task<IEnumerable<Vehicles>> GetVehiclesById(int id)
        {

            var listVehiclesById = await FTContext.Vehicles
                .Where(x => x.VehiclesID == id)
                .ToListAsync();

            return listVehiclesById;
        }

    }
}
