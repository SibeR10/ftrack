﻿using AutoMapper;
using Angle.Models;
using System.Linq;
using System;

namespace Angle.DTOs
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            CreateMap<StudentDTO, Student>();
            CreateMap<Student, StudentDTO>();

            CreateMap<CourseDTO, Course>();
            CreateMap<Course, CourseDTO>();

            CreateMap<StatusDTO, Status>();
            CreateMap<Status, StatusDTO>();

            CreateMap<CurrencyDTO, Currency>();
            CreateMap<Currency, CurrencyDTO>();

            CreateMap<DivisionDTO, Division>();
            CreateMap<Division, DivisionDTO>();

            CreateMap<DocumentTypeDTO, DocumentType>();
            CreateMap<DocumentType, DocumentTypeDTO>();

            CreateMap<CustomerTypeDTO, CustomerType>();
            CreateMap<CustomerType, CustomerTypeDTO>();

            CreateMap<BrandDTO, Brand>();
            CreateMap<Brand, BrandDTO>();

            CreateMap<FuelDTO, Fuel>();
            CreateMap<Fuel, FuelDTO>();

            CreateMap<DriverTypeDTO, DriverType>();
            CreateMap<DriverType, DriverTypeDTO>();

            CreateMap<PaymentTypeDTO, PaymentType>();
            CreateMap<PaymentType, PaymentTypeDTO>();

            CreateMap<PaymentTermsDTO, PaymentTerms>();
            CreateMap<PaymentTerms, PaymentTermsDTO>();

            CreateMap<CountryDTO, Country>();
            CreateMap<Country, CountryDTO>();

            CreateMap<GeoLevel1DTO, GeoLevel1>();
            CreateMap<GeoLevel1, GeoLevel1DTO>();

            CreateMap<GeoLevel2DTO, GeoLevel2>();
            CreateMap<GeoLevel2, GeoLevel2DTO>();

            CreateMap<GeoLevel3DTO, GeoLevel3>();
            CreateMap<GeoLevel3, GeoLevel3DTO>();

            CreateMap<AddressTypeDTO, AddressType>();
            CreateMap<AddressType, AddressTypeDTO>();

            CreateMap<SalesPersonDTO, SalesPerson>();
            CreateMap<SalesPerson, SalesPersonDTO>();

            CreateMap<DriverDTO, Driver>();
            CreateMap<Driver, DriverDTO>();

            CreateMap<VehiclesDTO, Vehicles>();
            CreateMap<Vehicles, VehiclesDTO>();

            CreateMap<HistoricDriverDTO, HistoricDriver>();
            CreateMap<HistoricDriver, HistoricDriverDTO>();

            CreateMap<HistoricFuelDTO, HistoricFuel>();
            CreateMap<HistoricFuel, HistoricFuelDTO>();

            CreateMap<CustomerDTO, Customer>();
            CreateMap<Customer, CustomerDTO>();

            CreateMap<ContactDTO, Contact>();
            CreateMap<Contact, ContactDTO>();

            CreateMap<AddressDTO, Address>();
            CreateMap<Address, AddressDTO>();

            CreateMap<QuoteStatusDTO, QuoteStatus>();
            CreateMap<QuoteStatus, QuoteStatusDTO>();

            CreateMap<WarehouseDTO, Warehouse>();
            CreateMap<Warehouse, WarehouseDTO>();

            CreateMap<ApplyByDTO, ApplyBy>();
            CreateMap<ApplyBy, ApplyByDTO>();

            CreateMap<MeasureUnitDTO, MeasureUnit>();
            CreateMap<MeasureUnit, MeasureUnitDTO>();

            CreateMap<ServiceTypeDTO, ServiceType>();
            CreateMap<ServiceType, ServiceTypeDTO>();

            CreateMap<TransportTypeDTO, TransportType>();
            CreateMap<TransportType, TransportTypeDTO>();

            CreateMap<MerchandiseTypeDTO, MerchandiseType>();
            CreateMap<MerchandiseType, MerchandiseTypeDTO>();

            CreateMap<DeliveryModeDTO, DeliveryMode>();
            CreateMap<DeliveryMode, DeliveryModeDTO>();

            CreateMap<ServiceClassDTO, ServiceClass>();
            CreateMap<ServiceClass, ServiceClassDTO>();

            CreateMap<ContractDTO, Contract>();
            CreateMap<Contract, ContractDTO>();

            CreateMap<ContractSpecialDTO, ContractSpecial>();
            CreateMap<ContractSpecial, ContractSpecialDTO>();

            CreateMap<ContractSequenceDTO, ContractSequence>();
            CreateMap<ContractSequence, ContractSequenceDTO>();

            CreateMap<ContractSpecialSequenceDTO, ContractSpecialSequence>();
            CreateMap<ContractSpecialSequence, ContractSpecialSequenceDTO>();

            CreateMap<ContractMeasuryDTO, ContractMeasury>();
            CreateMap<ContractMeasury, ContractMeasuryDTO>();

            CreateMap<QuoteDTO, Quote>();
            CreateMap<Quote, QuoteDTO>();

            CreateMap<ChargesByQuoteDTO, ChargesByQuote>();
            CreateMap<ChargesByQuote, ChargesByQuoteDTO>();

            CreateMap<AccountTypeDTO, AccountType>();
            CreateMap<AccountType, AccountTypeDTO>();

            CreateMap<DocumentUploadDTO, DocumentUpload>();
            CreateMap<DocumentUpload, DocumentUploadDTO>();

            CreateMap<ServiceOrderStatusDTO, ServiceOrderStatus>();
            CreateMap<ServiceOrderStatus, ServiceOrderStatusDTO>();

            CreateMap<ServiceOrderDTO, ServiceOrder>();
            CreateMap<ServiceOrder, ServiceOrderDTO>();

            CreateMap<ReservationDTO, Reservation>();
            CreateMap<Reservation, ReservationDTO>();

            CreateMap<TravelDTO, Travel>();
            CreateMap<Travel, TravelDTO>();

            CreateMap<HistoricTravelDTO, HistoricTravel>();
            CreateMap<HistoricTravel, HistoricTravelDTO>();

            CreateMap<StopTravelDTO, StopTravel>();
            CreateMap<StopTravel, StopTravelDTO>();


            
        }

        internal static T Map<T>(IOrderedQueryable<Status> statuses)
        {
            throw new NotImplementedException();
        }
    }
}
