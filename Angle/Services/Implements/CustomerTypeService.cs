﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class CustomerTypeService : GenericService<CustomerType>, ICustomerTypeService
    {
        private ICustomerTypeRepository customertypeRepository;

        public CustomerTypeService(ICustomerTypeRepository customertypeRepository) : base(customertypeRepository)
        {
            this.customertypeRepository = customertypeRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await customertypeRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByCustomerType(int id)
        {
            return await customertypeRepository.GetStatusByCustomerType(id);
        }

    }
}
