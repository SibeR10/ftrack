﻿using Angle.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly FTContext FTContext;

        public GenericRepository(FTContext FTContext)
        {
            this.FTContext = FTContext;
        }

        public int Count()
        {
            return FTContext.Set<TEntity>().ToList().Count();
        }

        public async Task Delete(int id)
        {
            var entity = await GetById(id);

            if (entity == null)
                throw new Exception("The entity is null");

            FTContext.Set<TEntity>().Remove(entity);
            await FTContext.SaveChangesAsync();
        }
       
        public async Task<List<TEntity>> GetAll()
        {
            return await FTContext.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> GetById(int id)
        {
            return await FTContext.Set<TEntity>().FindAsync(id);
        }

        public async Task<TEntity> Insert(TEntity entity)
        {
            await FTContext.Set<TEntity>().AddAsync(entity);
            await FTContext.SaveChangesAsync();
            return entity;
        }

        public async Task<TEntity> Update(TEntity entity)
        {
            FTContext.Set<TEntity>().Update(entity);
            await FTContext.SaveChangesAsync();
            return entity;
        }
    }
}
