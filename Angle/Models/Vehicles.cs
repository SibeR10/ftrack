﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Vehicles
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VehiclesID { get; set; }
		public string LicensePlateCar { get; set; }
		public Status Status { get; set; }
		public int StatusID { get; set; }
		public int Capacity { get; set; }
		
		public string Enrollment { get; set; }
		public string ColourCar { get; set; }
		public string Equipment { get; set; }
		public string ModelCar { get; set; }
		public int YearCar { get; set; }
		public string ChassisCar { get; set; }
		public string InsuranceCar { get; set; }
		public DateTime ExpirationInsuranceCar { get; set; }
		public DateTime CreateDateTransaction { get; set; }
		public string CreateUserTransaction { get; set; }
		public DateTime UpdateDateTransaction { get; set; }
		public string UpdateUserTransaction { get; set; }
		public Division Division { get; set; }
		public int DivisionID { get; set; }
		public Brand Brand { get; set; }
		public int BrandID { get; set; }
		public Driver Driver { get; set; }
		public int DriverID { get; set; }
	}
}
