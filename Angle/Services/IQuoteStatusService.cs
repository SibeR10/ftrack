﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Services
{
    public interface IQuoteStatusService : IGenericService<QuoteStatus>
    {
        Task Insert(List<QuoteStatus> list);
    }
}
