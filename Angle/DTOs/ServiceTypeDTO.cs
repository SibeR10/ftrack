﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ServiceTypeDTO
    {
        public int ServiceTypeID { get; set; }

        [Required(ErrorMessage = "The ServiceTypeName is required")]
        [Display(Name = "Tipo de servicio")]
        public string ServiceTypeName { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

