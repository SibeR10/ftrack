﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Quote
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuoteID { get; set; }
        public string QuoteDate { get; set; }
        public string ExpirationDate { get; set; }
        public QuoteStatus QuoteStatus { get; set; }
        public int QuoteStatusID { get; set; }
        public string ReasonStatus { get; set; }
        public ServiceType ServiceType { get; set; }
        public int ServiceTypeID { get; set; }
        public int OriginID { get; set; }
        public int DestinationID { get; set; }
        public string AddressQuote { get; set; }
        public string ReferenceQuote { get; set; }
        public SalesPerson SalesPerson { get; set; }
        public int SalesPersonID { get; set; }
        public Customer Customer { get; set; }
        public int CustomerID { get; set; }
        public int MainPhone { get; set; }
        public int Remittee { get; set; }
        public int Sender { get; set; }
        public string Email { get; set; }
    }
}

