﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class ChargesByQuoteRepository : GenericRepository<ChargesByQuote>, IChargesByQuoteRepository
    {
        private readonly FTContext FTContext;

        public ChargesByQuoteRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }

        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {

            var listMerchandiseType = await FTContext.MerchandiseType.ToListAsync();

            return listMerchandiseType;
        }
        public async Task<IEnumerable<Customer>> GetCustomer()
        {

            var listCustomer = await FTContext.Customer.ToListAsync();

            return listCustomer;
        }
        public async Task<IEnumerable<Quote>> GetQuote()
        {

            var listQuote = await FTContext.Quote.ToListAsync();

            return listQuote;
        }
        public async Task<IEnumerable<ContractSpecial>> GetContractSpecial()
        {

            var listContractSpecial = await FTContext.ContractSpecial.ToListAsync();

            return listContractSpecial;
        }
        public async Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence()
        {

            var listContractSpecialSequence = await FTContext.ContractSpecialSequence.ToListAsync();

            return listContractSpecialSequence;
        }
        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {

            var listServiceType = await FTContext.ServiceType.ToListAsync();

            return listServiceType;
        }
        public async Task<IEnumerable<ApplyBy>> GetApplyBy()
        {

            var listApplyBy = await FTContext.ApplyBy.ToListAsync();

            return listApplyBy;
        }
        public async Task<IEnumerable<MeasureUnit>> GetMeasureUnit()
        {

            var listMeasureUnit = await FTContext.MeasureUnit.ToListAsync();

            return listMeasureUnit;
        }
        public async Task<IEnumerable<ContractSpecialMeasury>> GetContractSpecialMeasury()
        {

            var listContractSpecialMeasury = await FTContext.ContractSpecialMeasury.ToListAsync();

            return listContractSpecialMeasury;
        }
        public async Task<IEnumerable<ContractSequence>> GetContractSequence()
        {

            var listContractSequence = await FTContext.ContractSequence.ToListAsync();

            return listContractSequence;
        }
        public async Task<IEnumerable<ContractMeasury>> GetContractMeasury()
        {

            var listContractMeasury = await FTContext.ContractMeasury.ToListAsync();

            return listContractMeasury;
        }
    }
}
