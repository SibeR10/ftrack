﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ContactDTO
    {
        public int ContactID { get; set; }

        [Display(Name = "Nombres")]
        public string ContactName { get; set; }

        [Display(Name = "Apellidos")]
        public string ContactLastName { get; set; }

        [Display(Name = "Posición")]
        public string ContactPosition { get; set; }

        [Display(Name = "Teléfono principal")]
        public int ContactMainPhone { get; set; }

        [Display(Name = "Teléfono celular")]
        public int ContactCellPhone { get; set; }

        [Display(Name = "Correo electrónico")]
        public string ContactMail { get; set; }

        [Display(Name = "Cliente")]
        public int CustomerID { get; set; }

        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

