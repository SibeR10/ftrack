﻿using AutoMapper;
using Angle.DTOs;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using QuoteStatus = Angle.Models.QuoteStatus;
using System.Data;
using Microsoft.Data.SqlClient;

namespace Angle.Controllers
{
    public class QuoteStatusController : Controller
    {
        private readonly IQuoteStatusService quotestatusService;
        private readonly IMapper mapper;

        public QuoteStatusController(IQuoteStatusService quotestatusService,
            IMapper mapper)
        {
            this.quotestatusService = quotestatusService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await quotestatusService.GetAll();

            var listQuoteStatus = data.Select(x => mapper.Map<QuoteStatusDTO>(x)).ToList();

            return View(listQuoteStatus);
        }

        public async Task<IActionResult> CreateAsync()
        {

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(QuoteStatusDTO quotestatusDTO)
        {
            if (ModelState.IsValid)
            {

                var quotestatus = mapper.Map<QuoteStatus>(quotestatusDTO);

                quotestatus = await quotestatusService.Insert(quotestatus);


                return RedirectToAction("Index");
            }

            return View(quotestatusDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var quotestatus = await quotestatusService.GetById(id.Value);

            if (quotestatus == null)
                return NotFound();

            var quotestatusDTO = mapper.Map<QuoteStatusDTO>(quotestatus);
            return View(quotestatusDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(QuoteStatusDTO quotestatusDTO)
        {
            if (ModelState.IsValid)
            {
                var quotestatus = mapper.Map<QuoteStatus>(quotestatusDTO);

                quotestatus = await quotestatusService.Update(quotestatus);
                return RedirectToAction("Index");
            }

            return View(quotestatusDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var quotestatus = await quotestatusService.GetById(id.Value);

            if (quotestatus == null)
                return NotFound();

            var quotestatusDTO = mapper.Map<QuoteStatusDTO>(quotestatus);

            return View(quotestatusDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await quotestatusService.Delete(id.Value);

            return RedirectToAction("Index");
        }

    }
}