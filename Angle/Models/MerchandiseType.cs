﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class MerchandiseType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MerchandiseTypeID { get; set; }
        public string MerchandiseTypeName { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
