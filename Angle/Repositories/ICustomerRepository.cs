﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        Task<IEnumerable<Status>> GetStatus();
        Task<IEnumerable<DocumentUpload>> GetDocumentUpload();
        Task<IEnumerable<Status>> GetStatusByCustomer(int id);
        Task<IEnumerable<DocumentType>> GetDocumentType();
        Task<IEnumerable<DocumentType>> GetDocumentTypeByCustomer(int id);
        Task<IEnumerable<Division>> GetDivision();
        Task<IEnumerable<Division>> GetDivisionByCustomer(int id);
        Task<IEnumerable<CustomerType>> GetCustomerType();
        Task<IEnumerable<CustomerType>> GetCustomerTypeByCustomer(int id);
        Task<IEnumerable<Account>> GetAccount();
        Task<IEnumerable<Account>> GetAccountByCustomer(int id);
        Task<IEnumerable<AccountType>> GetAccountType();
        Task<IEnumerable<AccountType>> GetAccountTypeByCustomer(int id);
        Task<IEnumerable<SalesPerson>> GetSalesPerson();
        Task<IEnumerable<SalesPerson>> GetSalesPersonByCustomer(int id);
        Task<IEnumerable<Contact>> GetContact();
        Task<IEnumerable<Contact>> GetContactByCustomer(int id);
        Task<IEnumerable<Address>> GetAddress();
        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();
        Task<IEnumerable<Address>> GetAddressByCustomer(int id);
        
        

    }
}
