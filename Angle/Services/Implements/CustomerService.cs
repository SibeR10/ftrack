﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class CustomerService : GenericService<Customer>, ICustomerService
    {
        private ICustomerRepository customerRepository;

        public CustomerService(ICustomerRepository customerRepository) : base(customerRepository)
        {
            this.customerRepository = customerRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await customerRepository.GetStatus();
        }

        public async Task<IEnumerable<DocumentUpload>> GetDocumentUpload()
        {
            return await customerRepository.GetDocumentUpload();
        }

        public async Task<IEnumerable<Status>> GetStatusByCustomer(int id)
        {
            return await customerRepository.GetStatusByCustomer(id);
        }
        public async Task<IEnumerable<Division>> GetDivision()
        {
            return await customerRepository.GetDivision();
        }

        public async Task<IEnumerable<Division>> GetDivisionByCustomer(int id)
        {
            return await customerRepository.GetDivisionByCustomer(id);
        }


        public async Task<IEnumerable<CustomerType>> GetCustomerType()
        {
            return await customerRepository.GetCustomerType();
        }

        public async Task<IEnumerable<CustomerType>> GetCustomerTypeByCustomer(int id)
        {
            return await customerRepository.GetCustomerTypeByCustomer(id);
        }


        public async Task<IEnumerable<DocumentType>> GetDocumentType()
        {
            return await customerRepository.GetDocumentType();
        }

        public async Task<IEnumerable<DocumentType>> GetDocumentTypeByCustomer(int id)
        {
            return await customerRepository.GetDocumentTypeByCustomer(id);
        }
        public async Task<IEnumerable<Account>> GetAccount()
        {
            return await customerRepository.GetAccount();
        }

        public async Task<IEnumerable<Account>> GetAccountByCustomer(int id)
        {
            return await customerRepository.GetAccountByCustomer(id);
        }
        public async Task<IEnumerable<AccountType>> GetAccountType()
        {
            return await customerRepository.GetAccountType();
        }

        public async Task<IEnumerable<AccountType>> GetAccountTypeByCustomer(int id)
        {
            return await customerRepository.GetAccountTypeByCustomer(id);
        }
        public async Task<IEnumerable<SalesPerson>> GetSalesPerson()
        {
            return await customerRepository.GetSalesPerson();
        }

        public async Task<IEnumerable<SalesPerson>> GetSalesPersonByCustomer(int id)
        {
            return await customerRepository.GetSalesPersonByCustomer(id);
        }
        public async Task<IEnumerable<Contact>> GetContact()
        {
            return await customerRepository.GetContact();
        }

        public async Task<IEnumerable<Contact>> GetContactByCustomer(int id)
        {
            return await customerRepository.GetContactByCustomer(id);
        }
        public async Task<IEnumerable<Address>> GetAddress()
        {
            return await customerRepository.GetAddress();
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await customerRepository.GetGeoLevel1();
        }

        public async Task<IEnumerable<Address>> GetAddressByCustomer(int id)
        {
            return await customerRepository.GetAddressByCustomer(id);
        }

    }
}
