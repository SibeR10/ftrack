﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Services.Implements
{
    public class CountryService : GenericService<Country>, ICountryService
    {
        private ICountryRepository CountryRepository;

        public CountryService(ICountryRepository CountryRepository) : base(CountryRepository)
        {
            this.CountryRepository = CountryRepository;
        }

    }
}
