﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ReservationDTO
    {
                    
        public int ReservationID { get; set; }

        [Required(ErrorMessage = "The ReservationDate is required")]
        [Display(Name = "Fecha de reserva")]
        public string ReservationDate { get; set; }


        [Display(Name = "Tipo de servicio")]
        public int ServiceTypeID { get; set; }

        [Display(Name = "Origen")]
        public int OriginID { get; set; }

        [Display(Name = "Destino")]
        public int DestinationID { get; set; }


        [Display(Name = "Dirección")]
        public string AddressQuote { get; set; }


        [Display(Name = "Referencia")]
        public string ReferenceQuote { get; set; }


        [Required(ErrorMessage = "The SalesPersonID is required")]
        [Display(Name = "Vendedor")]
        public int SalesPersonID { get; set; }


        [Required(ErrorMessage = "The CustomerID is required")]
        [Display(Name = "Cliente")]
        public int CustomerID { get; set; }


        [Display(Name = "Estatus")]
        public int StatusReservationID { get; set; }


    }
}

	
	
	
	
	
	
	


