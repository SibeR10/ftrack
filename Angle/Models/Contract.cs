﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Contract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContractID { get; set; }
        public string ContractDescription { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
        public ServiceType ServiceType { get; set; }
        public int ServiceTypeID { get; set; }
    }
}
