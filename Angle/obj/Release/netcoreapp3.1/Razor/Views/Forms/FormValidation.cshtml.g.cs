#pragma checksum "C:\Users\siber\Desktop\Fuente actual Ftrack\Angle\Angle\Views\Forms\FormValidation.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a312ff825578e383985b27a50332653779cd6058"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(Angle.Pages.Forms.Views_Forms_FormValidation), @"mvc.1.0.view", @"/Views/Forms/FormValidation.cshtml")]
namespace Angle.Pages.Forms
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\siber\Desktop\Fuente actual Ftrack\Angle\Angle\Views\_ViewImports.cshtml"
using Angle;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a312ff825578e383985b27a50332653779cd6058", @"/Views/Forms/FormValidation.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2cac7a1b7297ab84fc06688fdfc495972e2e12c0", @"/Views/_ViewImports.cshtml")]
    public class Views_Forms_FormValidation : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("#"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-parsley-validate", new global::Microsoft.AspNetCore.Html.HtmlString(""), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("novalidate", new global::Microsoft.AspNetCore.Html.HtmlString(""), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-horizontal"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/vendor/parsleyjs/dist/parsley.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("names", "Development", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("names", "Staging,Production", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.EnvironmentTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\siber\Desktop\Fuente actual Ftrack\Angle\Angle\Views\Forms\FormValidation.cshtml"
  
    ViewBag.Title = "FormValidation";

#line default
#line hidden
#nullable disable
            WriteLiteral("<div class=\"content-heading\">\r\n   <div>Form Validation\r\n      <small>Validating forms frontend have never been so powerful and easy.</small>\r\n   </div>\r\n</div>\r\n<!-- START row-->\r\n<div class=\"row\">\r\n   <div class=\"col-xl-6\">\r\n      ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a312ff825578e383985b27a50332653779cd60586525", async() => {
                WriteLiteral(@"
         <!-- START card-->
         <div class=""card card-default"">
            <div class=""card-header"">
               <div class=""card-title"">Form Register</div>
            </div>
            <div class=""card-body"">
               <div class=""form-group"">
                  <label class=""col-form-label"">Email Address *</label>
                  <input class=""form-control"" type=""text"" name=""email"" required=""required"" />
               </div>
               <div class=""form-group"">
                  <label class=""col-form-label"">Password *</label>
                  <input class=""form-control"" id=""id-password"" type=""password"" name=""password"" required=""required"" />
               </div>
               <div class=""form-group"">
                  <label class=""col-form-label"">Confirm Password *</label>
                  <input class=""form-control"" type=""password"" name=""confirmPassword"" required=""required"" data-parsley-equalto=""#id-password"" />
               </div>
               <div class=""");
                WriteLiteral(@"required"">* Required fields</div>
            </div>
            <div class=""card-footer"">
               <div class=""clearfix"">
                  <div class=""float-left"">
                     <div class=""checkbox c-checkbox"">
                        <label>
                           <input type=""checkbox"" name=""agreements"" required=""required"" data-parsley-error-message=""Please read and agree the terms"" />
                           <span class=""fa fa-check""></span>I agree with the<a class=""ml-2"" href=""#"">terms</a>
                        </label>
                     </div>
                  </div>
                  <div class=""float-right"">
                     <button class=""btn btn-primary"" type=""submit"">Register</button>
                  </div>
               </div>
            </div>
         </div>
         <!-- END card-->
      ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n   </div>\r\n   <div class=\"col-xl-6\">\r\n      ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a312ff825578e383985b27a50332653779cd605810260", async() => {
                WriteLiteral(@"
         <!-- START card-->
         <div class=""card card-default"">
            <div class=""card-header"">
               <div class=""card-title"">Form Login</div>
            </div>
            <div class=""card-body"">
               <div class=""form-group"">
                  <label class=""col-form-label"">Email Address *</label>
                  <input class=""form-control"" type=""text"" name=""email"" required=""required"" />
               </div>
               <div class=""form-group"">
                  <label class=""col-form-label"">Password *</label>
                  <input class=""form-control"" type=""password"" name=""password"" required=""required"" />
               </div>
               <div class=""required"">* Required fields</div>
            </div>
            <div class=""card-footer"">
               <button class=""btn btn-primary"" type=""submit"">Login</button>
            </div>
         </div>
         <!-- END card-->
      ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n   </div>\r\n</div>\r\n<!-- END row-->\r\n<!-- START row-->\r\n<div class=\"row\">\r\n   <div class=\"col-lg-12\">\r\n      ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a312ff825578e383985b27a50332653779cd605812979", async() => {
                WriteLiteral(@"
         <!-- START card-->
         <div class=""card card-default"">
            <div class=""card-header"">
               <div class=""card-title"">Fields validation</div>
            </div>
            <div class=""bg-gray-lighter px-3 py-2 my-3"">Type validation</div>
            <div class=""card-body"">
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Required Text</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" name=""required"" required=""required"" />
                     </div>
                     <div class=""col-md-4""><code>required</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Email</label>
                     <div class=""col-md-6"">
                        <input c");
                WriteLiteral(@"lass=""form-control"" type=""email"" name=""email"" data-parsley-type=""email"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-type=""email""</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Number</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" name=""number"" data-parsley-type=""number"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-type=""number""</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Integer</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" typ");
                WriteLiteral(@"e=""text"" name=""integer"" data-parsley-type=""integer"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-type=""integer""</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Digits</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" name=""digits"" data-parsley-type=""digits"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-type=""digits""</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Alphanum</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" name=""al");
                WriteLiteral(@"phanum"" data-parsley-type=""alphanum"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-type=""alphanum""</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Url</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" name=""url"" data-parsley-type=""url"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-type=""url""</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset class=""m-0"">
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Equal to</label>
                     <div class=""col-md-3"">
                        <input class=""form-control"" id=""id-source"" type=""text"" placeho");
                WriteLiteral(@"lder=""#id-source"" />
                     </div>
                     <div class=""col-md-3"">
                        <input class=""form-control"" type=""text"" data-parsley-equalto=""#id-source"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-equalto='#id-source'</code>
                     </div>
                  </div>
               </fieldset>
            </div>
            <div class=""bg-gray-lighter px-3 py-2 mb-3"">Range validation</div>
            <div class=""card-body"">
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Minlength</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" name=""minlength"" data-parsley-minlength=""6"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-minlength=""6""</code>
                     </div>
                  </div>
       ");
                WriteLiteral(@"        </fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Maxlength</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" name=""maxlength"" data-parsley-maxlength=""10"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-maxlength=""10""</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Length</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" name=""length"" data-parsley-length=""[6, 10]"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-length=""[6, 10]""</code>
                     </div>
                  </div>
               ");
                WriteLiteral(@"</fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Min</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" name=""min"" data-parsley-min=""6"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-min=""6""</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Max</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" name=""max"" data-parsley-max=""6"" />
                     </div>
                     <div class=""col-md-4""><code>data-parsley-max=""6""</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  ");
                WriteLiteral(@"<div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Min words</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" data-minwords=""6"" />
                     </div>
                     <div class=""col-md-4""><code>data-minwords='6'</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-form-label"">Max words</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" data-maxwords=""6"" />
                     </div>
                     <div class=""col-md-4""><code>data-maxwords='6'</code>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class=""form-group row"">
                     <label class=""col-md-2 col-fo");
                WriteLiteral(@"rm-label"">Range of words</label>
                     <div class=""col-md-6"">
                        <input class=""form-control"" type=""text"" data-rangewords=""[6,10]"" />
                     </div>
                     <div class=""col-md-4""><code>data-rangewords='[6,10]'</code>
                     </div>
                  </div>
               </fieldset>
            </div>
            <div class=""card-footer text-center"">
               <button class=""btn btn-info"" type=""submit"">Run validation</button>
            </div>
         </div>
         <!-- END card-->
      ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n   </div>\r\n</div>\r\n<!-- END row-->\r\n\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("environment", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a312ff825578e383985b27a50332653779cd605824060", async() => {
                    WriteLiteral("\r\n        ");
                    __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a312ff825578e383985b27a50332653779cd605824342", async() => {
                    }
                    );
                    __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                    __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                    __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                    await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                    if (!__tagHelperExecutionContext.Output.IsContentModified)
                    {
                        await __tagHelperExecutionContext.SetOutputContentAsync();
                    }
                    Write(__tagHelperExecutionContext.Output);
                    __tagHelperExecutionContext = __tagHelperScopeManager.End();
                    WriteLiteral("\r\n    ");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.EnvironmentTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper.Names = (string)__tagHelperAttribute_6.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("environment", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a312ff825578e383985b27a50332653779cd605826475", async() => {
                    WriteLiteral("\r\n        ");
                    __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a312ff825578e383985b27a50332653779cd605826757", async() => {
                    }
                    );
                    __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                    __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                    __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                    await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                    if (!__tagHelperExecutionContext.Output.IsContentModified)
                    {
                        await __tagHelperExecutionContext.SetOutputContentAsync();
                    }
                    Write(__tagHelperExecutionContext.Output);
                    __tagHelperExecutionContext = __tagHelperScopeManager.End();
                    WriteLiteral("\r\n    ");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.EnvironmentTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper.Names = (string)__tagHelperAttribute_7.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n\r\n");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
