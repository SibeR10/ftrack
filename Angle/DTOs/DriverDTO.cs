﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class DriverDTO
    {
        public int DriverID { get; set; }

        [Required(ErrorMessage = "The DriverName is required")]
        [Display(Name = "Nombres")]
        public string DriverName { get; set; }

        [Required(ErrorMessage = "The DriverName is required")]
        [Display(Name = "Apellidos")]
        public string DriverLastName { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }

        [Required(ErrorMessage = "The Division is required")]
        [Display(Name = "Division")]
        public int DivisionID { get; set; }

        [Required(ErrorMessage = "The DriverType is required")]
        [Display(Name = "Tipo de chofer")]
        public int DriverTypeID { get; set; }

        [Required(ErrorMessage = "The Document is required")]
        [Display(Name = "Documento")]
        public int Document { get; set; }

        [Required(ErrorMessage = "The DocumentType is required")]
        [Display(Name = "Tipo de documento")]
        public int DocumentTypeID { get; set; }
        

    }
}

