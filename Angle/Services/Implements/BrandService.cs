﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class BrandService : GenericService<Brand>, IBrandService
    {
        private IBrandRepository brandRepository;

        public BrandService(IBrandRepository brandRepository) : base(brandRepository)
        {
            this.brandRepository = brandRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await brandRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByBrand(int id)
        {
            return await brandRepository.GetStatusByBrand(id);
        }

    }
}
