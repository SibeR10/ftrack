﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class TravelRepository : GenericRepository<Travel>, ITravelRepository
    {
        private readonly FTContext FTContext;

        public TravelRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }
        public async Task<IEnumerable<GeoLevel1>> GetOrigin()
        {

            var listOrigin = await FTContext.GeoLevel1.ToListAsync();

            return listOrigin;
        }
        public async Task<IEnumerable<GeoLevel1>> GetDestination()
        {

            var listDestination = await FTContext.GeoLevel1.ToListAsync();

            return listDestination;
        }
        public async Task<IEnumerable<Driver>> GetDriver()
        {

            var listDriver = await FTContext.Driver.ToListAsync();

            return listDriver;
        }
        public async Task<IEnumerable<Vehicles>> GetVehicles()
        {

            var listVehicles = await FTContext.Vehicles.ToListAsync();

            return listVehicles;
        }
        public async Task<IEnumerable<HistoricTravel>> GetHistoricTravel()
        {

            var listHistoricTravel = await FTContext.HistoricTravel.ToListAsync();

            return listHistoricTravel;
        }
        public async Task<IEnumerable<ItemsServiceOrder>> GetItemsServiceOrder()
        {

            var listItemsServiceOrder = await FTContext.ItemsServiceOrder.ToListAsync();

            return listItemsServiceOrder;
        }
        public async Task<IEnumerable<ServiceOrder>> GetServiceOrder()
        {

            var listServiceOrder = await FTContext.ServiceOrder.ToListAsync();

            return listServiceOrder;
        }
        public async Task<IEnumerable<ItemsQuote>> GetItemsQuote()
        {

            var listItemsQuote = await FTContext.ItemsQuote.ToListAsync();

            return listItemsQuote;
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {

            var listGeoLevel1 = await FTContext.GeoLevel1.ToListAsync();

            return listGeoLevel1;
        }
        public async Task<IEnumerable<Customer>> GetCustomer()
        {

            var listCustomer = await FTContext.Customer.ToListAsync();

            return listCustomer;
        }
        public async Task<IEnumerable<StopTravel>> GetStopTravel()
        {

            var listStopTravel = await FTContext.StopTravel.ToListAsync();

            return listStopTravel;
        }


        
    }
}
