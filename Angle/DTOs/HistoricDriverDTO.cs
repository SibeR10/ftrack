﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class HistoricDriverDTO
    {
        public int HistoricDriverID { get; set; }

		[Display(Name = "Culminacion Uso Vehiculo")]
		public DateTime DateStartVehicle { get; set; }

		[Display(Name = "Inicio Uso Vehiculo")]
		public DateTime DateEndVehicle { get; set; }

		[Display(Name = "Estatus")]
		public int StatusID { get; set; }

		[Display(Name = "Chofer")]
		public int DriverID { get; set; }

		[Display(Name = "Vehiculo")]
		public int VehiclesID { get; set; }

	}
}
