﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class DocumentType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DocumentTypeID { get; set; }
        public string DocumentTypeName { get; set; }
        public string DocumentTypeDescription { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
