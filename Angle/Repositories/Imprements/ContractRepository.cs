﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class ContractRepository : GenericRepository<Contract>, IContractRepository
    {
        private readonly FTContext FTContext;

        public ContractRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }
        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {

            var listServiceType = await FTContext.ServiceType.ToListAsync();

            return listServiceType;
        }
        public async Task<IEnumerable<ContractSequence>> GetContractSequence()
        {

            var listContractSequence = await FTContext.ContractSequence.ToListAsync();

            return listContractSequence;
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {

            var listGeoLevel1 = await FTContext.GeoLevel1.ToListAsync();

            return listGeoLevel1;
        }
        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {

            var listWarehouse = await FTContext.Warehouse.ToListAsync();

            return listWarehouse;
        }
        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {

            var listMerchandiseType = await FTContext.MerchandiseType.ToListAsync();

            return listMerchandiseType;
        }
        public async Task<IEnumerable<DeliveryMode>> GetDeliveryMode()
        {

            var listDeliveryMode = await FTContext.DeliveryMode.ToListAsync();

            return listDeliveryMode;
        }



    }
}
