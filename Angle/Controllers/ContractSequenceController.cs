﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using ContractSequence = Angle.Models.ContractSequence;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class ContractSequenceController : Controller
    {
        private readonly IContractSequenceService ContractSequenceService;
        private readonly IMapper mapper;

        public ContractSequenceController(IContractSequenceService ContractSequenceService,
            IMapper mapper)
        {
            this.ContractSequenceService = ContractSequenceService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await ContractSequenceService.GetAll();


            ViewBag.Contract = await ContractSequenceService.GetContract();
            ViewBag.GeoLevel1 = await ContractSequenceService.GetGeoLevel1();
            ViewBag.Warehouse = await ContractSequenceService.GetWarehouse();
            ViewBag.MerchandiseType = await ContractSequenceService.GetMerchandiseType();
            ViewBag.DeliveryMode = await ContractSequenceService.GetDeliveryMode();
            ViewBag.Status = await ContractSequenceService.GetStatus();
            ViewBag.ServiceType = await ContractSequenceService.GetServiceType();

            var listContractSequence = data.Select(x => mapper.Map<ContractSequenceDTO>(x)).ToList();
            return View(listContractSequence);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Contract = await ContractSequenceService.GetContract();
            ViewBag.GeoLevel1 = await ContractSequenceService.GetGeoLevel1();
            ViewBag.Warehouse = await ContractSequenceService.GetWarehouse();
            ViewBag.MerchandiseType = await ContractSequenceService.GetMerchandiseType();
            ViewBag.DeliveryMode = await ContractSequenceService.GetDeliveryMode();
            ViewBag.Status = await ContractSequenceService.GetStatus();
            ViewBag.ServiceType = await ContractSequenceService.GetServiceType();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ContractSequenceDTO ContractSequenceDTO)
        {
            
            if (ModelState.IsValid)
            {

                var ContractSequence = mapper.Map<ContractSequence>(ContractSequenceDTO);

                ContractSequence = await ContractSequenceService.Insert(ContractSequence);

                return RedirectToAction("Index","Contract");
            }

            return View(ContractSequenceDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var ContractSequence = await ContractSequenceService.GetById(id.Value);

            if (ContractSequence == null)
                return NotFound();

            ViewBag.Contract = await ContractSequenceService.GetContract();
            ViewBag.GeoLevel1 = await ContractSequenceService.GetGeoLevel1();
            ViewBag.Warehouse = await ContractSequenceService.GetWarehouse();
            ViewBag.MerchandiseType = await ContractSequenceService.GetMerchandiseType();
            ViewBag.DeliveryMode = await ContractSequenceService.GetDeliveryMode();
            ViewBag.Status = await ContractSequenceService.GetStatus();
            ViewBag.ServiceType = await ContractSequenceService.GetServiceType();
            var ContractSequenceDTO = mapper.Map<ContractSequenceDTO>(ContractSequence);
            return View(ContractSequenceDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ContractSequenceDTO ContractSequenceDTO)
        {
            if (ModelState.IsValid)
            {
                var ContractSequence = mapper.Map<ContractSequence>(ContractSequenceDTO);

                ContractSequence = await ContractSequenceService.Update(ContractSequence);

                return RedirectToAction("Index", "Contract");
            }

            return View(ContractSequenceDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var ContractSequence = await ContractSequenceService.GetById(id.Value);

            if (ContractSequence == null)
                return NotFound();

            var ContractSequenceDTO = mapper.Map<ContractSequenceDTO>(ContractSequence);

            return View(ContractSequenceDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await ContractSequenceService.Delete(id.Value);

            return RedirectToAction("Index", "Contract");
        }
    }
}