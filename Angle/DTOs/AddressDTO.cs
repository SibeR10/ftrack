﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class AddressDTO
    {
        public int AddressID { get; set; }

        [Display(Name = "Dirección")]
        public string AddressName { get; set; }

        [Display(Name = "Código Postal")]
        public int PostalCode { get; set; }

        [Display(Name = "Tipo de dirección")]
        public int AddressTypeID { get; set; }


        [Display(Name = "GeoLevel 1")]
        public int GeoLevel1ID { get; set; }


        [Display(Name = "Estatus")]
        public int StatusID { get; set; }

        [Display(Name = "Cliente")]
        public int CustomerID { get; set; }
    }
}
