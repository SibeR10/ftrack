﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ContractMeasuryDTO
    {
        public int ContractMeasuryID { get; set; }

        [Display(Name = "ID Tarifa regular")]
        public int ContractSequenceID { get; set; }

        [Display(Name = "ID Unidad de medida")]
        public int MeasureUnitID { get; set; }

        [Display(Name = "Minimo")]
        public int Minimum { get; set; }

        [Display(Name = "Maximo")]
        public int Maximum { get; set; }

        [Display(Name = "Maximo")]
        public double Tariff { get; set; }
    }
}


	
	
	
