﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ContractSpecialMeasury
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContractSpecialMeasuryID { get; set; }
        public ContractSpecialSequence ContractSpecialSequence { get; set; }
        public int ContractSpecialSequenceID { get; set; }
        public MeasureUnit MeasureUnit { get; set; }
        public int MeasureUnitID { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public double Tariff { get; set; }
    }
}
	
	
	
