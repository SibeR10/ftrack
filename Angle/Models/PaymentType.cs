﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class PaymentType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PaymentTypeID { get; set; }
        public string PaymentTypeDescription { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
