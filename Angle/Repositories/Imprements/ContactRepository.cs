﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class ContactRepository : GenericRepository<Contact>, IContactRepository
    {
        private readonly FTContext FTContext;

        public ContactRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }

        public async Task<IEnumerable<Status>> GetStatusByContact(int id)
        {

            var listStatusByContact = await FTContext.Contact
                .Include(x => x.Status)
                .Where(x => x.StatusID == id)
                .Select(x => x.Status)
                .ToListAsync();

            return listStatusByContact;
        }
        public async Task<IEnumerable<Customer>> GetCustomer()
        {

            var listCustomer = await FTContext.Customer.ToListAsync();

            return listCustomer;
        }


        public async Task<IEnumerable<Customer>> GetCustomerById(int id)
        {

            var listCustomerById = await FTContext.Customer
                .Where(x => x.CustomerID == id)
                .ToListAsync();

            return listCustomerById;
        }
        
    }
}
