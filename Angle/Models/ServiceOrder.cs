﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ServiceOrder
    {
		         
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServiceOrderID { get; set; }
		public Quote Quote { get; set; }
		public int QuoteID { get; set; }
		public string DateDeliveryMax { get; set; }
		public string ServiceOrderDate { get; set; }
		public string CollectDate { get; set; }
		public string InTransitDate { get; set; }
		public string DeliveryDate { get; set; }
		public ServiceOrderStatus ServiceOrderStatus { get; set; }
		public int ServiceOrderStatusID { get; set; }
		public ServiceType ServiceType { get; set; }
		public int ServiceTypeID { get; set; }
		public int OriginID { get; set; }
		public int DestinationID { get; set; }
		public string AddressQuote { get; set; }
		public string ReferenceQuote { get; set; }
		public SalesPerson SalesPerson { get; set; }
		public int SalesPersonID { get; set; }
		public Customer Customer { get; set; }
		public int CustomerID { get; set; }
		public int Remittee { get; set; }
		public int MainPhone { get; set; }
		public string Email { get; set; }
		public int Sender { get; set; }
		public int IdCreate { get; set; }
		public int MaxDays { get; set; }
		public string DescriptionCreate { get; set; }

	}
}

