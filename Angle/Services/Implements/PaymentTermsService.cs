﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class PaymentTermsService : GenericService<PaymentTerms>, IPaymentTermsService
    {
        private IPaymentTermsRepository paymenttermsRepository;

        public PaymentTermsService(IPaymentTermsRepository paymenttermsRepository) : base(paymenttermsRepository)
        {
            this.paymenttermsRepository = paymenttermsRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await paymenttermsRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByPaymentTerms(int id)
        {
            return await paymenttermsRepository.GetStatusByPaymentTerms(id);
        }

    }
}
