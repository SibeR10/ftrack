﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IContractSpecialSequenceService : IGenericService<ContractSpecialSequence>
    {
        Task<IEnumerable<Status>> GetStatus();
        
        Task<IEnumerable<Customer>> GetCustomer();
        Task<IEnumerable<ContractSpecial>> GetContractSpecial();

        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();
        Task<IEnumerable<Warehouse>> GetWarehouse();
        Task<IEnumerable<MerchandiseType>> GetMerchandiseType();
        Task<IEnumerable<DeliveryMode>> GetDeliveryMode();
        Task<IEnumerable<ServiceType>> GetServiceType();

    }
}
