﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class ServiceOrderStatusRepository : GenericRepository<ServiceOrderStatus>, IServiceOrderStatusRepository
    {
        private readonly FTContext FTContext;

        public ServiceOrderStatusRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
    }
}
