﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;
using Status = Angle.Models.Status;

namespace Angle.Controllers
{
    public class StatusController : Controller
    {
        private readonly IStatusService statusService;
        private readonly IMapper mapper;

        public StatusController(IStatusService statusService,
            IMapper mapper)
        {
            this.statusService = statusService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await statusService.GetAll();

            var listStatus = data.Select(x => mapper.Map<StatusDTO>(x)).ToList();

            return View(listStatus);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(StatusDTO statusDTO)
        {
            if (ModelState.IsValid)
            {

                var status = mapper.Map<Status>(statusDTO);

                status = await statusService.Insert(status);


                return RedirectToAction("Index");
            }

            return View(statusDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var status = await statusService.GetById(id.Value);

            if (status == null)
                return NotFound();

            var statusDTO = mapper.Map<StatusDTO>(status);

            return View(statusDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(StatusDTO statusDTO)
        {
            if (ModelState.IsValid)
            {
                var status = mapper.Map<Status>(statusDTO);

                status = await statusService.Update(status);
                return RedirectToAction("Index");
            }

            return View(statusDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var status = await statusService.GetById(id.Value);

            if (status == null)
                return NotFound();

            var statusDTO = mapper.Map<StatusDTO>(status);

            return View(statusDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await statusService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}