﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using ServiceClass = Angle.Models.ServiceClass;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class ServiceClassController : Controller
    {
        private readonly IServiceClassService serviceclassService;
        private readonly IMapper mapper;

        public ServiceClassController(IServiceClassService serviceclassService,
            IMapper mapper)
        {
            this.serviceclassService = serviceclassService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await serviceclassService.GetAll();
            ViewBag.Status = await serviceclassService.GetStatus();
            var listServiceClass = data.Select(x => mapper.Map<ServiceClassDTO>(x)).ToList();
            return View(listServiceClass);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await serviceclassService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ServiceClassDTO serviceclassDTO)
        {
            
            if (ModelState.IsValid)
            {

                var serviceclass = mapper.Map<ServiceClass>(serviceclassDTO);

                serviceclass = await serviceclassService.Insert(serviceclass);

                return RedirectToAction("Index");
            }

            return View(serviceclassDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var serviceclass = await serviceclassService.GetById(id.Value);

            if (serviceclass == null)
                return NotFound();

            ViewBag.Status = await serviceclassService.GetStatus();
            var serviceclassDTO = mapper.Map<ServiceClassDTO>(serviceclass);
            return View(serviceclassDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ServiceClassDTO serviceclassDTO)
        {
            if (ModelState.IsValid)
            {
                var serviceclass = mapper.Map<ServiceClass>(serviceclassDTO);

                serviceclass = await serviceclassService.Update(serviceclass);

                return RedirectToAction("Index");
            }

            return View(serviceclassDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var serviceclass = await serviceclassService.GetById(id.Value);

            if (serviceclass == null)
                return NotFound();

            var serviceclassDTO = mapper.Map<ServiceClassDTO>(serviceclass);

            return View(serviceclassDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await serviceclassService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}