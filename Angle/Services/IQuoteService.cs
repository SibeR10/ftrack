﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IQuoteService : IGenericService<Quote>
    {
        Task<IEnumerable<QuoteStatus>> GetQuoteStatus();
        Task<IEnumerable<ChargesByQuote>> GetChargesByQuote();
        Task<IEnumerable<Warehouse>> GetWarehouse();
        Task<IEnumerable<Country>> GetCountry();
        Task<IEnumerable<Customer>> GetCustomer();
        Task<IEnumerable<SalesPerson>> GetSalesPerson();
        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();
        Task<IEnumerable<Division>> GetDivision();
        Task<IEnumerable<AddressType>> GetAddressType();
        Task<IEnumerable<ServiceType>> GetServiceType();
        Task<IEnumerable<ItemsQuote>> GetItemsQuote();
        Task<IEnumerable<MerchandiseType>> GetMerchandiseType();
        Task<IEnumerable<ContractSpecial>> GetContractSpecial();
        Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence();
        Task<IEnumerable<ContractSequence>> GetContractSequence();
        Task<IEnumerable<Person>> GetSender();
        Task<IEnumerable<Person>> GetRemittee();

    }
}
