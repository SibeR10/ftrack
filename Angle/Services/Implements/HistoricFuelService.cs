﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class HistoricFuelService : GenericService<HistoricFuel>, IHistoricFuelService
    {
        private IHistoricFuelRepository historicfuelRepository;

        public HistoricFuelService(IHistoricFuelRepository historicfuelRepository) : base(historicfuelRepository)
        {
            this.historicfuelRepository = historicfuelRepository;
        }

        public async Task<IEnumerable<Driver>> GetDriver()
        {
            return await historicfuelRepository.GetDriver();
        }
        public async Task<IEnumerable<Fuel>> GetFuel()
        {
            return await historicfuelRepository.GetFuel();
        }
        public async Task<IEnumerable<Vehicles>> GetVehiclesById(int id)
        {
            return await historicfuelRepository.GetVehiclesById(id);
        }

    }
}
