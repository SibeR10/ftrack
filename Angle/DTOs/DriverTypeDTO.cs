﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class DriverTypeDTO
    {
        public int DriverTypeID { get; set; }

        [Required(ErrorMessage = "The DriverTypeName is required")]
        [Display(Name = "Tipo de chofer")]
        public string DriverTypeName { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

