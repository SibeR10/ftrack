﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IReportesService : IGenericService<Customer>
    {
        Task<IEnumerable<Customer>> GetFilterCustomer(int division, int CustomerTypeID, int StatusID);
        Task<IEnumerable<Customer>> GetCustomer();
        
    }
}
