﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Quote = Angle.Models.Quote;
using Angle.Services.Implements;
using System.Data;
using Microsoft.Data.SqlClient;
using System;

using Angle.Data;
using Microsoft.EntityFrameworkCore;

namespace Angle.Controllers
{
    public class QuoteController : Controller
    {
        private readonly IQuoteService quoteService;
        private readonly IServiceOrderService ServiceOrderService;
        private readonly IMapper mapper;


        private readonly FTContext _context;
        public QuoteController(FTContext context, IQuoteService quoteService,
            IMapper mapper)
        {
            this.quoteService = quoteService;
            this.mapper = mapper;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var data = await quoteService.GetAll();
            ViewBag.Customer = await quoteService.GetCustomer();
            ViewBag.SalesPerson = await quoteService.GetSalesPerson();
            ViewBag.QuoteStatus = await quoteService.GetQuoteStatus();
            var listQuote = data.Select(x => mapper.Map<QuoteDTO>(x)).ToList();
            return View(listQuote);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.QuoteStatus = await quoteService.GetQuoteStatus();
            ViewBag.ServiceType = await quoteService.GetServiceType();
            ViewBag.Customer = await quoteService.GetCustomer();
            ViewBag.Sender = await quoteService.GetSender();
            ViewBag.Remittee = await quoteService.GetRemittee();
            ViewBag.SalesPerson = await quoteService.GetSalesPerson();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(QuoteDTO quoteDTO)
        {
            
            if (ModelState.IsValid)
            {

                var quote = mapper.Map<Quote>(quoteDTO);

                quote = await quoteService.Insert(quote);

                var url = "/Edit/" + quote.QuoteID;
                return RedirectToAction(url);
            }

            return View(quoteDTO);
        }

        public async Task<IActionResult> CreatePerson(string PersonName, int PersonDocument, int PersonTelephone, string PersonEmail)
        {

            SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
            conexion.Open();

            string cadena = "INSERT INTO [dbo].[FT_Person]([PersonName],[PersonDocument],[PersonTelepone],[PersonEmail])VALUES('"  + PersonName + "'," + PersonDocument + "," + PersonTelephone + ",'" + PersonEmail + "')";
            SqlCommand comando = new SqlCommand(cadena, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();

            var url = "/Index/";
            return RedirectToAction(url);
        }
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var quote = await quoteService.GetById(id.Value);

            if (quote == null)
                return NotFound();

            ViewBag.QuoteStatus = await quoteService.GetQuoteStatus();
            ViewBag.ServiceType = await quoteService.GetServiceType();
            ViewBag.Customer = await quoteService.GetCustomer();
            ViewBag.Sender = await quoteService.GetSender();
            ViewBag.Remittee = await quoteService.GetRemittee();
            ViewBag.SalesPerson = await quoteService.GetSalesPerson();
            ViewBag.GeoLevel1 = await quoteService.GetGeoLevel1();
            ViewBag.Warehouse = await quoteService.GetWarehouse();
            ViewBag.ItemsQuote = await quoteService.GetItemsQuote();
            ViewBag.MerchandiseType = await quoteService.GetMerchandiseType();
            ViewBag.ContractSpecial = await quoteService.GetContractSpecial();
            ViewBag.ContractSpecialSequence = await quoteService.GetContractSpecialSequence();
            ViewBag.ContractSequence = await quoteService.GetContractSequence();
            var quoteDTO = mapper.Map<QuoteDTO>(quote);
            return View(quoteDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(QuoteDTO quoteDTO)
        {
            
                var quote = mapper.Map<Quote>(quoteDTO);
                //var serviceorder = mapper.Map<ServiceOrder>(ServiceOrderDTO);


                if (quote.QuoteStatusID == 1) { // cotizacion aprobada
                    //agregar orden de servicio
                    SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                    conexion.Open();
                    int Quoteid = quote.QuoteID;
                    DateTime now = DateTime.Now;
                    string strDate = now.ToString("yyyy-MM-dd hh:mm:ss");
                    int Remittee = quote.Remittee;
                    int Sender = quote.Sender;
                    int MainPhone = quote.MainPhone;
                    string Email = quote.Email;
                    int ServiceType = quote.ServiceTypeID;
                    int Origin = quote.OriginID;
                    int Destination = quote.DestinationID;
                    string Address = quote.AddressQuote;
                    string Reference = quote.ReferenceQuote;
                    int SalesPerson = quote.SalesPersonID;
                    int Customer = quote.CustomerID;
                    string DateDeliveryMax = "-";
                    int MaxDays = 0;
                    string cadena = "insert into [dbo].[FT_ServiceOrder]([QuoteID],[ServiceOrderDate],[CollectDate],[InTransitDate],[DeliveryDate],[ServiceOrderStatusID] ,[ServiceTypeID],[OriginID],[DestinationID],[AddressQuote] ,[ReferenceQuote],[SalesPersonID],[CustomerID],[Remittee] ,[Sender],[MainPhone] ,[Email],[IdCreate],[DescriptionCreate],[DateDeliveryMax],[MaxDays])" + "values (" + Quoteid + ",'" + strDate + "','0001-01-01','0001-01-01','0001-01-01',1,	"+ ServiceType + ","+Origin+","+Destination+",'"+ Address + "','"+Reference+"','"+ SalesPerson + "','"+Customer+"'," + Remittee + "," + Sender + "," + MainPhone + ",'" + Email + "',"+ Quoteid + ",'ID Cotización', '" + DateDeliveryMax + "' , " + MaxDays + ")";
                    SqlCommand comando = new SqlCommand(cadena, conexion);
                    comando.ExecuteNonQuery();
                    conexion.Close();
                }
                quote = await quoteService.Update(quote);

                var ContractSequence = await _context.ContractSequence.ToListAsync();
                var items = 0;
                var ItemsQuote = await _context.ItemsQuote.ToListAsync();
                for (int i = 0; i < ContractSequence.Count; i++)
                { // Loop through List with for
                    if(ContractSequence[i].ServiceTypeID == quote.ServiceTypeID && ContractSequence[i].OriginID == quote.OriginID && ContractSequence[i].DestinationID == quote.DestinationID)
                    {
                        items++;
                    }
                }
                
                if(items == 0)
                {
                    SqlConnection conexion2 = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                    conexion2.Open();
                    string cadena2 = "delete from [dbo].[FT_ItemsQuote] where QuoteID = " + quote.QuoteID + "";
                    SqlCommand comando2 = new SqlCommand(cadena2, conexion2);
                    comando2.ExecuteNonQuery();
                    conexion2.Close();
                }
                else
                {
                    for (int i = 0; i < ItemsQuote.Count; i++)
                    { // Loop through List with for
                        if (ItemsQuote[i].QuoteID == quote.QuoteID)
                        {
                            var Weight = ItemsQuote[i].Weight;
                            var Price = 0;
                            for (int x = 0; x < ContractSequence.Count; x++)
                            { // Loop through List with for
                                if (ContractSequence[x].ServiceTypeID == quote.ServiceTypeID && ContractSequence[x].OriginID == quote.OriginID && ContractSequence[x].DestinationID == quote.DestinationID)
                                {
                                    if(Weight <= ContractSequence[x].WeightMin)
                                    {
                                        Price = ContractSequence[x].BasePrice;
                                    }
                                    else
                                    if (Weight > ContractSequence[x].WeightMin)
                                    {
                                        var extra = Weight - ContractSequence[x].WeightMin;
                                        extra = extra * ContractSequence[x].ExtraPrice;
                                        Price = extra + ContractSequence[x].BasePrice;
                                    }
                                    //    UPDATE[dbo].[FT_ItemsQuote] SET[Amount] = < Amount, int,> WHERE QuoteID = 0
                                    SqlConnection conexion3 = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                                    conexion3.Open();
                                    string cadena3 = "UPDATE[dbo].[FT_ItemsQuote] SET[Amount] = " + Price + " WHERE ItemsQuoteID = " + ItemsQuote[i].ItemsQuoteID + "";
                                    SqlCommand comando3 = new SqlCommand(cadena3, conexion3);
                                    comando3.ExecuteNonQuery();
                                    conexion3.Close();
                                }
                            }
                        }
                    }
                }

                var url = "/Edit/" + quote.QuoteID;
                return RedirectToAction(url);
           
        }       
        
        public async Task<IActionResult> CreateItem(int QuoteID, int TipoMercancia, string Description, int Quantity, int Weight, int Amount)
        {
            SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
            conexion.Open();

            string cadena = "insert into [dbo].[FT_ItemsQuote]([QuoteID],[MerchandiseTypeID],[Description],[Quantity],[Weight],[Amount]) values" + " (" + QuoteID + "," + TipoMercancia + " ,'" + Description + "'," + Quantity + "," + Weight + "," + Amount + ")";
            SqlCommand comando = new SqlCommand(cadena, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            var url = "/Edit/" + QuoteID;
            return RedirectToAction(url);
        }

        public async Task<IActionResult> DeleteItem(int QuoteID, int ItemQuoteID)
        {
            SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
            conexion.Open();

            string cadena = "delete from[dbo].[FT_ItemsQuote] where ItemsQuoteID = "+ ItemQuoteID;
            SqlCommand comando = new SqlCommand(cadena, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            var url = "/Edit/" + QuoteID;
            return RedirectToAction(url);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var quote = await quoteService.GetById(id.Value);

            if (quote == null)
                return NotFound();

            var quoteDTO = mapper.Map<QuoteDTO>(quote);

            return View(quoteDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await quoteService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}