﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Contact = Angle.Models.Contact;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class ContactController : Controller
    {
        private readonly IContactService contactService;
        private readonly IMapper mapper;

        public ContactController(IContactService contactService,
            IMapper mapper)
        {
            this.contactService = contactService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await contactService.GetAll();
            ViewBag.Status = await contactService.GetStatus();
            var listContact = data.Select(x => mapper.Map<ContactDTO>(x)).ToList();
            return View(listContact);
        }



        public async Task<IActionResult> CreateAsync(int? id)
        {

            ViewBag.Status = await contactService.GetStatus();
            ViewBag.Customer = await contactService.GetCustomerById(id.Value);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ContactDTO contactDTO)
        {
            
            if (ModelState.IsValid)
            {

                var contact = mapper.Map<Contact>(contactDTO);

                contact = await contactService.Insert(contact);

                return RedirectToAction("Index", "Customer");
            }

            return View(contactDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var contact = await contactService.GetById(id.Value);

            if (contact == null)
                return NotFound();

            ViewBag.Status = await contactService.GetStatus();
            ViewBag.Customer = await contactService.GetCustomerById(id.Value);
            var contactDTO = mapper.Map<ContactDTO>(contact);
            return View(contactDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ContactDTO contactDTO)
        {
            if (ModelState.IsValid)
            {
                var contact = mapper.Map<Contact>(contactDTO);

                contact = await contactService.Update(contact);

                return RedirectToAction("Index", "Customer");
            }

            return View(contactDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var contact = await contactService.GetById(id.Value);

            if (contact == null)
                return NotFound();

            var contactDTO = mapper.Map<ContactDTO>(contact);

            return View(contactDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await contactService.Delete(id.Value);

            return RedirectToAction("Index", "Customer");
        }
    }
}