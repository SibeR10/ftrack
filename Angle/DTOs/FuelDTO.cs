﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class FuelDTO
    {
        public int FuelID { get; set; }

        [Required(ErrorMessage = "The Tipo is required")]
        [Display(Name = "Tipo")]
        public string FuelType { get; set; }

        [Required(ErrorMessage = "The Descripción is required")]
        [Display(Name = "Descripción")]
        public string FuelDescription { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

