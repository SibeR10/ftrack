﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ContractDTO
    {
        public int ContractID { get; set; }

        [Required(ErrorMessage = "The ContractName is required")]
        [Display(Name = "Tarifa regular")]
        public string ContractDescription { get; set; }

        [Display(Name = "Fecha Efectiva")]
        public DateTime EffectiveDate { get; set; }

        [Display(Name = "Fecha de expiración")]
        public DateTime ExpirationDate { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }

        [Required(ErrorMessage = "The ServiceType is required")]
        [Display(Name = "Tipo de servicio")]
        public int ServiceTypeID { get; set; }
    }
}

