﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Customer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomerID { get; set; }
		public int Document { get; set; }
		public string CompanyName { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int MainPhone { get; set; }
		public int SecondPhone { get; set; }
		public string Email { get; set; }
		public DateTime CreateDateTransaction { get; set; }
		public string CreateUserTransaction { get; set; }
		public DateTime UpdateDateTransaction { get; set; }
		public string UpdateUserTransaction { get; set; }
		public Division Division { get; set; }
		public int DivisionID { get; set; }
		
		public CustomerType CustomerType { get; set; }
		public int CustomerTypeID { get; set; }
		public Status Status { get; set; }
		public int StatusID { get; set; }
		public DocumentType DocumentType { get; set; }
		public int DocumentTypeID { get; set; }
		public Account Account { get; set; }
		public int AccountID { get; set; }
		public AccountType AccountType { get; set; }
		public int AccountTypeID { get; set; }
		public SalesPerson SalesPerson { get; set; }
		public int SalesPersonID { get; set; }

    }
}
