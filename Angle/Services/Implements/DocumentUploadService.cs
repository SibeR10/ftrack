﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Services.Implements
{
    public class DocumentUploadService : GenericService<DocumentUpload>, IDocumentUploadService
    {
        private IDocumentUploadRepository DocumentUploadRepository;

        public DocumentUploadService(IDocumentUploadRepository DocumentUploadRepository) : base(DocumentUploadRepository)
        {
            this.DocumentUploadRepository = DocumentUploadRepository;
        }

    }
}
