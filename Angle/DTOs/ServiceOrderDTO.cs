﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ServiceOrderDTO
    {
		public int ServiceOrderID { get; set; }

		[Display(Name = "Cotizacion")]
		public int QuoteID { get; set; }

		[Display(Name = "Fecha máxima de entrega")]

		public string DateDeliveryMax  { get; set; }

		[Display(Name = "Orden Servicio")]
		public string ServiceOrderDate { get; set; }

		[Display(Name = "Colecta")]
		public string CollectDate { get; set; }

		[Display(Name = "Transito")]
		public string InTransitDate { get; set; }

		[Display(Name = "Entrega")]
		public string DeliveryDate { get; set; }

		[Display(Name = "Estatus")]
		public int ServiceOrderStatusID { get; set; }

		[Display(Name = "Tipo de servicio")]
		public int ServiceTypeID { get; set; }

		[Display(Name = "Origen")]
		public int OriginID { get; set; }

		[Display(Name = "Destino")]
		public int DestinationID { get; set; }

		[Display(Name = "Dirección")]
		public string AddressQuote { get; set; }

		[Display(Name = "Referencia")]
		public string ReferenceQuote { get; set; }

		[Display(Name = "Vendedor")]
		public int SalesPersonID { get; set; }

		[Display(Name = "Cliente")]
		public int CustomerID { get; set; }

		[Display(Name = "Consignatario")]
		public int Remittee { get; set; }

		[Display(Name = "Teléfono")]
		public int MainPhone { get; set; }

		[Display(Name = "Correo electrónico")]
		public string Email { get; set; }

		[Display(Name = "Remitente")]
		public int Sender { get; set; }

		[Display(Name = "idCreate")]
		public int IdCreate { get; set; }

		[Display(Name = "MaxDays")]
		public int MaxDays { get; set; }

		[Display(Name = "Description Create")]
		public string DescriptionCreate { get; set; }
	}
}
		








