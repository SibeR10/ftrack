﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class QuoteService : GenericService<Quote>, IQuoteService
    {
        private IQuoteRepository quoteRepository;

        public QuoteService(IQuoteRepository quoteRepository) : base(quoteRepository)
        {
            this.quoteRepository = quoteRepository;
        }

        public async Task<IEnumerable<QuoteStatus>> GetQuoteStatus()
        {
            return await quoteRepository.GetQuoteStatus();
        }

        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await quoteRepository.GetGeoLevel1();
        }
        

        public async Task<IEnumerable<ChargesByQuote>> GetChargesByQuote()
        {
            return await quoteRepository.GetChargesByQuote();
        }
        

        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {
            return await quoteRepository.GetWarehouse();
        }
        
        public async Task<IEnumerable<Country>> GetCountry()
        {
            return await quoteRepository.GetCountry();
        }

        public async Task<IEnumerable<Customer>> GetCustomer()
        {
            return await quoteRepository.GetCustomer();
        }

        public async Task<IEnumerable<SalesPerson>> GetSalesPerson()
        {
            return await quoteRepository.GetSalesPerson();
        }

        public async Task<IEnumerable<Division>> GetDivision()
        {
            return await quoteRepository.GetDivision();
        }

        public async Task<IEnumerable<AddressType>> GetAddressType()
        {
            return await quoteRepository.GetAddressType();
        }

        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {
            return await quoteRepository.GetServiceType();
        }

        public async Task<IEnumerable<ItemsQuote>> GetItemsQuote()
        {
            return await quoteRepository.GetItemsQuote();
        }

        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {
            return await quoteRepository.GetMerchandiseType();
        }

        public async Task<IEnumerable<ContractSpecial>> GetContractSpecial()
        {
            return await quoteRepository.GetContractSpecial();
        }

        public async Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence()
        {
            return await quoteRepository.GetContractSpecialSequence();
        }

        public async Task<IEnumerable<ContractSequence>> GetContractSequence()
        {
            return await quoteRepository.GetContractSequence();
        }

        public async Task<IEnumerable<Person>> GetSender()
        {
            return await quoteRepository.GetSender();
        }

        public async Task<IEnumerable<Person>> GetRemittee()
        {
            return await quoteRepository.GetRemittee();
        }


    }
}
