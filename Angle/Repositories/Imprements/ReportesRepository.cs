﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class ReportesRepository : GenericRepository<Customer>, IReportesRepository
    {
        private readonly FTContext FTContext;

        public ReportesRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }

        public async Task<IEnumerable<Customer>> GetFilterCustomer(int division, int CustomerTypeID, int StatusID)
        {

            var listCustomer = await FTContext.Customer
                .Include(x => x.Division)
                .Where(x => x.DivisionID == division)
                .Where(x => x.CustomerTypeID == CustomerTypeID)
                .Where(x => x.StatusID == StatusID)
                .ToListAsync();

            return listCustomer;
        }
        public async Task<IEnumerable<Customer>> GetCustomer()
        {

            var listCustomer = await FTContext.Customer
                .ToListAsync();

            return listCustomer;
        }
        
    }
}
