﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using GeoLevel1 = Angle.Models.GeoLevel1;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class GeoLevel1Controller : Controller
    {
        private readonly IGeoLevel1Service geolevel1Service;
        private readonly IMapper mapper;

        public GeoLevel1Controller(IGeoLevel1Service geolevel1Service,
            IMapper mapper)
        {
            this.geolevel1Service = geolevel1Service;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await geolevel1Service.GetAll();
            ViewBag.Country = await geolevel1Service.GetCountry();
            var listGeoLevel1 = data.Select(x => mapper.Map<GeoLevel1DTO>(x)).ToList();
            return View(listGeoLevel1);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Country = await geolevel1Service.GetCountry();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(GeoLevel1DTO geolevel1DTO)
        {
            
            if (ModelState.IsValid)
            {

                var geolevel1 = mapper.Map<GeoLevel1>(geolevel1DTO);

                geolevel1 = await geolevel1Service.Insert(geolevel1);

                return RedirectToAction("Index");
            }

            return View(geolevel1DTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var geolevel1 = await geolevel1Service.GetById(id.Value);

            if (geolevel1 == null)
                return NotFound();

            ViewBag.Country = await geolevel1Service.GetCountry();
            var geolevel1DTO = mapper.Map<GeoLevel1DTO>(geolevel1);
            return View(geolevel1DTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(GeoLevel1DTO geolevel1DTO)
        {
            if (ModelState.IsValid)
            {
                var geolevel1 = mapper.Map<GeoLevel1>(geolevel1DTO);

                geolevel1 = await geolevel1Service.Update(geolevel1);

                return RedirectToAction("Index");
            }

            return View(geolevel1DTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var geolevel1 = await geolevel1Service.GetById(id.Value);

            if (geolevel1 == null)
                return NotFound();

            var geolevel1DTO = mapper.Map<GeoLevel1DTO>(geolevel1);

            return View(geolevel1DTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await geolevel1Service.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}