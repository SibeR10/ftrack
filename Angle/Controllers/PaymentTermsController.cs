﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using PaymentTerms = Angle.Models.PaymentTerms;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class PaymentTermsController : Controller
    {
        private readonly IPaymentTermsService paymenttermsService;
        private readonly IMapper mapper;

        public PaymentTermsController(IPaymentTermsService paymenttermsService,
            IMapper mapper)
        {
            this.paymenttermsService = paymenttermsService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await paymenttermsService.GetAll();
            ViewBag.Status = await paymenttermsService.GetStatus();
            var listPaymentTerms = data.Select(x => mapper.Map<PaymentTermsDTO>(x)).ToList();
            return View(listPaymentTerms);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await paymenttermsService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(PaymentTermsDTO paymenttermsDTO)
        {
            
            if (ModelState.IsValid)
            {

                var paymentterms = mapper.Map<PaymentTerms>(paymenttermsDTO);

                paymentterms = await paymenttermsService.Insert(paymentterms);

                return RedirectToAction("Index");
            }

            return View(paymenttermsDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var paymentterms = await paymenttermsService.GetById(id.Value);

            if (paymentterms == null)
                return NotFound();

            ViewBag.Status = await paymenttermsService.GetStatus();
            var paymenttermsDTO = mapper.Map<PaymentTermsDTO>(paymentterms);
            return View(paymenttermsDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(PaymentTermsDTO paymenttermsDTO)
        {
            if (ModelState.IsValid)
            {
                var paymentterms = mapper.Map<PaymentTerms>(paymenttermsDTO);

                paymentterms = await paymenttermsService.Update(paymentterms);

                return RedirectToAction("Index");
            }

            return View(paymenttermsDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var paymentterms = await paymenttermsService.GetById(id.Value);

            if (paymentterms == null)
                return NotFound();

            var paymenttermsDTO = mapper.Map<PaymentTermsDTO>(paymentterms);

            return View(paymenttermsDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await paymenttermsService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}