﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class DeliveryMode
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DeliveryModeID { get; set; }
        public int DeliveryModeName { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
