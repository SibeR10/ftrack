﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations.Schema;
namespace Angle.Models
{
    public class StatusPrueba
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string QuoteStatusName { get; set; }
        public string StatusColor { get; set; }
    }
}
