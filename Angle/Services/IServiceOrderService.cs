﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Services
{
    public interface IServiceOrderService : IGenericService<ServiceOrder>
    {
        Task<IEnumerable<ServiceOrderStatus>> GetStatus();
        Task<IEnumerable<DeliveryMode>> GetDeliveryMode();
        Task<IEnumerable<Quote>> GetQuote();
        Task<IEnumerable<Customer>> GetCustomer();
        Task<IEnumerable<ServiceType>> GetServiceType();
        Task<IEnumerable<SalesPerson>> GetSalesPerson();
        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();
        Task<IEnumerable<Warehouse>> GetWarehouse();
        Task<IEnumerable<ItemsQuote>> GetItemsQuote();
        Task<IEnumerable<MerchandiseType>> GetMerchandiseType();
        Task<IEnumerable<ItemsServiceOrder>> GetItemsServiceOrder();
        Task<IEnumerable<ContractSpecial>> GetContractSpecial();
        Task<IEnumerable<ContractSequence>> GetContractSequence();
        Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence();
        Task<IEnumerable<Person>> GetRemittee();
        Task<IEnumerable<Person>> GetSender();
        Task<IEnumerable<Person>> GetSenderInfo(int id);

    }
}