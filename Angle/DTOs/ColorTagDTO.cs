﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ColorTagDTO
    {
        public int ColorTagID { get; set; }

        [Display(Name = "Nombre del color")]
        public string ColorTagName { get; set; }

        [Display(Name = "Descripción del color")]
        public string ColorTagDescription { get; set; }

    }
}
