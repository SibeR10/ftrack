﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class StatusDTO
    {
        public int StatusID { get; set; }

        [Required(ErrorMessage = "The Status Name is required")]
        [Display(Name = "Descripción")]
        public string StatusName { get; set; }

        [Display(Name = "Color")]
        public string StatusColor { get; set; }

    }
}
