﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ContractSpecialSequenceService : GenericService<ContractSpecialSequence>, IContractSpecialSequenceService
    {
        private IContractSpecialSequenceRepository contractspecialsequenceRepository;

        public ContractSpecialSequenceService(IContractSpecialSequenceRepository contractspecialsequenceRepository) : base(contractspecialsequenceRepository)
        {
            this.contractspecialsequenceRepository = contractspecialsequenceRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await contractspecialsequenceRepository.GetStatus();
        }

        public async Task<IEnumerable<Customer>> GetCustomer()
        {
            return await contractspecialsequenceRepository.GetCustomer();
        }
        public async Task<IEnumerable<ContractSpecial>> GetContractSpecial()
        {
            return await contractspecialsequenceRepository.GetContractSpecial();
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await contractspecialsequenceRepository.GetGeoLevel1();
        }
        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {
            return await contractspecialsequenceRepository.GetWarehouse();
        }
        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {
            return await contractspecialsequenceRepository.GetMerchandiseType();
        }
        public async Task<IEnumerable<DeliveryMode>> GetDeliveryMode()
        {
            return await contractspecialsequenceRepository.GetDeliveryMode();
        }
        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {
            return await contractspecialsequenceRepository.GetServiceType();
        }
    }
}
