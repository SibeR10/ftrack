﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IAddressService : IGenericService<Address>
    {
        Task<IEnumerable<Status>> GetStatus();

        Task<IEnumerable<Status>> GetStatusByAddress(int id);
        Task<IEnumerable<Customer>> GetCustomer();

        Task<IEnumerable<Customer>> GetCustomerById(int id);

        Task<IEnumerable<GeoLevel1>> GetGeoLevel1();
        Task<IEnumerable<AddressType>> GetAddressType();

        Task<IEnumerable<AddressType>> GetAddressTypeById(int id);
    }
}
