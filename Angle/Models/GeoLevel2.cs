﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class GeoLevel2
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GeoLevel2ID { get; set; }
        public string GeoLevel2Name { get; set; }
        public string GeoLevel2Title { get; set; }
        public GeoLevel1 GeoLevel1 { get; set; }
        public int GeoLevel1ID { get; set; }
    }
}
