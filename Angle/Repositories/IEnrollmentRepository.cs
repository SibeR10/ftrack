﻿using Angle.Models;

namespace Angle.Repositories
{
    public interface IEnrollmentRepository : IGenericRepository<Enrollment>
    {
    }
}
