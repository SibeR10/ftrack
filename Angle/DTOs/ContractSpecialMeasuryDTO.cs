﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ContractSpecialMeasuryDTO
    {
        public int ContractSpecialMeasuryID { get; set; }

        [Display(Name = "ID Tarifa especial")]
        public int ContractSpecialSequenceID { get; set; }

        [Display(Name = "ID Unidad de medida")]
        public int MeasureUnitID { get; set; }

        [Display(Name = "Minimo")]
        public int Minimum { get; set; }

        [Display(Name = "Maximo")]
        public int Maximum { get; set; }

        [Display(Name = "Maximo")]
        public double Tariff { get; set; }
    }
}


	
	
	
