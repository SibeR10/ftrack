﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class PaymentTermsDTO
    {
        public int PaymentTermsID { get; set; }

        [Required(ErrorMessage = "The PaymentTermsName is required")]
        [Display(Name = "Términos de pago")]
        public string PaymentTermsDescription{ get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

