﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using ServiceType = Angle.Models.ServiceType;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class ServiceTypeController : Controller
    {
        private readonly IServiceTypeService servicetypeService;
        private readonly IMapper mapper;

        public ServiceTypeController(IServiceTypeService servicetypeService,
            IMapper mapper)
        {
            this.servicetypeService = servicetypeService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await servicetypeService.GetAll();
            ViewBag.Status = await servicetypeService.GetStatus();
            var listServiceType = data.Select(x => mapper.Map<ServiceTypeDTO>(x)).ToList();
            return View(listServiceType);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await servicetypeService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ServiceTypeDTO servicetypeDTO)
        {
            
            if (ModelState.IsValid)
            {

                var servicetype = mapper.Map<ServiceType>(servicetypeDTO);

                servicetype = await servicetypeService.Insert(servicetype);

                return RedirectToAction("Index");
            }

            return View(servicetypeDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var servicetype = await servicetypeService.GetById(id.Value);

            if (servicetype == null)
                return NotFound();

            ViewBag.Status = await servicetypeService.GetStatus();
            var servicetypeDTO = mapper.Map<ServiceTypeDTO>(servicetype);
            return View(servicetypeDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ServiceTypeDTO servicetypeDTO)
        {
            if (ModelState.IsValid)
            {
                var servicetype = mapper.Map<ServiceType>(servicetypeDTO);

                servicetype = await servicetypeService.Update(servicetype);

                return RedirectToAction("Index");
            }

            return View(servicetypeDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var servicetype = await servicetypeService.GetById(id.Value);

            if (servicetype == null)
                return NotFound();

            var servicetypeDTO = mapper.Map<ServiceTypeDTO>(servicetype);

            return View(servicetypeDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await servicetypeService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}