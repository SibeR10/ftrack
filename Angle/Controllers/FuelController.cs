﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using Fuel = Angle.Models.Fuel;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class FuelController : Controller
    {
        private readonly IFuelService fuelService;
        private readonly IMapper mapper;

        public FuelController(IFuelService fuelService,
            IMapper mapper)
        {
            this.fuelService = fuelService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await fuelService.GetAll();
            ViewBag.Status = await fuelService.GetStatus();
            var listFuel = data.Select(x => mapper.Map<FuelDTO>(x)).ToList();
            return View(listFuel);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await fuelService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(FuelDTO fuelDTO)
        {
            
            if (ModelState.IsValid)
            {

                var fuel = mapper.Map<Fuel>(fuelDTO);

                fuel = await fuelService.Insert(fuel);

                return RedirectToAction("Index");
            }

            return View(fuelDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var fuel = await fuelService.GetById(id.Value);

            if (fuel == null)
                return NotFound();

            ViewBag.Status = await fuelService.GetStatus();
            var fuelDTO = mapper.Map<FuelDTO>(fuel);
            return View(fuelDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(FuelDTO fuelDTO)
        {
            if (ModelState.IsValid)
            {
                var fuel = mapper.Map<Fuel>(fuelDTO);

                fuel = await fuelService.Update(fuel);

                return RedirectToAction("Index");
            }

            return View(fuelDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var fuel = await fuelService.GetById(id.Value);

            if (fuel == null)
                return NotFound();

            var fuelDTO = mapper.Map<FuelDTO>(fuel);

            return View(fuelDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await fuelService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}