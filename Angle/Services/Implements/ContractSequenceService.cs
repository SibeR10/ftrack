﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ContractSequenceService : GenericService<ContractSequence>, IContractSequenceService
    {
        private IContractSequenceRepository dontractsequenceRepository;

        public ContractSequenceService(IContractSequenceRepository dontractsequenceRepository) : base(dontractsequenceRepository)
        {
            this.dontractsequenceRepository = dontractsequenceRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await dontractsequenceRepository.GetStatus();
        }
        public async Task<IEnumerable<Contract>> GetContract()
        {
            return await dontractsequenceRepository.GetContract();
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await dontractsequenceRepository.GetGeoLevel1();
        }
        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {
            return await dontractsequenceRepository.GetWarehouse();
        }
        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {
            return await dontractsequenceRepository.GetMerchandiseType();
        }
        public async Task<IEnumerable<DeliveryMode>> GetDeliveryMode()
        {
            return await dontractsequenceRepository.GetDeliveryMode();
        }
        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {
            return await dontractsequenceRepository.GetServiceType();
        }
          
    }
}
