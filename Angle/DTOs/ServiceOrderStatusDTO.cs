﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ServiceOrderStatusDTO
    {
        public int ServiceOrderStatusID { get; set; }

        [Required(ErrorMessage = "The ServiceOrderStatus Name is required")]
        [Display(Name = "Descripción")]
        public string ServiceOrderStatusName { get; set; }

        [Display(Name = "Color")]
        public string StatusColor { get; set; }

    }
}
