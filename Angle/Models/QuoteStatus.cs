﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations.Schema;
namespace Angle.Models
{
    public class QuoteStatus
    {
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuoteStatusID { get; set; }
        public string QuoteStatusName { get; set; }
        public string StatusColor { get; set; }


    }
}
