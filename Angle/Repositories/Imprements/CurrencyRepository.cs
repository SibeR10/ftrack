﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class CurrencyRepository : GenericRepository<Currency>, ICurrencyRepository
    {
        private readonly FTContext FTContext;

        public CurrencyRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
    }
}
