﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class DeliveryModeDTO
    {
        public int DeliveryModeID { get; set; }

        [Required(ErrorMessage = "The DeliveryModeName is required")]
        [Display(Name = "Cantidad máxima de dias")]
        public int DeliveryModeName { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

