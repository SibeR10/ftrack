﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class AddressType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AddressTypeID { get; set; }
        public string AddressTypeName { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
