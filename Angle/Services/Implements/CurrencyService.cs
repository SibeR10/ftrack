﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Services.Implements
{
    public class CurrencyService : GenericService<Currency>, ICurrencyService
    {
        private ICurrencyRepository CurrencyRepository;

        public CurrencyService(ICurrencyRepository CurrencyRepository) : base(CurrencyRepository)
        {
            this.CurrencyRepository = CurrencyRepository;
        }

    }
}
