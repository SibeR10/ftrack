﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class TransportTypeDTO
    {
        public int TransportTypeID { get; set; }

        [Required(ErrorMessage = "The TransportTypeName is required")]
        [Display(Name = "Tipo de transporte")]
        public string TransportTypeName { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

