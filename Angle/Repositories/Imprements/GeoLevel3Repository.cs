﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class GeoLevel3Repository : GenericRepository<GeoLevel3>, IGeoLevel3Repository
    {
        private readonly FTContext FTContext;

        public GeoLevel3Repository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<GeoLevel2>> GetGeoLevel2()
        {

            var listGeoLevel2 = await FTContext.GeoLevel2.ToListAsync();

            return listGeoLevel2;
        }

        public async Task<IEnumerable<GeoLevel2>> GetGeoLevel2ByGeoLevel3(int id)
        {

            var listGeoLevel2ByGeoLevel3 = await FTContext.GeoLevel3
                .Include(x => x.GeoLevel2)
                .Where(x => x.GeoLevel2ID == id)
                .Select(x => x.GeoLevel2)
                .ToListAsync();

            return listGeoLevel2ByGeoLevel3;
        }

        
    }
}
