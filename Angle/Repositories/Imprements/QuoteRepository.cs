﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class QuoteRepository : GenericRepository<Quote>, IQuoteRepository
    {
        private readonly FTContext FTContext;

        public QuoteRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {

            var listGeoLevel1 = await FTContext.GeoLevel1.ToListAsync();

            return listGeoLevel1;
        }
        public async Task<IEnumerable<QuoteStatus>> GetQuoteStatus()
        {

            var listQuoteStatus = await FTContext.QuoteStatus.ToListAsync();

            return listQuoteStatus;
        }
        public async Task<IEnumerable<Customer>> GetCustomer()
        {

            var listCustomer = await FTContext.Customer.ToListAsync();

            return listCustomer;
        }
        public async Task<IEnumerable<SalesPerson>> GetSalesPerson()
        {

            var listSalesPerson = await FTContext.SalesPerson.ToListAsync();

            return listSalesPerson;
        }
        public async Task<IEnumerable<Division>> GetDivision()
        {

            var listDivision = await FTContext.Division.ToListAsync();

            return listDivision;
        }
        public async Task<IEnumerable<AddressType>> GetAddressType()
        {

            var listAddressType = await FTContext.AddressType.ToListAsync();

            return listAddressType;
        }
        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {

            var listWarehouse = await FTContext.Warehouse.ToListAsync();

            return listWarehouse;
        }
        public async Task<IEnumerable<Country>> GetCountry()
        {

            var listCountry = await FTContext.Country.ToListAsync();

            return listCountry;
        }
        public async Task<IEnumerable<ChargesByQuote>> GetChargesByQuote()
        {

            var listChargesByQuote = await FTContext.ChargesByQuote.ToListAsync();

            return listChargesByQuote;
        }
        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {

            var listServiceType = await FTContext.ServiceType.ToListAsync();

            return listServiceType;
        }
        public async Task<IEnumerable<ItemsQuote>> GetItemsQuote()
        {

            var listItemsQuote = await FTContext.ItemsQuote.ToListAsync();

            return listItemsQuote;
        }
        public async Task<IEnumerable<MerchandiseType>> GetMerchandiseType()
        {

            var listMerchandiseType = await FTContext.MerchandiseType.ToListAsync();

            return listMerchandiseType;
        }
        public async Task<IEnumerable<ContractSpecial>> GetContractSpecial()
        {

            var listContractSpecial = await FTContext.ContractSpecial.ToListAsync();

            return listContractSpecial;
        }
        public async Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence()
        {

            var listContractSpecialSequence = await FTContext.ContractSpecialSequence.ToListAsync();

            return listContractSpecialSequence;
        }
        public async Task<IEnumerable<ContractSequence>> GetContractSequence()
        {

            var listContractSequence = await FTContext.ContractSequence.ToListAsync();

            return listContractSequence;
        }
        public async Task<IEnumerable<Person>> GetSender()
        {

            var listPerson = await FTContext.Person.ToListAsync();

            return listPerson;
        }
        public async Task<IEnumerable<Person>> GetRemittee()
        {

            var listPerson = await FTContext.Person.ToListAsync();

            return listPerson;
        }

        

    }
}
