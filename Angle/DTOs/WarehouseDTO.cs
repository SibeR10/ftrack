﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class WarehouseDTO
    {
        public int WarehouseID { get; set; }

        [Required(ErrorMessage = "The WarehouseName is required")]
        [Display(Name = "Nombre del almacen")]
        public string WarehouseName { get; set; }

        [Required(ErrorMessage = "The WarehouseDescription is required")]
        [Display(Name = "Descripción del almacen")]
        public string WarehouseDescription { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }

        [Required(ErrorMessage = "The DivisionID is required")]
        [Display(Name = "Division")]
        public int DivisionID { get; set; }
    }
}