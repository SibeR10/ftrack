﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IContractMeasuryRepository : IGenericRepository<ContractMeasury>
    {
        Task<IEnumerable<ContractMeasury>> GetContractMeasury();


    }
}
