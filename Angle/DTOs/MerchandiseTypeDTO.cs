﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class MerchandiseTypeDTO
    {
        public int MerchandiseTypeID { get; set; }

        [Required(ErrorMessage = "The MerchandiseTypeName is required")]
        [Display(Name = "Tipo de mercancia")]
        public string MerchandiseTypeName { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

