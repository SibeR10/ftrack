﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations.Schema;
namespace Angle.Models
{
    public class Currency
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CurrencyID { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyDescription { get; set; }
        public int CurrencyChangeUSD { get; set; }

    }
}
