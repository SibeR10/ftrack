﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ServiceClassService : GenericService<ServiceClass>, IServiceClassService
    {
        private IServiceClassRepository serviceclassRepository;

        public ServiceClassService(IServiceClassRepository serviceclassRepository) : base(serviceclassRepository)
        {
            this.serviceclassRepository = serviceclassRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await serviceclassRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByServiceClass(int id)
        {
            return await serviceclassRepository.GetStatusByServiceClass(id);
        }

    }
}
