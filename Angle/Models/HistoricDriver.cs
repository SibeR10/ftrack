﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class HistoricDriver
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HistoricDriverID { get; set; }
		public DateTime DateStartVehicle { get; set; }
		public DateTime DateEndVehicle { get; set; }
		public Status Status { get; set; }
		public int StatusID { get; set; }
		public Driver Driver { get; set; }
		public int DriverID { get; set; }
		public Vehicles Vehicles { get; set; }
		public int VehiclesID { get; set; }
	}
}