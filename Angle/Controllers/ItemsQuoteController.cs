﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using ItemsQuote = Angle.Models.ItemsQuote;
using Angle.Services.Implements;
using System.Data;
using Microsoft.Data.SqlClient;
using System;

namespace Angle.Controllers
{
    public class ItemsQuoteController : Controller
    {
        private readonly IItemsQuoteService quoteService;
        private readonly IServiceOrderService ServiceOrderService;
        private readonly IMapper mapper;

        public ItemsQuoteController(IItemsQuoteService quoteService,
            IMapper mapper)
        {
            this.quoteService = quoteService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await quoteService.GetAll();
            ViewBag.Customer = await quoteService.GetCustomer();
            ViewBag.SalesPerson = await quoteService.GetSalesPerson();
            var listItemsQuote = data.Select(x => mapper.Map<ItemsQuoteDTO>(x)).ToList();
            return View(listItemsQuote);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.ServiceType = await quoteService.GetServiceType();
            ViewBag.Customer = await quoteService.GetCustomer();
            ViewBag.SalesPerson = await quoteService.GetSalesPerson();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ItemsQuoteDTO quoteDTO)
        {
            
            if (ModelState.IsValid)
            {

                var quote = mapper.Map<ItemsQuote>(quoteDTO);

                quote = await quoteService.Insert(quote);

                var url = "/Edit/" + quote.ItemsQuoteID;
                return RedirectToAction(url);
            }

            return View(quoteDTO);
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var quote = await quoteService.GetById(id.Value);

            if (quote == null)
                return NotFound();

            ViewBag.ServiceType = await quoteService.GetServiceType();
            ViewBag.Customer = await quoteService.GetCustomer();
            ViewBag.SalesPerson = await quoteService.GetSalesPerson();
            ViewBag.GeoLevel1 = await quoteService.GetGeoLevel1();
            ViewBag.Warehouse = await quoteService.GetWarehouse();
            var quoteDTO = mapper.Map<ItemsQuoteDTO>(quote);
            return View(quoteDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ItemsQuoteDTO quoteDTO)
        {
            if (ModelState.IsValid)
            {
                var quote = mapper.Map<ItemsQuote>(quoteDTO);
                quote = await quoteService.Update(quote);

                var url = "/Edit/" + quote.ItemsQuoteID;
                return RedirectToAction(url);
            }

            return View(quoteDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var quote = await quoteService.GetById(id.Value);

            if (quote == null)
                return NotFound();

            var quoteDTO = mapper.Map<ItemsQuoteDTO>(quote);

            return View(quoteDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await quoteService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}