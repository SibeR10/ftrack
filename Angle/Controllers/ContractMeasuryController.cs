﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using ContractMeasury = Angle.Models.ContractMeasury;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class ContractMeasuryController : Controller
    {
        private readonly IContractMeasuryService contractmeasuryService;
        private readonly IMapper mapper;

        public ContractMeasuryController(IContractMeasuryService contractmeasuryService,
            IMapper mapper)
        {
            this.contractmeasuryService = contractmeasuryService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await contractmeasuryService.GetAll();
            //ViewBag.MeasureUnit = await contractmeasuryService.GetMeasureUnit();
            var listContractMeasury = data.Select(x => mapper.Map<ContractMeasuryDTO>(x)).ToList();
            return View(listContractMeasury);
        }



        public async Task<IActionResult> CreateAsync()
        {

           // ViewBag.MeasureUnit = await contractmeasuryService.GetMeasureUnit();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ContractMeasuryDTO contractmeasuryDTO)
        {
            
            if (ModelState.IsValid)
            {

                var contractmeasury = mapper.Map<ContractMeasury>(contractmeasuryDTO);

                contractmeasury = await contractmeasuryService.Insert(contractmeasury);

                return RedirectToAction("Index");
            }

            return View(contractmeasuryDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var contractmeasury = await contractmeasuryService.GetById(id.Value);

            if (contractmeasury == null)
                return NotFound();

           // ViewBag.MeasureUnit = await contractmeasuryService.GetMeasureUnit();
            var contractmeasuryDTO = mapper.Map<ContractMeasuryDTO>(contractmeasury);
            return View(contractmeasuryDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ContractMeasuryDTO contractmeasuryDTO)
        {
            if (ModelState.IsValid)
            {
                var contractmeasury = mapper.Map<ContractMeasury>(contractmeasuryDTO);

                contractmeasury = await contractmeasuryService.Update(contractmeasury);

                return RedirectToAction("Index");
            }

            return View(contractmeasuryDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var contractmeasury = await contractmeasuryService.GetById(id.Value);

            if (contractmeasury == null)
                return NotFound();

            var contractmeasuryDTO = mapper.Map<ContractMeasuryDTO>(contractmeasury);

            return View(contractmeasuryDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await contractmeasuryService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}