﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using DriverType = Angle.Models.DriverType;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class DriverTypeController : Controller
    {
        private readonly IDriverTypeService drivertypeService;
        private readonly IMapper mapper;

        public DriverTypeController(IDriverTypeService drivertypeService,
            IMapper mapper)
        {
            this.drivertypeService = drivertypeService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await drivertypeService.GetAll();
            ViewBag.Status = await drivertypeService.GetStatus();
            var listDriverType = data.Select(x => mapper.Map<DriverTypeDTO>(x)).ToList();
            return View(listDriverType);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await drivertypeService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DriverTypeDTO drivertypeDTO)
        {
            
            if (ModelState.IsValid)
            {

                var drivertype = mapper.Map<DriverType>(drivertypeDTO);

                drivertype = await drivertypeService.Insert(drivertype);

                return RedirectToAction("Index");
            }

            return View(drivertypeDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var drivertype = await drivertypeService.GetById(id.Value);

            if (drivertype == null)
                return NotFound();

            ViewBag.Status = await drivertypeService.GetStatus();
            var drivertypeDTO = mapper.Map<DriverTypeDTO>(drivertype);
            return View(drivertypeDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(DriverTypeDTO drivertypeDTO)
        {
            if (ModelState.IsValid)
            {
                var drivertype = mapper.Map<DriverType>(drivertypeDTO);

                drivertype = await drivertypeService.Update(drivertype);

                return RedirectToAction("Index");
            }

            return View(drivertypeDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var drivertype = await drivertypeService.GetById(id.Value);

            if (drivertype == null)
                return NotFound();

            var drivertypeDTO = mapper.Map<DriverTypeDTO>(drivertype);

            return View(drivertypeDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await drivertypeService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}