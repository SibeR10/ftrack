﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class ItemsQuoteService : GenericService<ItemsQuote>, IItemsQuoteService
    {
        private IItemsQuoteRepository itemsquoteRepository;

        public ItemsQuoteService(IItemsQuoteRepository itemsquoteRepository) : base(itemsquoteRepository)
        {
            this.itemsquoteRepository = itemsquoteRepository;
        }


        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await itemsquoteRepository.GetGeoLevel1();
        }
        


        public async Task<IEnumerable<Warehouse>> GetWarehouse()
        {
            return await itemsquoteRepository.GetWarehouse();
        }
        
        public async Task<IEnumerable<Country>> GetCountry()
        {
            return await itemsquoteRepository.GetCountry();
        }

        public async Task<IEnumerable<Customer>> GetCustomer()
        {
            return await itemsquoteRepository.GetCustomer();
        }

        public async Task<IEnumerable<SalesPerson>> GetSalesPerson()
        {
            return await itemsquoteRepository.GetSalesPerson();
        }

        public async Task<IEnumerable<Division>> GetDivision()
        {
            return await itemsquoteRepository.GetDivision();
        }

        public async Task<IEnumerable<AddressType>> GetAddressType()
        {
            return await itemsquoteRepository.GetAddressType();
        }

        public async Task<IEnumerable<ServiceType>> GetServiceType()
        {
            return await itemsquoteRepository.GetServiceType();
        }


    }
}
