﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class DivisionRepository : GenericRepository<Division>, IDivisionRepository
    {
        private readonly FTContext FTContext;

        public DivisionRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<Status>> GetStatus()
        {

            var listStatus = await FTContext.Status.ToListAsync();

            return listStatus;
        }

        public async Task<IEnumerable<Status>> GetStatusByDivision(int id)
        {

            var listStatusByDivision = await FTContext.Division
                .Include(x => x.Status)
                .Where(x => x.StatusID == id)
                .Select(x => x.Status)
                .ToListAsync();

            return listStatusByDivision;
        }

        
    }
}
