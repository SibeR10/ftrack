﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class GeoLevel1
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GeoLevel1ID { get; set; }
        public string GeoLevel1Name { get; set; }
        public string GeoLevel1Title { get; set; }
        public Country Country { get; set; }
        public int CountryID { get; set; }
        public int GeoLevel1P { get; set; }
    }
}
