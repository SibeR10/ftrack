﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ServiceClassDTO
    {
        public int ServiceClassID { get; set; }

        [Required(ErrorMessage = "The ServiceClassName is required")]
        [Display(Name = "Clase de servicio")]
        public string ServiceClassName { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

