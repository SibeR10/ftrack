﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class DocumentUploadRepository : GenericRepository<DocumentUpload>, IDocumentUploadRepository
    {
        private readonly FTContext FTContext;

        public DocumentUploadRepository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
    }
}
