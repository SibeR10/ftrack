﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using DeliveryMode = Angle.Models.DeliveryMode;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class DeliveryModeController : Controller
    {
        private readonly IDeliveryModeService deliverymodeService;
        private readonly IMapper mapper;

        public DeliveryModeController(IDeliveryModeService deliverymodeService,
            IMapper mapper)
        {
            this.deliverymodeService = deliverymodeService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await deliverymodeService.GetAll();
            ViewBag.Status = await deliverymodeService.GetStatus();
            var listDeliveryMode = data.Select(x => mapper.Map<DeliveryModeDTO>(x)).ToList();
            return View(listDeliveryMode);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await deliverymodeService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DeliveryModeDTO deliverymodeDTO)
        {
            
            if (ModelState.IsValid)
            {

                var deliverymode = mapper.Map<DeliveryMode>(deliverymodeDTO);

                deliverymode = await deliverymodeService.Insert(deliverymode);

                return RedirectToAction("Index");
            }

            return View(deliverymodeDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var deliverymode = await deliverymodeService.GetById(id.Value);

            if (deliverymode == null)
                return NotFound();

            ViewBag.Status = await deliverymodeService.GetStatus();
            var deliverymodeDTO = mapper.Map<DeliveryModeDTO>(deliverymode);
            return View(deliverymodeDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(DeliveryModeDTO deliverymodeDTO)
        {
            if (ModelState.IsValid)
            {
                var deliverymode = mapper.Map<DeliveryMode>(deliverymodeDTO);

                deliverymode = await deliverymodeService.Update(deliverymode);

                return RedirectToAction("Index");
            }

            return View(deliverymodeDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var deliverymode = await deliverymodeService.GetById(id.Value);

            if (deliverymode == null)
                return NotFound();

            var deliverymodeDTO = mapper.Map<DeliveryModeDTO>(deliverymode);

            return View(deliverymodeDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await deliverymodeService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}