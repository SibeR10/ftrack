﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using GeoLevel2 = Angle.Models.GeoLevel2;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class GeoLevel2Controller : Controller
    {
        private readonly IGeoLevel2Service geolevel2Service;
        private readonly IMapper mapper;

        public GeoLevel2Controller(IGeoLevel2Service geolevel2Service,
            IMapper mapper)
        {
            this.geolevel2Service = geolevel2Service;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await geolevel2Service.GetAll();
            ViewBag.GeoLevel1 = await geolevel2Service.GetGeoLevel1();
            var listGeoLevel2 = data.Select(x => mapper.Map<GeoLevel2DTO>(x)).ToList();
            return View(listGeoLevel2);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.GeoLevel1 = await geolevel2Service.GetGeoLevel1();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(GeoLevel2DTO geolevel2DTO)
        {
            
            if (ModelState.IsValid)
            {

                var geolevel2 = mapper.Map<GeoLevel2>(geolevel2DTO);

                geolevel2 = await geolevel2Service.Insert(geolevel2);

                return RedirectToAction("Index");
            }

            return View(geolevel2DTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var geolevel2 = await geolevel2Service.GetById(id.Value);

            if (geolevel2 == null)
                return NotFound();

            ViewBag.GeoLevel1 = await geolevel2Service.GetGeoLevel1();
            var geolevel2DTO = mapper.Map<GeoLevel2DTO>(geolevel2);
            return View(geolevel2DTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(GeoLevel2DTO geolevel2DTO)
        {
            if (ModelState.IsValid)
            {
                var geolevel2 = mapper.Map<GeoLevel2>(geolevel2DTO);

                geolevel2 = await geolevel2Service.Update(geolevel2);

                return RedirectToAction("Index");
            }

            return View(geolevel2DTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var geolevel2 = await geolevel2Service.GetById(id.Value);

            if (geolevel2 == null)
                return NotFound();

            var geolevel2DTO = mapper.Map<GeoLevel2DTO>(geolevel2);

            return View(geolevel2DTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await geolevel2Service.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}