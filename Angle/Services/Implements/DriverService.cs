﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class DriverService : GenericService<Driver>, IDriverService
    {
        private IDriverRepository driverRepository;

        public DriverService(IDriverRepository driverRepository) : base(driverRepository)
        {
            this.driverRepository = driverRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await driverRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByDriver(int id)
        {
            return await driverRepository.GetStatusByDriver(id);
        }

        public async Task<IEnumerable<Division>> GetDivision()
        {
            return await driverRepository.GetDivision();
        }

        public async Task<IEnumerable<Division>> GetDivisionByDriver(int id)
        {
            return await driverRepository.GetDivisionByDriver(id);
        }

        public async Task<IEnumerable<DriverType>> GetDriverType()
        {
            return await driverRepository.GetDriverType();
        }

        public async Task<IEnumerable<DriverType>> GetDriverTypeByDriver(int id)
        {
            return await driverRepository.GetDriverTypeByDriver(id);
        }

        public async Task<IEnumerable<DocumentType>> GetDocumentType()
        {
            return await driverRepository.GetDocumentType();
        }

        public async Task<IEnumerable<DocumentType>> GetDocumentTypeByDriver(int id)
        {
            return await driverRepository.GetDocumentTypeByDriver(id);
        }

        
    }
}
