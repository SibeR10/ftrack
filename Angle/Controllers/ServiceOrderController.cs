﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using System.Data;
using Microsoft.Data.SqlClient;
using ServiceOrder = Angle.Models.ServiceOrder;
using Angle.Services.Implements;
using System;

namespace Angle.Controllers
{
    public class ServiceOrderController : Controller
    {
        private readonly IServiceOrderService serviceorderService;
        private readonly IMapper mapper;

        public ServiceOrderController(IServiceOrderService serviceorderService,
            IMapper mapper)
        {
            this.serviceorderService = serviceorderService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await serviceorderService.GetAll();
            ViewBag.Status = await serviceorderService.GetStatus();
            ViewBag.Quote = await serviceorderService.GetQuote();
            ViewBag.Customer = await serviceorderService.GetCustomer();
            ViewBag.SalesPerson = await serviceorderService.GetSalesPerson();
            var listServiceOrder = data.Select(x => mapper.Map<ServiceOrderDTO>(x)).ToList();
            return View(listServiceOrder);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await serviceorderService.GetStatus();
            ViewBag.ServiceType = await serviceorderService.GetServiceType();
            ViewBag.SalesPerson = await serviceorderService.GetSalesPerson();
            ViewBag.Customer = await serviceorderService.GetCustomer();
            ViewBag.Sender = await serviceorderService.GetSender();
            ViewBag.Remittee = await serviceorderService.GetRemittee();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ServiceOrderDTO serviceorderDTO)
        {
            
            if (ModelState.IsValid)
            {

                var serviceorder = mapper.Map<ServiceOrder>(serviceorderDTO);

                serviceorder = await serviceorderService.Insert(serviceorder);


                var url = "/Edit/" + serviceorder.ServiceOrderID;
                return RedirectToAction(url);
            }

            return View(serviceorderDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var serviceorder = await serviceorderService.GetById(id.Value);

            if (serviceorder == null)
                return NotFound();

            ViewBag.Status = await serviceorderService.GetStatus();
            ViewBag.DeliveryMode = await serviceorderService.GetDeliveryMode();
            ViewBag.ServiceType = await serviceorderService.GetServiceType();
            ViewBag.SalesPerson = await serviceorderService.GetSalesPerson();
            ViewBag.Customer = await serviceorderService.GetCustomer();
            ViewBag.GeoLevel1 = await serviceorderService.GetGeoLevel1();
            ViewBag.Warehouse = await serviceorderService.GetWarehouse();
            ViewBag.ItemsQuote = await serviceorderService.GetItemsQuote();
            ViewBag.MerchandiseType = await serviceorderService.GetMerchandiseType();
            ViewBag.ItemsServiceOrder = await serviceorderService.GetItemsServiceOrder();
            ViewBag.ContractSpecial = await serviceorderService.GetContractSpecial();
            ViewBag.ContractSequence = await serviceorderService.GetContractSequence();
            ViewBag.ContractSpecialSequence = await serviceorderService.GetContractSpecialSequence();
            ViewBag.Sender = await serviceorderService.GetSender();
            ViewBag.Remittee = await serviceorderService.GetRemittee();
            var serviceorderDTO = mapper.Map<ServiceOrderDTO>(serviceorder);
            return View(serviceorderDTO);
        }



        [HttpPost]
        public async Task<IActionResult> Edit(ServiceOrderDTO serviceorderDTO)
        {
            if (ModelState.IsValid)
            {
                var serviceorder = mapper.Map<ServiceOrder>(serviceorderDTO);

                serviceorder = await serviceorderService.Update(serviceorder);

                return RedirectToAction("Index");
            }

            return View(serviceorderDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var serviceorder = await serviceorderService.GetById(id.Value);

            if (serviceorder == null)
                return NotFound();

            var serviceorderDTO = mapper.Map<ServiceOrderDTO>(serviceorder);

            return View(serviceorderDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await serviceorderService.Delete(id.Value);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> CreateItem(int QuoteID, int ServiceOrderID, int TipoMercancia, string Description, int Quantity, int Weight, int Amount)
        {
            if(QuoteID != 0)
            {
                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();

                string cadena = "insert into [dbo].[FT_ItemsQuote]([QuoteID],[MerchandiseTypeID],[Description],[Quantity],[Weight],[Amount]) values" + " (" + QuoteID + "," + TipoMercancia + " ,'" + Description + "'," + Quantity + "," + Weight + "," + Amount + ")";
                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();
                conexion.Close();
            }
            else
            {
                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();
                string cadena = "insert into [dbo].[FT_ItemsServiceOrder]([ServiceOrderID],[MerchandiseTypeID],[Description],[Quantity],[Weight],[Amount]) values" + " (" + ServiceOrderID + "," + TipoMercancia + " ,'" + Description + "'," + Quantity + "," + Weight + "," + Amount + ")";
                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();
                conexion.Close();
            }



            return RedirectToAction("Index");
        }

        public async Task<IActionResult> CreatePerson(string PersonName, int PersonDocument, int PersonTelephone, string PersonEmail)
        {
           
                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();

                string cadena = "INSERT INTO [dbo].[FT_Person]([PersonName],[PersonDocument],[PersonTelepone],[PersonEmail])VALUES('"+PersonName+"',"+PersonDocument+","+ PersonTelephone + ",'" + PersonEmail + "')";
                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();
                conexion.Close();

                var url = "/Index/";
                return RedirectToAction(url);
        }

        public async Task<IActionResult> DeleteItem(int ServiceOrderID, int ItemServiceOrderID, int QuoteID)
        {

            
                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();

                string cadena = "delete from[dbo].[FT_ItemsQuote] where ItemsQuoteID = " + ItemServiceOrderID;
                SqlCommand comando = new SqlCommand(cadena, conexion);
                comando.ExecuteNonQuery();

                string cadena2 = "delete from[dbo].[FT_ItemsServiceOrder] where ItemsServiceOrderID = " + ItemServiceOrderID;
                SqlCommand comando2 = new SqlCommand(cadena2, conexion);
                comando2.ExecuteNonQuery();
                conexion.Close();
            
            var url = "/Index/";
            return RedirectToAction(url);
        }
        public async Task<IActionResult> Import(IFormFile file)
        {
            var list = new List<ServiceOrder>();
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowcount = worksheet.Dimension.Rows;
                    for (int row = 2; row <= rowcount; row++)
                    {
                        list.Add(new ServiceOrder
                        {
                           /* LicensePlateCar = worksheet.Cells[row, 1].Value.ToString().Trim(),
                            Enrollment = worksheet.Cells[row, 2].Value.ToString().Trim(),
                            ColourCar = worksheet.Cells[row, 3].Value.ToString().Trim(),
                            Equipment = worksheet.Cells[row, 4].Value.ToString().Trim(),
                            ModelCar = worksheet.Cells[row, 5].Value.ToString().Trim(),
                            YearCar = (int)(double)worksheet.Cells[row, 6].Value,
                            ChassisCar = worksheet.Cells[row, 7].Value.ToString().Trim(),
                            InsuranceCar = worksheet.Cells[row, 8].Value.ToString().Trim(),
                            ExpirationInsuranceCar = (DateTime)worksheet.Cells[row, 9].Value,
                            DivisionID = (int)(double)worksheet.Cells[row, 10].Value,
                            BrandID = (int)(double)worksheet.Cells[row, 11].Value,
                            StatusID = (int)(double)worksheet.Cells[row, 12].Value,
                            DriverID = (int)(double)worksheet.Cells[row, 13].Value,*/
                        });
                    }
                }
            }
           
            list.ForEach(p =>
            {

                SqlConnection conexion = new SqlConnection("Server=EC2AMAZ-9EP7VB3;Database=FTrackV2;User Id=sa; Password=asdf@1245");
                conexion.Open();
            
            });


            return RedirectToAction("Index");
        }
    }
}