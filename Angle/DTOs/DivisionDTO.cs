﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class DivisionDTO
    {
        public int DivisionID { get; set; }

        [Required(ErrorMessage = "The DivisionName is required")]
        [Display(Name = "Descripción")]
        public string DivisionName { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

