﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class Travel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TravelID { get; set; }
        public DateTime DateTravel { get; set; }
        public Vehicles Vehicles { get; set; }
        public int VehiclesID { get; set; }
        public Driver Driver { get; set; }
        public int DriverID { get; set; }
        public int OriginID { get; set; }
        public int DestinationID { get; set; }
        public int StatusID { get; set; }
    }
}

