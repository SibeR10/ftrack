﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class CountryDTO
    {
        public int CountryID { get; set; }

        [Required(ErrorMessage = "The Country Name is required")]
        [Display(Name = "País")]
        public string CountryName { get; set; }

    }
}
