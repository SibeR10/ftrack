﻿using Angle.Data;
using Angle.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angle.Repositories.Implements
{
    public class GeoLevel2Repository : GenericRepository<GeoLevel2>, IGeoLevel2Repository
    {
        private readonly FTContext FTContext;

        public GeoLevel2Repository(FTContext FTContext) : base(FTContext)
        {
            this.FTContext = FTContext;
        }
        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {

            var listGeoLevel1 = await FTContext.GeoLevel1.ToListAsync();

            return listGeoLevel1;
        }

        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1ByGeoLevel2(int id)
        {

            var listGeoLevel1ByGeoLevel2 = await FTContext.GeoLevel2
                .Include(x => x.GeoLevel1)
                .Where(x => x.GeoLevel1ID == id)
                .Select(x => x.GeoLevel1)
                .ToListAsync();

            return listGeoLevel1ByGeoLevel2;
        }

        
    }
}
