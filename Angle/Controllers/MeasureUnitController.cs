﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using MeasureUnit = Angle.Models.MeasureUnit;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class MeasureUnitController : Controller
    {
        private readonly IMeasureUnitService measureunitService;
        private readonly IMapper mapper;

        public MeasureUnitController(IMeasureUnitService measureunitService,
            IMapper mapper)
        {
            this.measureunitService = measureunitService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await measureunitService.GetAll();
            ViewBag.Status = await measureunitService.GetStatus();
            var listMeasureUnit = data.Select(x => mapper.Map<MeasureUnitDTO>(x)).ToList();
            return View(listMeasureUnit);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await measureunitService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(MeasureUnitDTO measureunitDTO)
        {
            
            if (ModelState.IsValid)
            {

                var measureunit = mapper.Map<MeasureUnit>(measureunitDTO);

                measureunit = await measureunitService.Insert(measureunit);

                return RedirectToAction("Index");
            }

            return View(measureunitDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var measureunit = await measureunitService.GetById(id.Value);

            if (measureunit == null)
                return NotFound();

            ViewBag.Status = await measureunitService.GetStatus();
            var measureunitDTO = mapper.Map<MeasureUnitDTO>(measureunit);
            return View(measureunitDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(MeasureUnitDTO measureunitDTO)
        {
            if (ModelState.IsValid)
            {
                var measureunit = mapper.Map<MeasureUnit>(measureunitDTO);

                measureunit = await measureunitService.Update(measureunit);

                return RedirectToAction("Index");
            }

            return View(measureunitDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var measureunit = await measureunitService.GetById(id.Value);

            if (measureunit == null)
                return NotFound();

            var measureunitDTO = mapper.Map<MeasureUnitDTO>(measureunit);

            return View(measureunitDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await measureunitService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}