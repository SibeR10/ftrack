﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class TravelDTO
    {
        public int TravelID { get; set; }

        [Required(ErrorMessage = "The DateTravel is required")]
        [Display(Name = "Fecha del viaje")]
        public DateTime DateTravel { get; set; }

        [Required(ErrorMessage = "The VehiclesID is required")]
        [Display(Name = "Vehiculo")]
        public int VehiclesID { get; set; }

        [Required(ErrorMessage = "The DriverID is required")]
        [Display(Name = "Chofer")]
        public int DriverID { get; set; }

        [Required(ErrorMessage = "The Origen is required")]
        [Display(Name = "Origen")]
        public int OriginID { get; set; }

        [Required(ErrorMessage = "The DestinationID is required")]
        [Display(Name = "Destino")]
        public int DestinationID { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }

    }
}

