﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ChargesByQuoteDTO
    {
        public int ChargesByQuoteID { get; set; }

        [Display(Name = "Tipo de mercancia")]
        public int MerchandiseTypeID { get; set; }

        [Display(Name = "Cotizacion")]
        public int QuoteID { get; set; }

        [Display(Name = "Tipo de servicio")]
        public int ServiceTypeID { get; set; }

        [Display(Name = "Aplicado por")]
        public int ApplyByID { get; set; }

        [Display(Name = "Unidad de medida")]
        public int MeasureUnitID { get; set; }

        [Display(Name = "Valor")]
        public int ValueCharge { get; set; }

        [Display(Name = "Tarifa")]
        public double Tariff { get; set; }
    }
}
