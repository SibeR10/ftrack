﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations.Schema;
namespace Angle.Models
{
    public class ServiceOrderStatus
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServiceOrderStatusID { get; set; }
        public string ServiceOrderStatusName { get; set; }
        public string StatusColor { get; set; }
    }
}
