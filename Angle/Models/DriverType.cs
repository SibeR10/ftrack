﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class DriverType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DriverTypeID { get; set; }
        public string DriverTypeName { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
