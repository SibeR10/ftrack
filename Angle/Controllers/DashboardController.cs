﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Angle.Data;
using Microsoft.EntityFrameworkCore;


namespace Angle.Controllers
{
    public class DashboardController : Controller
    {
        private readonly FTContext _context;

        public DashboardController(FTContext context)
        {
            _context = context;
        }
        // GET: Dashboard
        public async Task<IActionResult> Index()
        {
            ViewBag.Fuel = await _context.Fuel.ToListAsync();
            ViewBag.Driver = await _context.Driver.ToListAsync();
            ViewBag.HistoricFuel = await _context.HistoricFuel.ToListAsync();
            ViewBag.ServiceOrder = await _context.ServiceOrder.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.Customer = await _context.Customer.ToListAsync();
            ViewBag.Vehicles = await _context.Vehicles.ToListAsync();
            return View();
        }
        // GET: Dashboard
        public async Task<IActionResult> Dashboard2()
        {
            ViewBag.Fuel = await _context.Fuel.ToListAsync();
            ViewBag.Driver = await _context.Driver.ToListAsync();
            ViewBag.HistoricFuel = await _context.HistoricFuel.ToListAsync();
            ViewBag.ServiceOrder = await _context.ServiceOrder.ToListAsync();
            ViewBag.Quote = await _context.Quote.ToListAsync();
            ViewBag.Customer = await _context.Customer.ToListAsync();
            ViewBag.Vehicles = await _context.Vehicles.ToListAsync();
            return View();
        }
        public IActionResult Dashboard_v2()
        {
            return View();
        }
        public IActionResult Dashboard_v3()
        {
            return View();
        }
        public IActionResult Dashboard_h()
        {
            return View();
        }
    }
}