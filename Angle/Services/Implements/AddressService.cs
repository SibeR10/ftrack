﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class AddressService : GenericService<Address>, IAddressService
    {
        private IAddressRepository addressRepository;

        public AddressService(IAddressRepository addressRepository) : base(addressRepository)
        {
            this.addressRepository = addressRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await addressRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByAddress(int id)
        {
            return await addressRepository.GetStatusByAddress(id);
        }


        public async Task<IEnumerable<Customer>> GetCustomerById(int id)
        {
            return await addressRepository.GetCustomerById(id);
        }

        public async Task<IEnumerable<Customer>> GetCustomer()
        {
            return await addressRepository.GetCustomer();
        }


 

        public async Task<IEnumerable<GeoLevel1>> GetGeoLevel1()
        {
            return await addressRepository.GetGeoLevel1();
        }

        public async Task<IEnumerable<AddressType>> GetAddressTypeById(int id)
        {
            return await addressRepository.GetAddressTypeById(id);
        }

        public async Task<IEnumerable<AddressType>> GetAddressType()
        {
            return await addressRepository.GetAddressType();
        }


    }
}
