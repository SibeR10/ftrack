﻿using Angle.Models;

namespace Angle.Services
{
    public interface IEnrollmentService : IGenericService<Enrollment>
    {
    }
}
