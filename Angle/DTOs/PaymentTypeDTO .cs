﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class PaymentTypeDTO
    {
        public int PaymentTypeID { get; set; }

        [Required(ErrorMessage = "The PaymentTypeDescription is required")]
        [Display(Name = "Formas de pago")]
        public string PaymentTypeDescription { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

