﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using ContractSpecialSequence = Angle.Models.ContractSpecialSequence;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class ContractSpecialSequenceController : Controller
    {
        private readonly IContractSpecialSequenceService ContractSpecialSequenceService;
        private readonly IMapper mapper;

        public ContractSpecialSequenceController(IContractSpecialSequenceService ContractSpecialSequenceService,
            IMapper mapper)
        {
            this.ContractSpecialSequenceService = ContractSpecialSequenceService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await ContractSpecialSequenceService.GetAll();
            ViewBag.Customer = await ContractSpecialSequenceService.GetCustomer();
            ViewBag.ContractSpecial = await ContractSpecialSequenceService.GetContractSpecial();
            ViewBag.GeoLevel1 = await ContractSpecialSequenceService.GetGeoLevel1();
            ViewBag.Warehouse = await ContractSpecialSequenceService.GetWarehouse();
            ViewBag.MerchandiseType = await ContractSpecialSequenceService.GetMerchandiseType();
            ViewBag.DeliveryMode = await ContractSpecialSequenceService.GetDeliveryMode();
            ViewBag.Status = await ContractSpecialSequenceService.GetStatus();
            ViewBag.ServiceType = await ContractSpecialSequenceService.GetServiceType();
            var listContractSpecialSequence = data.Select(x => mapper.Map<ContractSpecialSequenceDTO>(x)).ToList();
            return View(listContractSpecialSequence);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Customer = await ContractSpecialSequenceService.GetCustomer();
            ViewBag.ContractSpecial = await ContractSpecialSequenceService.GetContractSpecial();
            ViewBag.GeoLevel1 = await ContractSpecialSequenceService.GetGeoLevel1();
            ViewBag.Warehouse = await ContractSpecialSequenceService.GetWarehouse();
            ViewBag.MerchandiseType = await ContractSpecialSequenceService.GetMerchandiseType();
            ViewBag.DeliveryMode = await ContractSpecialSequenceService.GetDeliveryMode();
            ViewBag.Status = await ContractSpecialSequenceService.GetStatus();
            ViewBag.ServiceType = await ContractSpecialSequenceService.GetServiceType();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ContractSpecialSequenceDTO ContractSpecialSequenceDTO)
        {
            
            if (ModelState.IsValid)
            {

                var ContractSpecialSequence = mapper.Map<ContractSpecialSequence>(ContractSpecialSequenceDTO);

                ContractSpecialSequence = await ContractSpecialSequenceService.Insert(ContractSpecialSequence);

                return RedirectToAction("Index", "ContractSpecial");
            }

            return View(ContractSpecialSequenceDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var ContractSpecialSequence = await ContractSpecialSequenceService.GetById(id.Value);

            if (ContractSpecialSequence == null)
                return NotFound();
            ViewBag.Customer = await ContractSpecialSequenceService.GetCustomer();
            ViewBag.ContractSpecial = await ContractSpecialSequenceService.GetContractSpecial();
            ViewBag.GeoLevel1 = await ContractSpecialSequenceService.GetGeoLevel1();
            ViewBag.Warehouse = await ContractSpecialSequenceService.GetWarehouse();
            ViewBag.MerchandiseType = await ContractSpecialSequenceService.GetMerchandiseType();
            ViewBag.DeliveryMode = await ContractSpecialSequenceService.GetDeliveryMode();
            ViewBag.Status = await ContractSpecialSequenceService.GetStatus();
            ViewBag.ServiceType = await ContractSpecialSequenceService.GetServiceType();
            var ContractSpecialSequenceDTO = mapper.Map<ContractSpecialSequenceDTO>(ContractSpecialSequence);
            return View(ContractSpecialSequenceDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ContractSpecialSequenceDTO ContractSpecialSequenceDTO)
        {
            if (ModelState.IsValid)
            {
                var ContractSpecialSequence = mapper.Map<ContractSpecialSequence>(ContractSpecialSequenceDTO);

                ContractSpecialSequence = await ContractSpecialSequenceService.Update(ContractSpecialSequence);

                return RedirectToAction("Index", "ContractSpecial");
            }

            return View(ContractSpecialSequenceDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var ContractSpecialSequence = await ContractSpecialSequenceService.GetById(id.Value);

            if (ContractSpecialSequence == null)
                return NotFound();

            var ContractSpecialSequenceDTO = mapper.Map<ContractSpecialSequenceDTO>(ContractSpecialSequence);

            return View(ContractSpecialSequenceDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await ContractSpecialSequenceService.Delete(id.Value);

            return RedirectToAction("Index", "ContractSpecial");
        }
    }
}