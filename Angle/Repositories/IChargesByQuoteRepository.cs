﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IChargesByQuoteRepository : IGenericRepository<ChargesByQuote>
    {
        Task<IEnumerable<MerchandiseType>> GetMerchandiseType();
        Task<IEnumerable<Customer>> GetCustomer();
        Task<IEnumerable<Quote>> GetQuote();
        Task<IEnumerable<ContractSpecial>> GetContractSpecial();
        Task<IEnumerable<ContractSpecialSequence>> GetContractSpecialSequence();
        Task<IEnumerable<ServiceType>> GetServiceType();
        Task<IEnumerable<ApplyBy>> GetApplyBy();
        Task<IEnumerable<MeasureUnit>> GetMeasureUnit();
        Task<IEnumerable<ContractSpecialMeasury>> GetContractSpecialMeasury();
        Task<IEnumerable<ContractSequence>> GetContractSequence();
        Task<IEnumerable<ContractMeasury>> GetContractMeasury();
    }
}
