﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Angle.Services
{
    public interface IHistoricFuelService : IGenericService<HistoricFuel>
    {
        Task<IEnumerable<Driver>> GetDriver();
        Task<IEnumerable<Fuel>> GetFuel();
        Task<IEnumerable<Vehicles>> GetVehiclesById(int id);

    }
}
