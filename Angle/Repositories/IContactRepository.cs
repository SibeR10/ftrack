﻿using Angle.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Angle.Repositories
{
    public interface IContactRepository : IGenericRepository<Contact>
    {
        Task<IEnumerable<Status>> GetStatus();

        Task<IEnumerable<Status>> GetStatusByContact(int id);

        Task<IEnumerable<Customer>> GetCustomer();
        Task<IEnumerable<Customer>> GetCustomerById(int id);


        
    }
}
