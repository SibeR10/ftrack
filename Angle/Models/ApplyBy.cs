﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class ApplyBy
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ApplyByID { get; set; }
        public string ApplyByName { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
