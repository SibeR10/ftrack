﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class CustomerTypeDTO
    {
        public int CustomerTypeID { get; set; }

        [Required(ErrorMessage = "The CustomerTypeName is required")]
        [Display(Name = "Tipo de cliente")]
        public string CustomerTypeName { get; set; }

        [Required(ErrorMessage = "The CustomerTypeName is required")]
        [Display(Name = "Descripción del tipo de cliente")]
        public string CustomerTypeDescription { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

