﻿using AutoMapper;
using Angle.DTOs;
using Angle.Models;
using Angle.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using Amazon.EC2;

using MerchandiseType = Angle.Models.MerchandiseType;
using Angle.Services.Implements;

namespace Angle.Controllers
{
    public class MerchandiseTypeController : Controller
    {
        private readonly IMerchandiseTypeService merchandisetypeService;
        private readonly IMapper mapper;

        public MerchandiseTypeController(IMerchandiseTypeService merchandisetypeService,
            IMapper mapper)
        {
            this.merchandisetypeService = merchandisetypeService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var data = await merchandisetypeService.GetAll();
            ViewBag.Status = await merchandisetypeService.GetStatus();
            var listMerchandiseType = data.Select(x => mapper.Map<MerchandiseTypeDTO>(x)).ToList();
            return View(listMerchandiseType);
        }



        public async Task<IActionResult> CreateAsync()
        {

            ViewBag.Status = await merchandisetypeService.GetStatus();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(MerchandiseTypeDTO merchandisetypeDTO)
        {
            
            if (ModelState.IsValid)
            {

                var merchandisetype = mapper.Map<MerchandiseType>(merchandisetypeDTO);

                merchandisetype = await merchandisetypeService.Insert(merchandisetype);

                return RedirectToAction("Index");
            }

            return View(merchandisetypeDTO);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var merchandisetype = await merchandisetypeService.GetById(id.Value);

            if (merchandisetype == null)
                return NotFound();

            ViewBag.Status = await merchandisetypeService.GetStatus();
            var merchandisetypeDTO = mapper.Map<MerchandiseTypeDTO>(merchandisetype);
            return View(merchandisetypeDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(MerchandiseTypeDTO merchandisetypeDTO)
        {
            if (ModelState.IsValid)
            {
                var merchandisetype = mapper.Map<MerchandiseType>(merchandisetypeDTO);

                merchandisetype = await merchandisetypeService.Update(merchandisetype);

                return RedirectToAction("Index");
            }

            return View(merchandisetypeDTO);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var merchandisetype = await merchandisetypeService.GetById(id.Value);

            if (merchandisetype == null)
                return NotFound();

            var merchandisetypeDTO = mapper.Map<MerchandiseTypeDTO>(merchandisetype);

            return View(merchandisetypeDTO);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            await merchandisetypeService.Delete(id.Value);

            return RedirectToAction("Index");
        }
    }
}