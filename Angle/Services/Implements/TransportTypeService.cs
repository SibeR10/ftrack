﻿using Angle.Models;
using Angle.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Angle.Services.Implements
{
    public class TransportTypeService : GenericService<TransportType>, ITransportTypeService
    {
        private ITransportTypeRepository transporttypeRepository;

        public TransportTypeService(ITransportTypeRepository transporttypeRepository) : base(transporttypeRepository)
        {
            this.transporttypeRepository = transporttypeRepository;
        }

        public async Task<IEnumerable<Status>> GetStatus()
        {
            return await transporttypeRepository.GetStatus();
        }

        public async Task<IEnumerable<Status>> GetStatusByTransportType(int id)
        {
            return await transporttypeRepository.GetStatusByTransportType(id);
        }

    }
}
