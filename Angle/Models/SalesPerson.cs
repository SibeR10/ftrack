﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Angle.Models
{
    public class SalesPerson
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SalesPersonID { get; set; }
        public string SalesPersonName { get; set; }
        public Status Status { get; set; }
        public int StatusID { get; set; }
    }
}
