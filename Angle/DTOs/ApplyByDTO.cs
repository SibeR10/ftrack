﻿using System.ComponentModel.DataAnnotations;

namespace Angle.DTOs
{
    public class ApplyByDTO
    {
        public int ApplyByID { get; set; }

        [Required(ErrorMessage = "The ApplyByName is required")]
        [Display(Name = "Aplicado por")]
        public string ApplyByName { get; set; }

        [Required(ErrorMessage = "The Estatus is required")]
        [Display(Name = "Estatus")]
        public int StatusID { get; set; }
    }
}

